#include <iostream>
#include <algorithm>
#include <functional>
#include <vector>
#include <list>
#include <iterator>
#include <time.h>

using namespace std;

struct Cell
{
    int x;
    int y;

    Cell &operator +=(const Cell &cell)
    {
        x += cell.x;
        y += cell.y;
        return *this;
    }

    Cell &operator +(const Cell &cell)
    {
        x += cell.x;
        y += cell.y;
        return *this;
    }

    Cell &operator -(const Cell &cell)
    {
        x -= cell.x;
        y -= cell.y;
        return *this;
    }

    Cell &operator *(const Cell &cell)
    {
        x *= cell.x;
        y *= cell.y;
        return *this;
    }

    Cell &operator ++()
    {
        x += 1;
        y += 1;
        return *this;
    }

    bool operator <(const Cell &cell) const
    {
        if (x == cell.x)
        {
            return y < cell.y;
        }
        else
        {
            return x < cell.x;
        }
    }

    bool operator ==(const Cell &cell) const
    {
        return x == cell.x && y == cell.y;
    }
};

std::ostream &operator<<(std::ostream &os, const Cell &cell)
{
    os << "{" << cell.x << "," << cell.y << "}";
    return os;
}

int main()
{
    std::mt19937 g{std::random_device{}()};

    srand((unsigned)time(NULL));;

    std::cout << std::boolalpha;

    std::function<Cell()> generate = []()
    {
        int n = std::rand() % 10 + 100;
        Cell cell{n, n};
        return cell;
    };

    // 初始化lCells1
    std::list<vector<Cell>> lCells1(6, vector<Cell>(5));
    //用从起始值开始连续递增的值填充一个范围
    for (vector<Cell> &vCells : lCells1)
    {
        std::generate(vCells.begin(), vCells.end(), generate);
    }

    size_t index = 0;
    for (vector<Cell> &vCells : lCells1)
    {
        std::cout << "lCells    " << index << "           ";
        std::copy(vCells.begin(), vCells.end(), std::ostream_iterator<Cell>(std::cout, " "));
        std::cout << std::endl;

        vector<Cell> aCells(vCells.size());
        //计算 [first, last) 范围中每对相邻元素的第二个和第一个的差
        std::adjacent_difference(vCells.begin(), vCells.end(), aCells.begin());
        std::cout << "adjacent_difference   " ;
        std::copy(aCells.begin(), aCells.end(), std::ostream_iterator<Cell>(std::cout, " "));
        std::cout << std::endl;

        index++;
    }

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;


    auto BinaryOperation = [](const Cell & a, const Cell & b)
    {
        Cell cell{a.x - b.x, a.y - b.y};
        return cell;
    };

    // 初始化lCells2
    std::list<vector<Cell>> lCells2(6, vector<Cell>(5));
    for (vector<Cell> &vCells : lCells2)
    {
        std::generate(vCells.begin(), vCells.end(), generate);
    }

    index = 0;
    for (vector<Cell> &vCells : lCells2)
    {
        std::cout << "lCells    " << index << "           ";
        std::copy(vCells.begin(), vCells.end(), std::ostream_iterator<Cell>(std::cout, " "));
        std::cout << std::endl;

        vector<Cell> aCells(vCells.size());
        //计算 [first, last) 范围中每对相邻元素的第二个和第一个的差
        std::adjacent_difference(vCells.begin(), vCells.end(), aCells.begin(), BinaryOperation);
        std::cout << "adjacent_difference   " ;
        std::copy(aCells.begin(), aCells.end(), std::ostream_iterator<Cell>(std::cout, " "));
        std::cout << std::endl;

        index++;
    }

    return 0;
}
