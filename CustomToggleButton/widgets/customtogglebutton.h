#ifndef CUSTOMTOGGLEBUTTON_H
#define CUSTOMTOGGLEBUTTON_H

#include <QWidget>
#include <QMouseEvent>
#include <QTimer>

namespace Ui {
class CustomToggleButton;
}

class CustomToggleButton : public QWidget
{
    Q_OBJECT

public:
    typedef enum
    {
       AlignLeft = 0, // 按钮在左
       AlignRight

    }ButtonState;

public:
    explicit CustomToggleButton(QWidget *parent = 0);
    ~CustomToggleButton();

    void setTimer(const int msec = 10);
    void setText(const QString &left, const QString &right);
    void setButtonSize(const QSize &size);

    void setState(const CustomToggleButton::ButtonState state);
    uint getState();

signals:
    void sgStateChange(CustomToggleButton::ButtonState state);

private:

    void mousePressEvent(QMouseEvent *event) override;

    void showEvent(QShowEvent *event) override;

public slots:
    void slMove(); //定时器的超时槽函数，用来实现按钮的滑动效果

private:
    Ui::CustomToggleButton *ui;
    QTimer *m_pTimer;
    int m_Msec;

    uint m_ButtonState;
    QSize m_Size;
};

#endif // CUSTOMTOGGLEBUTTON_H
