#include "customtogglebutton.h"
#include "ui_customtogglebutton.h"

CustomToggleButton::CustomToggleButton(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomToggleButton)
{
    ui->setupUi(this);
    m_pTimer = new QTimer();
    m_Msec = 10;

    m_ButtonState = AlignRight;

    m_Size = QSize(72, 23);

    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(slMove()));
}

CustomToggleButton::~CustomToggleButton()
{
    delete ui;
    delete m_pTimer;
}

/**
 * @brief  设置移动一个像素需要的时间
 * @param  [in] msec 毫秒
 * @return
 * @author 龚桂兴
 * @date   2019-10-15
 */
void CustomToggleButton::setTimer(const int msec)
{
    m_Msec = msec;
}

/**
 * @brief  设置按钮显示文字
 * @param  [in] left 左边显示文字
 * @param  [in] right 右边显示文字
 * @return
 * @author 龚桂兴
 * @date   2019-10-15
 */
void CustomToggleButton::setText(const QString &left, const QString &right)
{
    ui->label_start->setText(left);
    ui->label_stop->setText(right);
}

/**
 * @brief  设置按钮大小，不建议设置，本身会根据文字自动调整大小
 * @param  [in] size 按钮尺寸
 * @return
 * @author 龚桂兴
 * @date   2019-10-15
 */
void CustomToggleButton::setButtonSize(const QSize &size)
{
    m_Size = size;
}

/**
 * @brief  设置按钮状态
 * @param  [in] state 按钮状态
 * @return
 * @author 龚桂兴
 * @date   2019-10-15
 */
void CustomToggleButton::setState(const CustomToggleButton::ButtonState state)
{
    m_pTimer->stop();
    m_ButtonState = state;
    if(state == AlignLeft)
    {
        this->setStyleSheet(QString(".QWidget{background-color:rgba(170, 170, 170, 1);spacing:1;min-width:%1;}").arg(m_Size.width()));
        ui->label_start->hide();
        ui->label_stop->show();
        ui->label_move->move(1, ui->label_move->pos().y());
    }
    else
    {
        this->setStyleSheet(QString(".QWidget{background-color:rgba(82, 119, 139, 1);spacing:1;min-width:%1;}").arg(m_Size.width()));
        ui->label_start->setVisible(true);
        ui->label_stop->setVisible(false);
        ui->label_move->move(this->size().width() - ui->label_move->size().width() - 1, ui->label_move->pos().y());
    }

    emit sgStateChange(state);
}

/**
 * @brief  获取按钮状态
 * @param
 * @return 按钮状态
 * @author 龚桂兴
 * @date   2019-10-15
 */
uint CustomToggleButton::getState()
{
    return m_ButtonState;
}

void CustomToggleButton::mousePressEvent(QMouseEvent *event)
{
    if(event->button() != Qt::LeftButton)
    {
        return ;
    }

    switch (m_ButtonState)
    {
        case AlignRight:
        case AlignLeft:
             m_pTimer->start(m_Msec);
             break;

        default:
            break;
    }

    ui->label_stop->hide();
    ui->label_start->hide();

    QWidget::mousePressEvent(event);
}

void CustomToggleButton::showEvent(QShowEvent *event)
{
    if(event == NULL)
    {
        return;
    }

    if(!ui->widget->isVisible())
        setState((ButtonState)m_ButtonState);

//    this->adjustSize();
    ui->label_start->adjustSize();
    ui->label_stop->adjustSize();
    ui->label_move->setFixedSize(ui->label_start->size());
    m_Size = QSize(ui->label_move->size().width() + ui->label_start->size().width() + 2, 23);
//    ui->label_move->setStyleSheet(QString(".QLabel{min-width:%1;}").arg(ui->label_move->size().width()));
    this->setFixedSize(m_Size);
}

/**
 * @brief  按钮移动槽函数
 * @param
 * @return
 * @author 龚桂兴
 * @date   2019-10-15
 */
void CustomToggleButton::slMove()
{
    switch(m_ButtonState)
    {
    case AlignLeft:

        if(ui->label_move->pos().x() + ui->label_move->size().width() + 2 > this->size().width())
        {
            setState(AlignRight);
            return ;
        }

        ui->label_move->move(ui->label_move->pos().x() + 1,  ui->label_move->pos().y());

        break;

    case AlignRight:

        if(ui->label_move->pos().x() < 1)
        {
            setState(AlignLeft);
            return;
        }
        ui->label_move->move(ui->label_move->pos().x() - 1, ui->label_move->pos().y());
        break;
    }
}
