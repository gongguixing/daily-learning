//#include <iostream>
//#include <fstream>
//#include <string>

//int main()
//{
//    std::ofstream os("test.txt");
//    std::ifstream is("test.txt");
//    std::string value("0");

//    os << "Hello";
//    is >> value;

//    std::cout << "Result before tie(): \"" << value << "\"\n";
//    is.clear();
//    is.tie(&os);

//    is >> value;

//    std::cout << "Result after tie(): \"" << value << "\"\n";
//}


//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream local;
//    auto cout_buff = std::cout.rdbuf(); // 保存指向 std::cout 缓冲的指针

//    std::cout.rdbuf(local.rdbuf()); // 以 'local' 对象的缓冲
//    // 替换内部的 std::cout 缓冲

//    // 现在 std::cout 以 'local' 缓冲工作
//    // 你看不到此消息
//    std::cout << "some message";

//    // 回到旧缓冲
//    std::cout.rdbuf(cout_buff);

//    // 你将看到此消息
//    std::cout << "back to default buffer\n";

//    // 打印 'local' 内容
//    std::cout << "local content: " << local.str() << "\n";
//}

//#include <iostream>
//#include <sstream>
//#include <locale>

//int main()
//{
//    std::istringstream iss;
//    iss.imbue(std::locale("C"));

//    std::cout << "Current locale: " << iss.getloc().name() << '\n';

//    iss.imbue(std::locale());
//    std::cout << "Global locale : " << iss.getloc().name() << '\n';
//}

//#include <iostream>
//#include <fstream>

//int main()
//{
//    int ivalue;
//    try
//    {
//        std::ifstream in("in.txt");
//        in.exceptions(std::ifstream::failbit);
//        in >> ivalue;
//    }
//    catch (std::ios_base::failure &fail)
//    {
//        // 此处处理异常
//    }
//    return 0;
//}

//#include <iostream>
//#include <iomanip>

//int main()
//{
//    std::cout << "With default setting : " << std::setw(10) << 40 << '\n';
//    char prev = std::cout.fill('x');
//    std::cout << "Replaced '" << prev << "' with '"
//              << std::cout.fill() << "': " << std::setw(10) << 40 << '\n';
//}

//#include <iostream>
//#include <fstream>

//int main()
//{
//    std::ofstream out;

//    out.copyfmt(std::cout); // 复制 rdstate 和 rdbuf 外的所有内容
//    out.clear(std::cout.rdstate()); // 复制 rdstate
//    out.basic_ios<char>::rdbuf(std::cout.rdbuf()); // 共享缓冲

//    out << "Hello, world\n";
//}

//#include <iostream>
//#include <string>

//int main()
//{
//    double n;
//    while (std::cout << "Please, enter a number\n"
//            && !(std::cin >> n))
//    {
//        std::cin.clear();
//        std::string line;
//        std::getline(std::cin, line);
//        std::cout << "I am sorry, but '" << line << "' is not a number\n";
//    }
//    std::cout << "Thank you for entering the number " << n << '\n';
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream stream;

//    if (!stream.fail())
//    {
//        std::cout << "stream is not fail\n";
//    }

//    stream.setstate(std::ios_base::failbit);

//    if (stream.fail())
//    {
//        std::cout << "now stream is fail\n";
//    }

//    if (!stream.good())
//    {
//        std::cout << "and stream is not good\n";
//    }
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream stream;

//    if (stream.rdstate() == std::ios_base::goodbit)
//    {
//        std::cout << "stream state is goodbit\n";
//    }

//    stream.setstate(std::ios_base::eofbit);

//    // 检查状态为准确的 eofbit （无 failbit 且无 badbit ）
//    if (stream.rdstate() == std::ios_base::eofbit)
//    {
//        std::cout << "stream state is eofbit\n";
//    }
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::istringstream s("1 2 3 error");
//    int n;
//    std::cout << std::boolalpha << "s is " << static_cast<bool>(s) << '\n';
//    while (s >> n)
//    {
//        std::cout << n << '\n';
//    }
//    std::cout << "s is " << static_cast<bool>(s) << '\n';
//}

//#include <iostream>
//#include <fstream>
//#include <cstdlib>

//int main()
//{
//    std::ifstream file("test.txt");
//    if (!file) // operator! 用于此
//    {
//        std::cout << "File opening failed\n";
//        return EXIT_FAILURE;
//    }

//    // 典型的 C++ I/O 循环以 I/O 函数的返回值为循环控制条件，
//    // operator bool() 用于此
//    for (int n; file >> n;)
//    {
//        std::cout << n << ' ';
//    }
//    std::cout << '\n';

//    if (file.bad())
//    {
//        std::cout << "I/O error while reading\n";
//    }
//    else if (file.eof())
//    {
//        std::cout << "End of file reached successfully\n";
//    }
//    else if (file.fail())
//    {
//        std::cout << "Non-integer data encountered\n";
//    }
//}
