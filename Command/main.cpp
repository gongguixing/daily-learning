#include <iostream>
#include "concretecommand.h"
#include "invoker.h"

int main()
{

    // 命令（Command）模式的定义如下：将一个请求封装为一个对象，
    // 使发出请求的责任和执行请求的责任分割开。
    // 这样两者之间通过命令对象进行沟通，
    // 这样方便将命令对象进行储存、传递、调用、增加与管理。

    // 命令类（Concrete Command）角色
    ICommand * pCmd = new ConcreteCommand();
    // 调用者/请求者（Invoker）角色
    Invoker * pInv  = new Invoker(pCmd);
    // 通过命令类调用接收者
    pInv->call();

    delete pInv;
    delete pCmd;

    return 0;
}
