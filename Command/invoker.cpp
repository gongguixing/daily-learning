#include "invoker.h"
#include <iostream>

Invoker::Invoker(ICommand *p)
{
    mPCommand = p;
}

void Invoker::setCommand(ICommand *p)
{
    mPCommand = p;
}

void Invoker::call()
{
    // 通过命令角色调用接受者
    std::cout << "Invoker Command execute." << std::endl;
    mPCommand->execute();
}
