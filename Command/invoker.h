#ifndef INVOKER_H
#define INVOKER_H

#include "icommand.h"

// 调用者/请求者（Invoker）角色
// 是请求的发送者，它通常拥有很多的命令对象，
// 并通过访问命令对象来执行相关请求，它不直接访问接收者。
class Invoker
{
public:
    Invoker(ICommand *p);
    // 设置命令角色
    void setCommand(ICommand *p);
    void call();

private:
    ICommand *mPCommand;
};

#endif // INVOKER_H
