#ifndef RECEIVER_H
#define RECEIVER_H

// 实现者/接收者（Receiver）角色
// 执行命令功能的相关操作，是具体命令对象业务的真正实现者。
class Receiver
{
public:
    Receiver();

    // 接收者方法
    void action();
};

#endif // RECEIVER_H
