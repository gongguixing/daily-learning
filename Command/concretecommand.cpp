#include "concretecommand.h"
#include "receiver.h"

ConcreteCommand::ConcreteCommand()
{
    mPReceiver = new Receiver();
}

void ConcreteCommand::execute()
{
    mPReceiver->action();
}
