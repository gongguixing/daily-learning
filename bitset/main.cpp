#include <iostream>
#include <bitset>
#include <string>
#include <sstream>
#include <limits>

template<size_t _Nb>
void printBitset(const std::string &name, const std::bitset<_Nb> &bitset)
{
    std::cout << name << ":  ";
    for (size_t index = 0; index < bitset.size(); index++)
    {
        std::cout << bitset[index] << " ";
    }
    std::cout << std::endl;
}

int main()
{
    std::cout << std::boolalpha;

    return 0;
}

void hash()
{

    //转换 bitset 的内容为 string 。用 zero 表示拥有值 false 的位，以 one 表示拥有值 true 的位。
    //产生的字符串含 N 个字符，其首字符对应最末（第 N-1th ）位而其尾字符对应首位。
    std::bitset<8> bitset1(123);
    std::cout << "bitset1.to_string():          " << bitset1.to_string() << std::endl;
    std::cout << "bitset1.to_string('*'):       " << bitset1.to_string('*') << std::endl;
    std::cout << "bitset1.to_string('X','Y'):   " << bitset1.to_string('X', 'Y') << std::endl;
    std::cout << std::endl;


    //转换 bitset 的内容为 unsigned long 整数。
    //bitset 的首位对应数的最低位，而尾位对应最高位。
    for (unsigned long i = 0; i < 8; ++i)
    {
        std::bitset<5> b(i);
        std::bitset<5> b_inverted = ~b;
        std::cout << i << '\t';
        std::cout << b << '\t';
        std::cout << b_inverted << '\t';
        std::cout << b_inverted.to_ulong() << '\n';
    }
    std::cout << std::endl;


    //转换 bitset 的内容为 unsigned long long 整数。
    //bitset 的首位对应数的最低位，而尾位对应最高位。
    std::bitset<std::numeric_limits<unsigned long long>::digits> bitset3(
        0x123456789abcdef0LL
    );
    std::cout << bitset3 << "  " << std::hex << bitset3.to_ullong() << std::endl;
    bitset3.flip();
    std::cout << bitset3 << "  " << bitset3.to_ullong() << std::endl;
    std::cout << std::endl;


    //1) 若 *this 与 rhs 中的所有位相等则返回 true 。
    std::bitset<8> bitset5("10101010");
    std::bitset<8> bitset6(bitset5);
    std::bitset<8> bitset7("01010101");
    std::cout << "bitset5:              " << bitset5 << std::endl;
    std::cout << "bitset6:              " << bitset6 << std::endl;
    std::cout << "bitset7:              " << bitset7 << std::endl;
    //1) 若 *this 与 rhs 中的所有位相等则返回 true 。
    //2) 若 *this 与 rhs 中的任何位不相等则返回 true 。
    std::cout << "bitset5 == bitset6    " << (bitset5 == bitset6) << std::endl;
    std::cout << "bitset5 != bitset6    " << (bitset5 != bitset6) << std::endl;
    std::cout << "bitset5 == bitset7    " << (bitset5 == bitset7) << std::endl;
    std::cout << "bitset5 != bitset7    " << (bitset5 != bitset7) << std::endl;
    std::cout << std::endl;

    //返回 bitset 所能保有的位数。
    std::cout << "bitset5.size():       " << bitset5.size() << std::endl;
    std::cout << "bitset6.size():       " << bitset6.size() << std::endl;
    std::cout << "bitset7.size():       " << bitset7.size() << std::endl;
    std::cout << std::endl;

    //std::hash 对 std::bitset<N> 的模板特化允许用户获得 std::bitset<N> 类型的对象的哈希。
    std::hash<std::bitset<8>> hash_fn;
    size_t hash1 = hash_fn(bitset5);
    size_t hash2 = hash_fn(bitset6);
    size_t hash3 = hash_fn(bitset7);
    std::cout << "std::hash<bitset5>:   " << hash1 << std::endl;
    std::cout << "std::hash<bitset6>:   " << hash2 << std::endl;
    std::cout << "std::hash<bitset7>:   " << hash3 << std::endl;

}

void operator_1()
{
    //进行二个 bitset lhs 和 rhs 间的二进制与、或及异或。
    std::bitset<8> bitset1("101010");
    std::bitset<8> bitset2("100011");
    std::cout << "bitset1:              " << bitset1 << std::endl;
    std::cout << "bitset2:              " << bitset2 << std::endl;
    //1) 返回含 lhs 和 rhs 的位对应对上的二进制与结果的 bitset<N> 。
    std::cout << "bitset1 & bitset2:    " << (bitset1 & bitset2) << std::endl;
    //2) 返回含 lhs 和 rhs 的位对应对上的二进制或结果的 bitset<N> 。
    std::cout << "bitset1 | bitset2:    " << (bitset1 | bitset2) << std::endl;
    //3) 返回含 lhs 和 rhs 的位对应对上的二进制异或结果的 bitset<N> 。
    std::cout << "bitset1 ^ bitset2:    " << (bitset1 ^ bitset2) << std::endl;
    std::cout << std::endl;

    //从字符流插入或释出 bitset 。
    std::string bit_string = "001101";
    std::istringstream bit_stream(bit_string);
    //1) 写 bitset x 入字符流 os ，如同首先用 to_string() 将它转换成 basic_string<CharT,Traits> ，
    //再用 operator<< （对字符串是有格式输出函数 (FormattedOutputFunction) ）将它写入 os 。
    std::bitset<3> bitset3;
    bit_stream >> bitset3; // 读取 "001" ，流仍保有 "101"
    std::cout << "bitset3:              " << bitset3 << std::endl;
    //2) 表现为有格式输入函数 (FormattedInputFunction) 。
    //构造并检查 sentry 对象，这可能跳过前导空白符，之后从 is 释出至多 N 个字符，并存储字符于 bitset x 。
    std::bitset<8> bitset4;
    bit_stream >> bitset4; // 读取 "101" ，产出 8 位集为 "00000101"
    std::cout << "bitset4:              " << bitset4 << std::endl;
}

void set_reset_flip()
{

    std::bitset<8> bitset11("101010");
    std::cout << "bitset11:             " << bitset11 << std::endl;
    //设置所有位为 true 或到指定值。1) 设置所有位为 true 。
    bitset11.set();
    std::cout << "bitset11.set():       " << bitset11 << std::endl;
    //设置所有位为 true 或到指定值。2) 设置在 pos 的位为值 value 。
    bitset11.set(0, false);
    std::cout << "bitset11.set():       " << bitset11 << std::endl;
    std::cout << std::endl;

    std::bitset<8> bitset12(bitset11);
    std::cout << "bitset12:             " << bitset12 << std::endl;
    //设置所有位为 true 或到指定值。2) 设置在 pos 的位为 false 。
    bitset12.reset(0);
    std::cout << "bitset12.reset():     " << bitset12 << std::endl;
    //设置所有位为 true 或到指定值。1) 设置所有位为 false 。
    bitset12.reset();
    std::cout << "bitset12.set():       " << bitset12 << std::endl;
    std::cout << std::endl;

    //翻转位，即更改 true 值为 false 并更改 false 值为 true 。
    //等价于在 bitset 一部分或全体上的逻辑非。
    std::bitset<8> bitset13("101010");
    std::cout << "bitset13:             " << bitset13 << std::endl;
    //1) 翻转所有位（类似 operator~ ，但是在原位）。
    bitset13.flip(0);
    std::cout << "bitset13.flip():      " << bitset13 << std::endl;
    //2) 翻转在 pos 的位。
    bitset13.flip();
    std::cout << "bitset13.flip():      " << bitset13 << std::endl;
}

void operator__()
{
    std::cout << std::boolalpha;
    std::bitset<8> bitset1("101010");
    std::bitset<8> bitset2("100011");
    std::cout << "bitset1:              " << bitset1 << std::endl;
    std::cout << "bitset2:              " << bitset2 << std::endl;
    //1) 设置位为 *this 与 other 的位的对应对上二进制与的结果。
    std::cout << "bitset1 & bitset2:    " << (bitset1 & bitset2) << std::endl;
    //2) 设置位为 *this 与 other 的位的对应对上二进制或的结果。
    std::cout << "bitset1 | bitset2:    " << (bitset1 | bitset2) << std::endl;
    //3) 设置位为 *this 与 other 的位的对应对上二进制异或的结果。
    std::cout << "bitset1 ^ bitset2:    " << (bitset1 ^ bitset2) << std::endl;
    //4) 返回翻转所有位的 *this 的临时副本（二进制非）。
    std::bitset<8> bitset3 = ~bitset1;
    std::cout << "~bitset1:             " << bitset3 << std::endl;
    std::cout << std::endl;


    //进行二进制左移和二进制右移。移入零。
    //1-2) 进行二进制左移。 (2) 是破坏性的，即对当前对象进行迁移。
    std::bitset<8> bitset5("101010");
    std::bitset<8> bitset6 = bitset5 << 3;
    std::cout << "bitset5:              " << bitset5 << std::endl;
    std::cout << "bitset6:              " << bitset6 << std::endl;
    std::bitset<8> bitset7 = bitset5 <<= 3;
    std::cout << "bitset5:              " << bitset5 << std::endl;
    std::cout << "bitset7:              " << bitset7 << std::endl;

    //进行二进制左移和二进制右移。移入零。
    //3-4) 进行二进制右移。 (4) 是破坏性的，即对当前对象进行迁移。
    std::bitset<8> bitset8("101010");
    std::bitset<8> bitset9 = bitset8 >> 3;
    std::cout << "bitset8:              " << bitset5 << std::endl;
    std::cout << "bitset9:              " << bitset9 << std::endl;
    std::bitset<8> bitset10 = bitset8 >>= 3;
    std::cout << "bitset8:              " << bitset5 << std::endl;
    std::cout << "bitset10:             " << bitset10 << std::endl;
    std::cout << std::endl;
}

void operator_()
{
    std::string bit_string = "110010";
    std::bitset<8> bitset1(bit_string);       // [0,0,1,1,0,0,1,0]
    std::cout << "bitset1:  " << bitset1 << std::endl;
    for (size_t index = 0; index < bitset1.size(); index++)
    {
        //访问位于位置 pos 的位。首版本返回位的值，
        //第二版本返回允许修改位的值的 std::bitset::reference 对象。
        //不同于 test() ，它不抛异常：若 pos 在边界外则行为未定义。
        bitset1[index] = ~bitset1[index];
    }
    std::cout << "bitset1:  " << bitset1 << std::endl;
    printBitset("bitset1", bitset1);
    std::cout << std::endl;


    std::bitset<8> bitset2(bit_string);       // [0,0,1,1,0,0,1,0]
    std::cout << "bitset2:  " << bitset2 << std::endl;
    std::cout << "bitset2:  " ;
    for (size_t index = 0; index < bitset1.size(); index++)
    {
        //返回位于位置 pos 的位的值。
        //不同于 operator[] ，它进行边界检查，
        //且若 pos 不对应 bitset 中的合法位置则抛出 std::out_of_range 。
        std::cout << bitset2.test(index) << " ";
    }
    std::cout << std::endl;
    std::cout << std::endl;

    std::bitset<6> bitset3("111111");
    std::bitset<6> bitset4("010101");
    std::bitset<6> bitset5("000000");
    //1) 检查是否全部位被设为 true 。
    std::cout << bitset3 << " --- " << "bool all() const noexcept:   "
              << bitset3.all() << std::endl;
    std::cout << bitset4 << " --- " << "bool all() const noexcept:   "
              << bitset4.all() << std::endl;
    std::cout << bitset5 << " --- " << "bool all() const noexcept:   "
              << bitset5.all() << std::endl;
    //2) 检查是否任一位被设为 true 。
    std::cout << bitset3 << " --- " << "bool any() const noexcept:   "
              << bitset3.any() << std::endl;
    std::cout << bitset4 << " --- " << "bool any() const noexcept:   "
              << bitset4.any() << std::endl;
    std::cout << bitset5 << " --- " << "bool any() const noexcept:   "
              << bitset5.any() << std::endl;
    //3) 检查是否无位被设为 true 。
    std::cout << bitset3 << " --- " << "bool none() const noexcept:  "
              << bitset3.none() << std::endl;
    std::cout << bitset4 << " --- " << "bool none() const noexcept:  "
              << bitset4.none() << std::endl;
    std::cout << bitset5 << " --- " << "bool none() const noexcept:  "
              << bitset5.none() << std::endl;
    std::cout << std::endl;

    //返回设为 true 的位数。
    std::cout << bitset3 << " --- " << "bool count() const noexcept: "
              << bitset3.count() << std::endl;
    std::cout << bitset4 << " --- " << "bool count() const noexcept: "
              << bitset4.count() << std::endl;
    std::cout << bitset5 << " --- " << "bool count() const noexcept: "
              << bitset5.count() << std::endl;

}

void bitset_()
{
    std::cout << std::boolalpha;

    //从数个数据源之一构造新的 bitset : 1) 默认构造函数。构造所有位设为零的 bitset 。
    std::bitset<6> bitset1;
    std::cout << "bitset1:  " << bitset1 << std::endl;
    printBitset("bitset1", bitset1);
    std::cout << std::endl;

    // 2) 构造 bitset ，初始化其首（最右、最低） M 位位置为对应 val 的位值，
    //其中 M 是 unsigned long long 的位数和正在构造的 bitset 中的位数 N 的较小者。
    //若 M 小于 N （ bitset 长于 32 (C++11 前)64 (C++11 起) 位，
    //对于典型的 unsigned long (C++11 起)long 实现），则剩余位位置被初始化为零。
    std::bitset<6> bitset2(100);
    std::cout << "bitset2:  " << bitset2 << std::endl;
    printBitset("bitset2", bitset2);
    std::cout << std::endl;

    //3) 用 std::basic_string str 中的字符构造 bitset 。
    //能提供可选的起始位置 pos 和长度 n ，以及指代设置（ one ）和不设置（ zero ）位的替代值的字符。
    //用 Traits::eq() 比较字符值。
    //初始化字符串的有效长度是 min(n, str.size() - pos) 。
    //若 pos > str.size() ，则构造函数抛出 std::out_of_range 。
    //若 str 中检验到的任何字符非 zero 或 one ，则抛出 std::invalid_argument 。
    std::string bit_string = "110010";
    std::bitset<8> bitset3(bit_string);       // [0,0,1,1,0,0,1,0]
    std::cout << "bitset3:  " << bitset3 << std::endl;
    printBitset("bitset3", bitset3);

    std::bitset<8> bitset4(bit_string, 2);
    std::cout << "bitset4:  " << bitset4 << std::endl;
    printBitset("bitset4", bitset4);

    std::bitset<8> bitset5(bit_string, 1, 5);
    std::cout << "bitset5:  " << bitset5 << std::endl;
    printBitset("bitset5", bitset5);
    std::cout << std::endl;

    //4) 同 (3) ，但用 CharT* 代替 std::basic_string 。
    //等价于 bitset(n == basic_string<CharT>::npos ?
    //basic_string<CharT>(str) : basic_string<CharT>(str, n), 0, n, zero, one) 。
    std::string alpha_bit_string = "aBaaBBaB";// [0,1,0,0,1,1,0,1]
    std::bitset<8> bitset6(alpha_bit_string, 0, alpha_bit_string.size(), 'a', 'B');
    std::cout << "bitset6:  " << bitset6 << std::endl;
    printBitset("bitset6", bitset6);
    std::cout << std::endl;

    std::bitset<8> bitset7("XXXXYYYY", 8, 'X', 'Y'); // [0,0,0,0,1,1,1,1]
    std::cout << "bitset7:  " << bitset7 << std::endl;
    printBitset("bitset7", bitset7);
    std::cout << std::endl;
}
