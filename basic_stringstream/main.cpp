#include <sstream>
#include <string>
#include <iostream>

int main()
{
    //3) 以 str 的副本为底层字符串设备的初始内容。
    std::string string1 = "I am a handsome programmer";
    std::basic_ostringstream<char>
    basic_ostringstream1(string1, std::ios_base::out);
    std::cout << "basic_ostringstream1: "
              << basic_ostringstream1.str() << std::endl;
    std::cout << "basic_ostringstream1 rdstate: ";
    std::cout << basic_ostringstream1.rdstate() << std::endl;

    std::string string2 = "I am a super handsome guy";
    std::basic_ostringstream<char>
    basic_ostringstream2(string2, std::ios_base::out);
    std::cout << "basic_ostringstream2: "
              << basic_ostringstream2.str() << std::endl;
    std::cout << "basic_ostringstream2 rdstate: ";
    std::cout << basic_ostringstream2.rdstate() << std::endl;
    std::cout << std::endl;

    //为 std::basic_ostringstream 特化 std::swap 算法。
    //交换 lhs 与 rhs 的状态。等效地调用 lhs.swap(rhs)
    std::swap(basic_ostringstream1, basic_ostringstream2);
    std::cout << "after swap: " << std::endl;

    std::cout << "basic_ostringstream1: "
              << basic_ostringstream1.str() << std::endl;
    std::cout << "basic_ostringstream1 rdstate: ";
    std::cout << basic_ostringstream1.rdstate() << std::endl;

    std::cout << "basic_ostringstream2: "
              << basic_ostringstream2.str() << std::endl;
    std::cout << "basic_ostringstream2 rdstate: ";
    std::cout << basic_ostringstream2.rdstate() << std::endl;

    return 0;
}

//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_stringstream<char>
//    basic_stringstream1(string1, std::ios_base::in | std::ios_base::out);
//    std::cout << "basic_stringstream1: "
//              << basic_stringstream1.str() << std::endl;
//    std::cout << "basic_stringstream1 rdstate: ";
//    std::cout << basic_stringstream1.rdstate() << std::endl;

//    std::string string2 = "I am a super handsome guy";
//    std::basic_stringstream<char>
//    basic_stringstream2(string2, std::ios_base::in | std::ios_base::out);
//    std::cout << "basic_stringstream2: "
//              << basic_stringstream2.str() << std::endl;
//    std::cout << "basic_stringstream2 rdstate: ";
//    std::cout << basic_stringstream2.rdstate() << std::endl;
//    std::cout << std::endl;

//    //交换流与 other 的状态。
//    //通过调用 basic_istream<CharT, Traits>::swap(other)
//    //和 rdbuf()->swap(*other.rdbuf()) 进行。
//    basic_stringstream1.swap(basic_stringstream2);
//    std::cout << "after swap: " << std::endl;

//    std::cout << "basic_stringstream1: "
//              << basic_stringstream1.str() << std::endl;
//    std::cout << "basic_stringstream1 rdstate: ";
//    std::cout << basic_stringstream1.rdstate() << std::endl;

//    std::cout << "basic_stringstream2: "
//              << basic_stringstream2.str() << std::endl;
//    std::cout << "basic_stringstream2 rdstate: ";
//    std::cout << basic_stringstream2.rdstate() << std::endl;

//    return 0;
//}

//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_stringstream<char>
//    basic_stringstream1(string1, std::ios_base::in | std::ios_base::out);
//    //1) 返回底层字符串的副本，如同通过调用 rdbuf()->str() 。
//    std::cout << "basic_stringstream1: "
//              << basic_stringstream1.str() << std::endl;

//    //2) 替换底层字符串，如同通过调用 rdbuf()->str(new_str) 。
//    std::string string2 = "I am a super handsome guy";
//    basic_stringstream1.str(string2);
//    std::cout << "basic_stringstream1: "
//              << basic_stringstream1.str() << std::endl;

//    return 0;
//}

//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_stringstream<char>
//    basic_stringstream1(string1, std::ios_base::in | std::ios_base::out);
//    std::cout << "basic_stringstream1: " << basic_stringstream1.str() << std::endl;
//    //返回指向底层未处理字符串设备对象的指针。
//    std::cout << "basic_stringstream1: "
//              << static_cast<void*>(basic_stringstream1.rdbuf()) << std::endl;

//    return 0;
//}

//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_stringstream<char>
//    basic_stringstream1(string1, std::ios_base::in | std::ios_base::out);
//    std::cout << "basic_stringstream1: "
//              << basic_stringstream1.str() << std::endl;
//    std::cout << "rdstate: " << basic_stringstream1.rdstate() << std::endl;

//    std::string string2 = "I am a super handsome guy";
//    std::basic_stringstream<char>
//    basic_stringstream2(string2, std::ios_base::in | std::ios_base::out);
//    std::cout << "basic_stringstream2: "
//              << basic_stringstream2.str() << std::endl;
//    basic_stringstream2.setstate(std::ios_base::badbit);
//    std::cout << "rdstate: " << basic_stringstream2.rdstate() << std::endl;

//    return 0;
//}

//int main()
//{
//    //1) 默认构造函数。以默认打开模式构造新的底层字符串设备。
//    std::basic_stringstream<int> basic_stringstream1;

//    //2) 构造新的底层字符串设备。
//    //以 basic_stringbuf<Char,Traits,Allocator>(mode | ios_base::in|ios_base::out)
//    //构造底层 basic_stringbuf 对象。
//    std::basic_stringstream<char>
//    basic_stringstream2(std::ios_base::in | std::ios_base::out);

//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_stringstream<char>basic_stringstream3
//    (string1, std::ios_base::in | std::ios_base::out | std::ios_base::app);
//    std::cout << "basic_stringstream3: " << basic_stringstream3.str() << std::endl;
//    basic_stringstream3 << "GGX";
//    std::cout << "basic_stringstream3: " << basic_stringstream3.str() << std::endl;
//    std::cout << std::endl;

//    //4) 移动构造函数。用移动语义，构造拥有 other 的状态的字符串流。
//    std::basic_stringstream<char> basic_stringstream4(std::move(basic_stringstream3));
//    std::cout << "basic_stringstream4: " << basic_stringstream4.str() << std::endl;

//    return 0;
//}
