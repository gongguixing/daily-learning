//#include <iostream>
//#include <iomanip>
//#include <sstream>

//int main()
//{
//    std::string input = "n greetings";
//    std::istringstream stream(input);
//    char c;
//    const int MAX = 6;
//    char cstr[MAX];

//    stream >> c >> std::setw(MAX) >> cstr;
//    std::cout << "c = " << c << '\n'
//              << "cstr = " << cstr << '\n';

//    double f;
//    std::istringstream("1.23") >> f; // 右值流释出
//    std::cout << "f = " << f << '\n';

//    return 0;
//}

//#include <iostream>
//#include <sstream>

//struct Foo
//{
//    char n[5];
//};

//std::istream& operator>>(std::istream& is, Foo& f)
//{
//    std::istream::sentry s(is);
//    if (s)
//    {
//        is.read(f.n, 5);
//    }
//    return is;
//}

//int main()
//{
//    std::string input = "   abcde";
//    std::istringstream stream(input);
//    Foo f;
//    stream >> f;
//    std::cout.write(f.n, 5);
//    std::cout << '\n';
//}

//#include <sstream>
//#include <iostream>
//#include <utility>

//int main()
//{
//    std::istringstream s1("hello");
//    std::istringstream s2("bye");

//    s1.swap(s2); // OK ： istringstream 拥有公开的 swap()
//    std::swap(s1, s2); // OK ：调用 s1.swap(s2)
////  std::cin.swap(s2); // 错误： swap 是受保护成员

//    std::cout << s1.rdbuf();
//}

//#include <iostream>
//#include <fstream>

//void file_abc()
//{
//    std::ofstream f("test.txt");
//    f << "abc\n";
//}

//void file_123()
//{
//    std::ofstream f("test.txt");
//    f << "123\n";
//}

//int main()
//{
//    file_abc(); // 文件现在含 "abc"
//    std::ifstream f("test.txt");
//    std::cout << "Reading from the file\n";
//    char c;
//    f >> c;
//    std::cout << c;
//    file_123(); // 文件现在含 "123"
//    f >> c;
//    std::cout << c;
//    f >> c;
//    std::cout << c << '\n';
//    f.close();

//    file_abc(); // 文件现在含 "abc"
//    f.open("test.txt");
//    std::cout << "Reading from the file, with sync()\n";
//    f >> c;
//    std::cout << c;
//    file_123(); // 文件现在含 "123"
//    f.sync();
//    f >> c;
//    std::cout << c;
//    f >> c;
//    std::cout << c << '\n';
//}

//#include <iostream>
//#include <string>
//#include <sstream>

//int main()
//{
//    std::string str = "Hello, world";
//    std::istringstream in(str);
//    std::string word1, word2;

//    in >> word1;
//    in.seekg(0); // 回溯
//    in >> word2;

//    std::cout << "word1 = " << word1 << '\n'
//              << "word2 = " << word2 << '\n';
//    return 0;
//}

//#include <iostream>
//#include <string>
//#include <sstream>

//int main()
//{
//    std::string str = "Hello, world";
//    std::istringstream in(str);
//    std::string word;
//    in >> word;
//    std::cout << "After reading the word \"" << word
//              << "\" tellg() returns " << in.tellg() << '\n';
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    char x[20];
//    std::istringstream stream("Hello World");

//    stream.read(x, sizeof x);
//    std::cout << "Characters extracted: " << stream.gcount();
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    char c[10] = {};
//    std::istringstream input("This is sample text."); // std::stringbuf 令个缓冲可用于无阻塞读取
//    input.readsome(c, 5); // 读取 'This ' 并存储于 c[0] .. c[4]
//    input.readsome(c, 9); // 读取 'is sample' 并存储于 c[0] .. c[8]
//    std::cout << c;
//}

//#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <string>
//#include <cstdint>

//int main()
//{
//    // read() 常用于二进制 I/O
//    std::string bin = {'\x12', '\x12', '\x12', '\x12'};
//    std::istringstream raw(bin);
//    std::uint32_t n;
//    if (raw.read(reinterpret_cast<char*>(&n), sizeof n))
//    {
//        std::cout << std::hex << std::showbase << n << '\n';
//    }

//    // 为下个片段准备文件
//    std::ofstream("test.txt", std::ios::binary) << "abcd1\nabcd2\nabcd3";

//    // 读取整个文件到 string
//    if (std::ifstream is{"test.txt", std::ios::binary | std::ios::ate})
//    {
//        auto size = is.tellg();
//        std::string str(size, '\0'); // 构造 string 为流大小
//        is.seekg(0);
//        if (is.read(&str[0], size))
//        {
//            std::cout << str << '\n';
//        }
//    }
//}

//#include <iostream>
//#include <sstream>
//#include <limits>

//int main()
//{
//    std::istringstream input("1\n"
//                             "some non-numeric input\n"
//                             "2\n");
//    for (;;)
//    {
//        int n;
//        input >> n;

//        if (input.eof() || input.bad())
//        {
//            break;
//        }
//        else if (input.fail())
//        {
//            input.clear(); // 反设置 failbit
//            input.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // 跳过坏输入
//        }
//        else
//        {
//            std::cout << n << '\n';
//        }
//    }
//    return 0;
//}

//#include <iostream>
//#include <sstream>
//#include <vector>
//#include <array>

//int main()
//{
//    std::istringstream input("abc|def|gh");
//    std::vector<std::array<char, 4>> v;

//    // 注意：下列循环在从  getline() 返回的流上的
//    // std::ios_base::operator bool() 返回 false 时终止
//    for (std::array<char, 4> a; input.getline(&a[0], 4, '|');)
//    {
//        v.push_back(a);
//    }

//    for (auto& a : v)
//    {
//        std::cout << &a[0] << '\n';
//    }
//}

//#include <sstream>
//#include <iostream>

//int main()
//{
//    std::stringstream s1("Hello, world"); // IO 流
//    s1.get();
//    if (s1.putback('Y')) // 修改缓冲区
//    {
//        std::cout << s1.rdbuf() << '\n';
//    }
//    else
//    {
//        std::cout << "putback failed\n";
//    }

//    std::istringstream s2("Hello, world"); // 仅输入流
//    s2.get();
//    if (s2.putback('Y')) // cannot modify input-only buffer
//    {
//        std::cout << s2.rdbuf() << '\n';
//    }
//    else
//    {
//        std::cout << "putback failed\n";
//    }

//    s2.clear();
//    if (s2.putback('H')) // 非修改回放
//    {
//        std::cout << s2.rdbuf() << '\n';
//    }
//    else
//    {
//        std::cout << "putback failed\n";
//    }
//}

//#include <sstream>
//#include <iostream>

//int main()
//{
//    std::istringstream s1("Hello, world.");
//    char c1 = s1.get();
//    if (s1.unget())
//    {
//        char c2 = s1.get();
//        std::cout << "Got: " << c1 << " got again: " << c2 << '\n';
//    }
//}

//#include <sstream>
//#include <iostream>

//int main()
//{
//    std::istringstream s1("Hello, world.");
//    char c1 = s1.peek();
//    char c2 = s1.get();
//    std::cout << "Peeked: " << c1 << " got: " << c2 << '\n';
//}

//#include <sstream>
//#include <iostream>

//int main()
//{
//    std::istringstream s1("Hello, world.");
//    char c1 = s1.get(); // 读取'H'
//    std::cout << "after reading " << c1 << ", gcount() == " <<  s1.gcount() << '\n';
//    char c2;
//    s1.get(c2);         // 读取 'e'
//    char str[5];
//    s1.get(str, 5);     // 读取 "llo,"
//    std::cout << "after reading " << str << ", gcount() == " <<  s1.gcount() << '\n';
//    std::cout << c1 << c2 << str;
//    s1.get(*std::cout.rdbuf()); // 读取剩余，不包括 '\n'
//    std::cout << "\nAfter the last get(), gcount() == " << s1.gcount() << '\n';
//}

//#include <iostream>
//#include <iomanip>
//#include <sstream>

//int main()
//{
//    std::string input = "41 3.14 false hello world";
//    std::istringstream stream(input);
//    int n;
//    double f;
//    bool b;

//    stream >> n >> f >> std::boolalpha >> b;
//    std::cout << "n = " << n << '\n'
//              << "f = " << f << '\n'
//              << "b = " << std::boolalpha << b << '\n';

//    // 用 streambuf 重载释出剩余内容
//    stream >> std::cout.rdbuf();
//    std::cout << '\n';
//}

//#include <sstream>
//#include <iostream>

//void print_stringbuf(std::streambuf* p)
//{
//    std::istream buf(p); // buf 与 s1 共享缓冲
//    int n;
//    buf >> n;
//    std::cout << n;
//} // 调用 buf 的析构函数。 p 保持不受影响

//int main()
//{
//    std::istringstream s1("10 20");
//    print_stringbuf(s1.rdbuf());
//    int n;
//    s1 >> n;
//    std::cout << ',' << n << '\n';
//}

//#include <sstream>
//#include <iostream>

//int main()
//{
//    std::istringstream s1("hello");
//    std::istream s2(s1.rdbuf());                        // OK ： s2 与 s1 共享缓冲

////    std::istream s3(std::istringstream("test"));      // 错误：移动构造函数为受保护
////    std::istream s4(s2);                              // 错误：复制构造函数被删除
//    std::istringstream s5(std::istringstream("world")); // OK ：导出类调用移动构造函数

//    std::cout << s2.rdbuf() << ' ' << s5.rdbuf() << '\n';
//}

//#include <iostream>

//struct Foo
//{
//    int n;
//    Foo()
//    {
//        std::cout << "Enter n: "; // 不需要冲入
//        std::cin >> n;
//    }
//};

//Foo f; // static object

//int main()
//{
//    std::cout << "f.n is " << f.n << '\n';
//}
