#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qtimer.h>

class CMotionControl;

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void slTimer();

    void on_start_clicked();

private:
    Ui::MainWindow *ui;

    int m_step;             //每次移动的像素

    CMotionControl *m_pMotionControl; //控制弹球运动轨迹及颜色
    QTimer      *m_pTimer;  //定时器，触发移动
};

#endif // MAINWINDOW_H
