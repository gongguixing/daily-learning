#include "cmotioncontrol.h"

CMotionControl::CMotionControl()
{
    m_Point = {0, 0};
    m_Range = {0, 0};
    m_Step  = {0, 0};

    initColor();
}

void CMotionControl::setPoint(int x, int y)
{
    m_Point = {x, y};
}

SPoint CMotionControl::getPoint()
{
    return m_Point;
}

void CMotionControl::setRange(int x, int y)
{
    m_Range = {x, y};
}

void CMotionControl::setStep(SPoint step)
{
    m_Step = step;
}

bool CMotionControl::move()
{
    //弹球是否碰壁
    bool bChange = false;

    if (!m_Point.isRangeX(0, m_Range.x))
    {
        //若是超出范围，则按反方向移动
        m_Step.x = -m_Step.x;
        bChange = true;
    }

    if (!m_Point.isRangeY(0, m_Range.y))
    {
        //若是超出范围，则按反方向移动
        m_Step.y = -m_Step.y;
        bChange = true;
    }

    if (bChange)
    {
        //弹球碰壁，则变换颜色
        m_indexColor++;
    }

    if (m_indexColor >= m_colors.size())
    {
        m_indexColor = 0;
    }

    //弹球位置移动
    m_Point += m_Step;
    return bChange;
}

std::string CMotionControl::color()
{
    return m_colors.at(m_indexColor);
}

std::string CMotionControl::borderColor()
{
    return m_colors.at(m_colors.size() - m_indexColor - 1);
}

void CMotionControl::initColor()
{
    m_indexColor = 0;

//    m_colors.emplace_back("color0");
//    m_colors.emplace_back("color1");
    m_colors.emplace_back("black");
    m_colors.emplace_back("white");
    m_colors.emplace_back("darkGray");
    m_colors.emplace_back("gray");
    m_colors.emplace_back("lightGray");
    m_colors.emplace_back("red");
    m_colors.emplace_back("green");
    m_colors.emplace_back("blue");
    m_colors.emplace_back("darkGray");
    m_colors.emplace_back("cyan");
    m_colors.emplace_back("magenta");
    m_colors.emplace_back("yellow");
    m_colors.emplace_back("darkRed");
    m_colors.emplace_back("darkGray");
    m_colors.emplace_back("darkGreen");
    m_colors.emplace_back("darkBlue");
    m_colors.emplace_back("darkCyan");
    m_colors.emplace_back("darkMagenta");
    m_colors.emplace_back("darkYellow");
//    m_colors.emplace_back("transparent");
}
