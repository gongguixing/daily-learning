#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "qdebug.h"
#include "cmotioncontrol.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_pTimer = new QTimer(this);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(slTimer()));

    m_step = 10;
    m_pMotionControl = new CMotionControl();
    m_pMotionControl->setStep({m_step, m_step});
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_pTimer;
    delete m_pMotionControl;
}

void MainWindow::slTimer()
{
    //移动弹球坐标
    bool bChange = m_pMotionControl->move();
    //获取移动后的位置
    SPoint point = m_pMotionControl->getPoint();
    //移动弹球
    ui->label_point->move(point.x, point.y);

    //弹球碰壁则改变颜色
    if (bChange)
    {
        QString bgColor = m_pMotionControl->color().c_str();
        QString bdColor = m_pMotionControl->borderColor().c_str();

        int length = 100;
        QString strColor = "";
        strColor.append(QString("min-width:%1px;").arg(length));
        strColor.append(QString("min-height:%1px;").arg(length));
        strColor.append(QString("max-width:%1px;").arg(length));
        strColor.append(QString("max-height:%1px;").arg(length));
        strColor.append(QString("border-radius:%1px;").arg(length / 2));
        strColor.append(QString("border:%1px solid %2;").arg(3).arg(bdColor));
        strColor.append(QString("background-color:%1;").arg(bgColor));

        ui->label_point->setStyleSheet(QString("QLabel{%1}").arg(strColor));  //设置样式表
    }

    qDebug() << "SPoint: {" << point.x << ", " << point.y << "}";
}

void MainWindow::on_start_clicked()
{
    //设置弹球起点
    m_pMotionControl->setPoint(ui->label_point->pos().x(),
                               ui->label_point->pos().y());

    //设置弹球范围
    m_pMotionControl->setRange(width() - ui->label_point->width() - m_step,
                               height() - ui->label_point->height() - m_step);
    int msec = 50;
    //启停定时器
    if (m_pTimer->isActive())
    {
        m_pTimer->stop();
    }
    else
    {
        m_pTimer->start(msec);
    }
}
