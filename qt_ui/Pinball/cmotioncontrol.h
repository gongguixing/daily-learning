#ifndef CMOTIONCONTROL_H
#define CMOTIONCONTROL_H

#include <vector>
#include <string>

/**
 * @brief  坐标信息结构体
 * @author GGX
 * @date   2023-08-13
 */
struct SPoint
{
    int x;
    int y;

    /**
     * @brief  重载操作符
     * @param  [in] point 自增信息
     * @return 无
     * @author GGX
     * @date   2023-08-13
     */
    SPoint & operator+=(SPoint point)
    {
        x += point.x;
        y += point.y;
        return *this;
    }

    /**
     * @brief  判断X是否在范围内
     * @param  [in] lX 范围左区间
     * @param  [in] rX 范围右区间
     * @return  true 在范围内，false不在范围内
     * @author GGX
     * @date   2023-08-13
     */
    bool isRangeX(int lX, int rX)
    {
        return x > lX && x < rX;
    }

    /**
     * @brief  判断Y是否在范围内
     * @param  [in] lY 范围左区间
     * @param  [in] rY 范围右区间
     * @return  true 在范围内，false不在范围内
     * @author GGX
     * @date   2023-08-13
     */
    bool isRangeY(int lY, int rY)
    {
        return y > lY && y < rY;
    }
};

/**
 * @brief  弹球控制类
 * @author GGX
 * @date   2023-08-13
 */
class CMotionControl
{
public:
    CMotionControl();

    /**
     * @brief  设置坐标位置
     * @param  [in] x 坐标轴
     * @param  [in] y 坐标轴
     * @return 无
     * @author GGX
     * @date   2023-08-13
     */
    void setPoint(int x, int y);

    /**
     * @brief  获取坐标位置
     * @param  无
     * @return SPoint 坐标位置
     * @author GGX
     * @date   2023-08-13
     */
    SPoint getPoint();

    /**
     * @brief  设置范围右区间
     * @param  [in] x 坐标轴
     * @param  [in] y 坐标轴
     * @return SPoint 坐标位置
     * @author GGX
     * @date   2023-08-13
     */
    void setRange(int x, int y);

    /**
     * @brief  设置步进值
     * @param  [in] SPoint x,y步进值
     * @return 无
     * @author GGX
     * @date   2023-08-13
     */
    void setStep(SPoint step);

    /**
     * @brief  在坐标范围内移动弹球位置
     * @param  [in] SPoint x,y步进值
     * @return 无
     * @author GGX
     * @date   2023-08-13
     */
    bool move();

    /**
     * @brief  获取弹球颜色
     * @param  无
     * @return std::string 弹球颜色
     * @author GGX
     * @date   2023-08-13
     */
    std::string color();

    /**
     * @brief  获取弹球边框颜色
     * @param  无
     * @return std::string 弹球边框颜色
     * @author GGX
     * @date   2023-08-13
     */
    std::string borderColor();

protected:
    /**
     * @brief  初始化弹球颜色
     * @param  无
     * @return 无
     * @author GGX
     * @date   2023-08-13
     */
    void initColor();

private:
    SPoint m_Point;     //弹球位置
    SPoint m_Range;     //范围右区间
    SPoint m_Step;      //步进值

    size_t m_indexColor;    //当前弹球颜色
    std::vector<std::string> m_colors;  //弹球颜色容器
};

#endif // CMOTIONCONTROL_H
