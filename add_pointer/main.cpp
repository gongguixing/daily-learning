#include <iostream>
#include <type_traits>

int main()
{
    int i = 123;
    int& ri = i;
    typedef std::add_pointer<decltype(i)>::type IntPtr;
    typedef std::add_pointer<decltype(ri)>::type IntPtr2;
    IntPtr pi = &i;
    std::cout << "i = " << i << std::endl;
    std::cout << "*pi = " << *pi << std::endl;
    std::cout << "std::is_pointer<std::add_pointer<decltype(i)>::type>::value:  "
              << std::is_pointer<IntPtr>::value << std::endl;
    std::cout << "std::is_same<std::add_pointer<decltype(i), int*>::value:      "
              << std::is_same<IntPtr, int*>::value << std::endl;
    std::cout << "std::is_same<std::add_pointer<IntPtr2, IntPtr>::value:        "
              << std::is_same<IntPtr2, IntPtr>::value << std::endl;
    std::cout << std::endl;

    typedef std::remove_pointer<IntPtr>::type IntAgain;
    IntAgain j = i;
    std::cout << "j = " << j << std::endl;

    std::cout << "std::is_pointer<std::remove_pointer<IntPtr>::type>::value:    "
              << std::is_pointer<std::remove_pointer<IntPtr>::type>::value << std::endl;
    std::cout << "std::is_pointer<std::remove_pointer<IntPtr>::type>::value:    "
              << std::is_same<IntAgain, int>::value << std::endl;

    return 0;
}

namespace detail
{

template <class T>
struct type_identity
{
    using type = T;
}; // ��ʹ�� std::type_identity (C++20 ��)

template <class T>
auto try_add_pointer(int) -> type_identity<typename std::remove_reference<T>::type*>;
template <class T>
auto try_add_pointer(...) -> type_identity<T>;

} // namespace detail

template <class T>
struct add_pointer : decltype(detail::try_add_pointer<T>(0)) {};
