#ifndef COS_H
#define COS_H


//class COS
//{
//public:
//    COS();
//};

#include <winsock2.h>
#include <windows.h>
#include <objbase.h>
#include <signal.h>
#include <dbghelp.h>
#include <string>

#define _DEBUG "123"

using namespace std;

inline BOOL   IsDataSectionNeeded(const WCHAR* pModuleName);
inline BOOL   CALLBACK MiniDumpCallback(PVOID pParam, const PMINIDUMP_CALLBACK_INPUT pInput, PMINIDUMP_CALLBACK_OUTPUT pOutput);
inline void   CreateMiniDump(PEXCEPTION_POINTERS pep, LPCTSTR strFileName);
LONG   WINAPI MyUnhandledExceptionFilter(PEXCEPTION_POINTERS pExceptionInfo);

static void
sighandler(int signal)
{
//    LOGF_UTILS("Process received signal %d.", signal);
    if (
        SIGINT   == signal ||
        SIGILL   == signal ||
        SIGFPE   == signal ||
        SIGSEGV  == signal ||
        SIGTERM  == signal ||
        SIGBREAK == signal ||
        SIGABRT  == signal)
    {
        exit(1);
    }
}

void winit()
{
#if 0
    vector<string> argv;
    str::split(::GetCommandLine(), " ", argv);
#endif

    for (int i = 1; i < __argc; ++i)
    {
        if (string(__argv[i]) == "console")
        {
            FreeConsole();
        }
    }

#ifdef _DEBUG
    SetUnhandledExceptionFilter(MyUnhandledExceptionFilter);
#endif

//    os::net()->init();

    for (int i = 1; i <= 22; i++)
    {
        if (
#ifdef _DEBUG
            SIGSEGV == i ||
            SIGABRT == i ||
#endif
            5       == i ||
            7       == i ||
            9       == i ||
            10      == i ||
            12      == i ||
            14      == i ||
            18      == i ||
            19      == i ||
            20      == i
        )
        {
            continue;
        }
        signal(i, sighandler);
    }
}

inline BOOL IsDataSectionNeeded(const WCHAR* pModuleName)
{
    if (pModuleName == 0)
    {
        return FALSE;
    }

    WCHAR szFileName[_MAX_FNAME] = L"";

    _wsplitpath_s(pModuleName, NULL, 0, szFileName, 0, NULL, 0, NULL, 0);

    if (_wcsicmp(szFileName, L"ntdll") == 0)
    {
        return TRUE;
    }
    return FALSE;
}

inline BOOL CALLBACK
MiniDumpCallback(PVOID pParam, const PMINIDUMP_CALLBACK_INPUT pInput, PMINIDUMP_CALLBACK_OUTPUT pOutput)
{
    if (pInput == 0 || pOutput == 0)
    {
        return FALSE;
    }

    switch (pInput->CallbackType)
    {
    case ModuleCallback:
        if (pOutput->ModuleWriteFlags & ModuleWriteDataSeg)
            if (!IsDataSectionNeeded(pInput->Module.FullPath))
            {
                pOutput->ModuleWriteFlags &= (~ModuleWriteDataSeg);
            }
    case IncludeModuleCallback:
    case IncludeThreadCallback:
    case ThreadCallback:
    case ThreadExCallback:
        return TRUE;
    default:
        ;
    }

    return FALSE;
}

inline void
CreateMiniDump(PEXCEPTION_POINTERS pep, LPCTSTR strFileName)
{
    HANDLE hFile = CreateFile(strFileName, GENERIC_READ | GENERIC_WRITE,
                              FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if ((hFile != NULL) && (hFile != INVALID_HANDLE_VALUE))
    {
        MINIDUMP_EXCEPTION_INFORMATION mdei;
        mdei.ThreadId          = GetCurrentThreadId();
        mdei.ExceptionPointers = pep;
        mdei.ClientPointers    = 0;

        MINIDUMP_CALLBACK_INFORMATION mci;
        mci.CallbackRoutine = (MINIDUMP_CALLBACK_ROUTINE)MiniDumpCallback;
        mci.CallbackParam   = 0;

        ::MiniDumpWriteDump(::GetCurrentProcess(), ::GetCurrentProcessId(), hFile, MiniDumpNormal, (pep != 0) ? &mdei : 0, NULL, &mci);

        CloseHandle(hFile);
    }
}

LONG __stdcall
MyUnhandledExceptionFilter(PEXCEPTION_POINTERS pExceptionInfo)
{
    CreateMiniDump(pExceptionInfo, (LPCTSTR)"core.dmp");
    return EXCEPTION_EXECUTE_HANDLER;
}

#endif // COS_H
