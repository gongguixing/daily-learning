#include <algorithm>
#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>

int main()
{
    std::vector<int> from_vector(10);
    std::iota(from_vector.begin(), from_vector.end(), 0);
    std::cout << "from_vector contains: ";
    std::copy(from_vector.begin(), from_vector.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;


    std::vector<int> to_vector;
    std::copy(from_vector.begin(), from_vector.end(),
              std::back_inserter(to_vector));
    std::cout << "to_vector  contains: ";
    std::copy(to_vector.begin(), to_vector.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;


    // 或可选地，
    // 任一方式都等价于
    //  std::vector<int> to_vector = from_vector;
    std::vector<int> to_vector1(from_vector.size());
    std::copy(from_vector.begin(), from_vector.end(), to_vector1.begin());
    std::cout << "to_vector1 contains: ";
    std::copy(to_vector1.begin(), to_vector1.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;



    auto func = [](int a)
    {
        return a % 2 == 0;
    };
    std::vector<int> to_vector2;
    std::copy_if(from_vector.begin(), from_vector.end(),
                 std::back_inserter(to_vector2), func);
    std::cout << "to_vector2 contains: ";
    std::copy(to_vector2.begin(), to_vector2.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}
