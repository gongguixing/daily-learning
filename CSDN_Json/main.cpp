#include <iostream>
#include <direct.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <map>

#define MAX_PATH 256

#include "jsoncpp/reader.h"
#include "jsoncpp/writer.h"

using namespace std;

int main()
{
    char   buffer[MAX_PATH] = {0};
    getcwd(buffer, MAX_PATH);
    std::string fname = string(buffer) + "\\" + "CSDN.openapi.json";
    std::string cfname = string(buffer) + "\\" + "csdn.json";

    std::ifstream fsin(fname, std::ios::binary);
    std::ifstream cfsin(cfname, std::ios::binary);
    if (!fsin.is_open() || !cfsin.is_open())
    {
        return 0;
    }

    std::string strLine;
    std::string tstrLine;
    while (std::getline(fsin, tstrLine))
    {
        strLine.append(tstrLine);
        tstrLine.clear();
        // std::cout << strLine << std::endl;
    }

    std::string cstrLine;
    while (std::getline(cfsin, cstrLine))
    {
        //   std::cout << "cstrLine :" << cstrLine.size() << "   " << cstrLine << std::endl;
    }

    Json::Reader reader;

    Json::Value root;
    if (!reader.parse(strLine, root))
    {
        std::cout << reader.getFormatedErrorMessages() << std::endl;
    }

    Json::Value croot;
    if (!reader.parse(cstrLine, croot))
    {
        std::cout << reader.getFormatedErrorMessages() << std::endl;
    }

//    Json::Value &apis = root["apis"];
//    Json::Value &childrens = apis[Json::Value::UInt(0)]["children"];
//    Json::Value children;
//    for (Json::Value::UInt i = 0; i < childrens.size(); i++)
//    {
//        if (!childrens.isArray())
//        {
//            break;
//        }

//        children = childrens[i];
//        Json::Value &request = children["request"];

//        std::cout << "children.asString()" << std::endl;

//        std::cout << "name: " << children["name"].asString() << std::endl;
//        std::cout << "url: " <<  request["url"].asString() << std::endl;

//        break;
//    }
    //childrens.clear();

    std::map<string, string> maptags
    {
        {"std::bitset", ""}, {"std::array", ""}, {"std::vector", ""},
        {"std::set", ""}, {"std::map", ""}, {"std::string", ""},
        {"std::list", ""}, {"std::deque", ""}, {"std::forward_list", ""},
        {"std::multiset", ""}, {"std::multimap", ""}, {"std::unique_ptr", ""},
        {"std::shared_ptr", ""}, {"std::chrono::duration", ""}, {"ģʽ", ""},
        {"std::condition_variable", ""}, {"std::weak_ptr", ""},
        {"std::chrono::time_point", ""}, {"std::atomic", ""}
    };
    Json::Value tags = root["tags"];
    for (Json::Value::UInt i = 0; i < tags.size(); i++)
    {
        if (!tags.isArray())
        {
            break;
        }
        Json::Value item = tags[i];
        std::string name = item["name"].asString();

        for (std::pair<const string, string> &it : maptags)
        {
            if (name.find(it.first) != std::string::npos)
            {
                it.second = name;
                break;
            }
        }
    }

    //std::cout << "119654148: " << Json::FastWriter().write(value119654148) << std::endl;
    std::cout << "croot.size(): " << croot.size() << std::endl;
    for (Json::Value::UInt i = 0; i < croot.size(); i++)
    {
        if (!croot.isArray())
        {
            break;
        }

        Json::Value value119654148 = root["paths"]["/qq_40788199/article/details/119654148"];
        Json::Value value = croot[i];
        std::string name = value["name"].asString();

        if (name.find("string") != std::string::npos)
        {
            name = "std::" + name;
        }

        if (name.find("condition_variable") != std::string::npos)
        {
            name = "std::" + name;
        }

        if (name.find("std::weak_ptr") != std::string::npos)
        {
            name = name;
        }

        value119654148["get"]["summary"] = name;
        for (std::pair<const string, string> &it : maptags)
        {
            if (name.find(it.first) != std::string::npos)
            {
                value119654148["get"]["x-apifox-folder"] = it.second;
                break;
            }
        }

        root["paths"][value["mock_url"].asString()] = value119654148;
        std::cout << "name: " << value["name"].asString() << std::endl;
        std::cout << "url: " <<  value["mock_url"].asString() << std::endl;

//        Json::Value &request = children["request"];
//        children["name"] = value["name"];
//        request["url"] = value["mock_url"];
//        childrens.append(children);
        //break;
    }

    std::string cofname = string(buffer) + "\\" + "GGX.json";
    std::fstream fout(cofname, std::ios::binary | std::ios::out);
    if (!fout.is_open())
    {
        std::cout << "is_open error" << std::endl;
    }

    fout << Json::FastWriter().write(root);

    return 0;
}
