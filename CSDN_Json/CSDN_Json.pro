TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += $$PWD/../3rd/include/GGX/pub

LIBS += -L$$PWD/../3rd/shadow/mingw/x32/debug/lib/GGX/pub
LIBS += -ljsoncppd

SOURCES += main.cpp
