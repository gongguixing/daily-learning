#include <iostream>
#include <type_traits>
#include <typeinfo>

struct C
{
};

struct A : C
{
};

struct B : C
{
};

int main()
{
    typedef typename std::common_type<int, double>::type Type1;
    typedef typename std::common_type<char, int>::type Type2;
    typedef typename std::common_type<std::string, char*>::type Type3;
    typedef typename std::common_type<bool, int>::type Type4;
    typedef typename std::common_type<bool, char>::type Type5;
    typedef typename std::common_type<char, double>::type Type6;
    typedef typename std::common_type<A, C, B>::type Type7;

    std::cout << "std::common_type<int, double>::type:          "
              << typeid(Type1).name() << std::endl;
    std::cout << "std::common_type<char, int>::type:            "
              << typeid(Type2).name() << std::endl;
    std::cout << "std::common_type<std::string,double>::type:   "
              << typeid(Type3).name() << std::endl;
    std::cout << "std::common_type<bool, int>::type:            "
              << typeid(Type4).name() << std::endl;
    std::cout << "std::common_type<bool, int>::type:            "
              << typeid(Type5).name() << std::endl;
    std::cout << "std::common_type<char, double>::type:         "
              << typeid(Type6).name() << std::endl;
    std::cout << "std::common_type<A, C, B>::type:              "
              << typeid(Type7).name() << std::endl;

    return 0;
}
