//#include <memory>
//#include <iostream>
//#include <string>

//int main()
//{
//    std::allocator<int> a1;   // int 的默认分配器
//    int* a = a1.allocate(1);  // 一个 int 的空间
//    a1.construct(a, 7);       // 构造 int
//    std::cout << a[0] << '\n';
//    a1.deallocate(a, 1);      // 解分配一个 int 的空间

//    // string 的默认分配器
//    std::allocator<std::string> a2;

//    // 同上，但以 a1 的重绑定获取
//    decltype(a1)::rebind<std::string>::other a2_1;

//    // 同上，但通过 allocator_traits 由类型 a1 的重绑定获取
//    std::allocator_traits<decltype(a1)>::rebind_alloc<std::string> a2_2;

//    std::string* s = a2.allocate(2); // 2 个 string 的空间

//    a2.construct(s, "foo");
//    a2.construct(s + 1, "bar");

//    std::cout << s[0] << ' ' << s[1] << '\n';

//    a2.destroy(s);
//    a2.destroy(s + 1);
//    a2.deallocate(s, 2);
//}

//#include <memory>
//#include <iostream>
//#include <string>

//struct Foo
//{
//    Foo() {}
//};

//int main()
//{
//    std::allocator<char> ac;            // char 的默认分配器
//    std::allocator<int> ai;             // int 的默认分配器
//    std::allocator<double> ad;          // double 的默认分配器
//    std::allocator<std::string> as;     // string 的默认分配器
//    std::allocator<Foo> af;             // Foo 的默认分配器
//    std::cout << "char max_size()       " << ac.max_size() << '\n';
//    std::cout << "int max_size()        " << ai.max_size() << '\n';
//    std::cout << "double max_size()     " << ad.max_size() << '\n';
//    std::cout << "as max_size()         " << as.max_size() << '\n';
//    std::cout << "Foo max_size()        " << af.max_size() << '\n';
//}

#include <memory>
#include <iostream>
#include <string>

int main()
{
    std::allocator<int> ai;   // int 的默认分配器
    int* a = ai.allocate(10);  // 10个 int 的空间
    for (int i = 0; i < 10; i++)
    {
        ai.construct(a + i, i);       // 构造 int
    }
    for (int i = 0; i < 10; i++)
    {
        std::cout << a[i] << '\n';
    }
    ai.deallocate(a, 10);      // 解分配10个 int 的空间

    std::allocator<std::string> as;   // string 的默认分配器
    std::string * s = as.allocate(10);  // 10个 string 的空间
    for (int i = 0; i < 10; i++)
    {
        as.construct(s + i, std::to_string(i) + "-string"); // 构造 int
    }
    for (int i = 0; i < 10; i++)
    {
        std::cout << s[i] << '\n';
    }
    as.deallocate(s, 10);      // 解分配10个 int 的空间
}
