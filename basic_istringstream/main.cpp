#include <sstream>
#include <string>
#include <iostream>

int main()
{
    //3) 以 str 的副本为底层字符串设备的初始内容。
    std::string string1 = "I am a handsome programmer";
    std::basic_istringstream<char> basic_istringstream1(string1, std::ios_base::in);
    std::cout << "basic_istringstream1: " << basic_istringstream1.str() << std::endl;
    std::cout << "basic_istringstream1 rdstate: ";
    std::cout << basic_istringstream1.rdstate() << std::endl;

    std::string string2 = "I am a super handsome guy";
    std::basic_istringstream<char> basic_istringstream2(string2, std::ios_base::in);
    std::cout << "basic_istringstream2: " << basic_istringstream2.str() << std::endl;
    basic_istringstream2.setstate(std::ios_base::badbit);
    std::cout << "basic_istringstream2 rdstate: ";
    std::cout << basic_istringstream2.rdstate() << std::endl;
    std::cout << std::endl;

    //为 std::basic_istringstream 特化 std::swap 算法。
    //交换 lhs 与 rhs 的状态。等效地调用 lhs.swap(rhs)
    std::swap(basic_istringstream1, basic_istringstream2);
    std::cout << "after swap: " << std::endl;

    basic_istringstream1.seekg(std::ios_base::beg);
    std::cout << "basic_istringstream1: " << basic_istringstream1.str() << std::endl;
    std::cout << "basic_istringstream1 rdstate: ";
    std::cout << basic_istringstream1.rdstate() << std::endl;

    basic_istringstream2.seekg(std::ios_base::beg);
    std::cout << "basic_istringstream2: " << basic_istringstream2.str() << std::endl;
    std::cout << "basic_istringstream2 rdstate: ";
    std::cout << basic_istringstream2.rdstate() << std::endl;

    return 0;
}


//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_istringstream<char> basic_istringstream1(string1, std::ios_base::in);
//    //1) 返回底层字符串的副本，如同通过调用 rdbuf()->str() 。
//    std::cout << "basic_istringstream1: " << basic_istringstream1.str() << std::endl;
//    //2) 替换底层字符串，如同通过调用 rdbuf()->str(new_str) 。
//    std::string string2 = "I am a super handsome guy";
//    basic_istringstream1.str(string2);
//    std::cout << "basic_istringstream1: " << basic_istringstream1.str() << std::endl;

//    return 0;
//}

//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_istringstream<char> basic_istringstream1(string1, std::ios_base::in);
//    std::cout << "basic_istringstream1: " << basic_istringstream1.rdbuf() << std::endl;
//    std::cout << "basic_istringstream1 rdstate: ";
//    std::cout << basic_istringstream1.rdstate() << std::endl;

//    std::string string2 = "I am a super handsome guy";
//    std::basic_istringstream<char> basic_istringstream2(string2, std::ios_base::in);
//    std::cout << "basic_istringstream2: " << basic_istringstream2.rdbuf() << std::endl;
//    std::cout << "basic_istringstream2 rdstate: ";
//    std::cout << basic_istringstream2.rdstate() << std::endl;
//    std::cout << std::endl;

//    //交换流与 other 的状态。
//    //通过调用 basic_istream<CharT, Traits>::swap(other)
//    //和 rdbuf()->swap(*other.rdbuf()) 进行。
//    basic_istringstream1.swap(basic_istringstream2);
//    std::cout << "after swap: " << std::endl;

//    basic_istringstream1.seekg(std::ios_base::beg);
//    std::cout << "basic_istringstream1: " << basic_istringstream1.rdbuf() << std::endl;
//    std::cout << "basic_istringstream1 rdstate: ";
//    std::cout << basic_istringstream1.rdstate() << std::endl;

//    basic_istringstream2.seekg(std::ios_base::beg);
//    std::cout << "basic_istringstream2: " << basic_istringstream2.rdbuf() << std::endl;
//    std::cout << "basic_istringstream2 rdstate: ";
//    std::cout << basic_istringstream2.rdstate() << std::endl;

//    return 0;
//}


//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_istringstream<char> basic_istringstream1(string1, std::ios_base::in);
//    std::cout << "basic_istringstream1: " << basic_istringstream1.rdbuf() << std::endl;
//    std::cout << "basic_istringstream1 rdstate: ";
//    std::cout << basic_istringstream1.rdstate() << std::endl;
//    basic_istringstream1.setstate(std::ios_base::failbit);
//    std::cout << "basic_istringstream1 rdstate: ";
//    std::cout << basic_istringstream1.rdstate() << std::endl;

//    //注意，基类移动赋值交换 *this 与 other 间的所有流状态变量（除了 rdbuf ）。
//    std::basic_istringstream<char> basic_istringstream2 = std::move(basic_istringstream1);
//    std::cout << "basic_istringstream2 rdstate: ";
//    std::cout << basic_istringstream2.rdstate() << std::endl;
//    basic_istringstream2.setstate(std::ios_base::badbit);
//    std::cout << "basic_istringstream2 rdstate: ";
//    std::cout << basic_istringstream2.rdstate() << std::endl;

//    return 0;
//}

//int main()
//{
//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_istringstream<char> basic_istringstream1(string1, std::ios_base::in);
//    std::cout << "basic_istringstream1: " << basic_istringstream1.rdbuf() << std::endl;
//    std::cout << "rdstate: " << basic_istringstream1.rdstate() << std::endl;

//    std::string string2 = "I am a super handsome guy";
//    std::basic_istringstream<char> basic_istringstream2(string2, std::ios_base::in);
//    std::cout << "basic_istringstream2: " << basic_istringstream2.rdbuf() << std::endl;
//    basic_istringstream2.setstate(std::ios_base::badbit);
//    std::cout << "rdstate: " << basic_istringstream2.rdstate() << std::endl;

//    return 0;
//}

//int main()
//{
//    //1) 默认构造函数。以默认打开模式构造新的底层字符串设备。
//    std::basic_istringstream<int> basic_istringstream1;

//    //2) 构造新的底层字符串设备。
//    //以 basic_stringbuf<Char,Traits,Allocator>(mode | ios_base::in)
//    //构造底层 basic_stringbuf 对象。
//    std::basic_istringstream<char> basic_istringstream2(std::ios_base::in);

//    //3) 以 str 的副本为底层字符串设备的初始内容。
//    std::string string1 = "I am a handsome programmer";
//    std::basic_istringstream<char> basic_istringstream3(string1, std::ios_base::in);
//    std::cout << "basic_istringstream3: " << basic_istringstream3.rdbuf() << std::endl;
//    basic_istringstream3.seekg(std::ios_base::beg);
//    while (!basic_istringstream3.eof())
//    {
//        char ch;
//        basic_istringstream3 >> ch;
//        std::cout << ch;
//    }
//    std::cout << std::endl;

//    basic_istringstream3.seekg(std::ios_base::beg);
//    //4) 移动构造函数。用移动语义，构造拥有 other 的状态的字符串流。
//    std::basic_istringstream<char> basic_istringstream4(std::move(basic_istringstream3));
//    basic_istringstream4.seekg(std::ios_base::beg);
//    std::cout << "basic_istringstream4: " << basic_istringstream4.rdbuf() << std::endl;

//    return 0;
//}
