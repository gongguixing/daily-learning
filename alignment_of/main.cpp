#include <iostream>
#include <type_traits>

class A {};

class B
{
    int b;
};

class C
{
    int b;
    double c;
};

int main()
{
    std::cout << "std::alignment_of<int>::value:        "
              << std::alignment_of<int>::value << std::endl;
    std::cout << "std::alignment_of<double>::value:     "
              << std::alignment_of<double>::value << std::endl;
    std::cout << "std::alignment_of<char>::value:       "
              << std::alignment_of<char>::value << std::endl;
    std::cout << "std::alignment_of<uint8_t>::value:    "
              << std::alignment_of<uint8_t>::value << std::endl;
    std::cout << "std::alignment_of<uint64_t>::value:   "
              << std::alignment_of<uint64_t>::value << std::endl;
    std::cout << "std::alignment_of<std::string>::value:"
              << std::alignment_of<std::string>::value << std::endl;
    std::cout << "std::alignment_of<A>::value:          "
              << std::alignment_of<A>::value << std::endl;

    std::cout << "std::alignment_of<A>():               "
              << std::alignment_of<A>() << std::endl; // 另一种语法
    std::cout << "std::alignment_of<B>():               "
              << std::alignment_of<B>() << std::endl; // 另一种语法
    std::cout << "std::alignment_of<C>():               "
              << std::alignment_of<C>() << std::endl; // 另一种语法

    return 0;
}
