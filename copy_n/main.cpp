#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

int main()
{
    std::vector<int> in = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::cout << "in :  ";
    std::copy(in.begin(), in.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::vector<int> out;
    std::copy_n(in.begin(), 5, std::back_inserter(out));
    std::cout << "out : ";
    std::copy(out.begin(), out.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::vector<int> out1(6);
    std::copy_n(in.begin(), out1.size(), std::begin(out1));
    std::cout << "out1: ";
    std::copy(out1.begin(), out1.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}
