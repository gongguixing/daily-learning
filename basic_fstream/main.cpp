#include <fstream>
#include <utility>
#include <string>
#include <iostream>

int main()
{
    std::fstream fstream1("test1.txt", std::ios::out);
    std::cout << "fstream1 is: "
              << (fstream1 ? "true" : "false") << std::endl;
    std::fstream fstream2("test2.txt", std::ios::out);
    std::cout << "fstream2 is: "
              << (fstream2 ? "true" : "false") << std::endl;
    std::cout << std::endl;

    fstream1 << "hello 1" << " ";
    fstream2 << "hello 2" << " ";
    //为 std::basic_fstream 特化 std::swap 算法。
    //交换 lhs 与 rhs 的状态。等效地调用 lhs.swap(rhs) 。
    std::cout << "std::swap(fstream1, fstream2) " << std::endl;
    std::swap(fstream1, fstream2);
    fstream1 << "hello 1" << " ";
    fstream2 << "hello 2" << " ";
    fstream1.close();
    fstream2.close();
    std::cout << std::endl;

    std::fstream fstream3("test1.txt", std::ios::in);
    std::cout << "fstream3 is: "
              << (fstream3.is_open() ? "true" : "false") << std::endl;
    if (fstream3.is_open())
    {
        std::cout << fstream3.rdbuf() << std::endl;
    }
    std::cout << std::endl;

    std::fstream fstream4("test2.txt", std::ios::in);
    std::cout << "fstream2 is: "
              << (fstream4.is_open() ? "true" : "false") << std::endl;
    if (fstream4.is_open())
    {
        std::cout << fstream4.rdbuf() << std::endl;
    }
    std::cout << std::endl;

    return 0;
}

//int main()
//{
//    std::fstream fstream1("test1.txt", std::ios::in);
//    std::fstream fstream2("test2.txt", std::ios::in);
//    std::fstream fstream3("test3.txt", std::ios::in);
//    std::cout << "fstream1 is: "
//              << (fstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream2 is: "
//              << (fstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    std::cout << "std::fstream close" << std::endl;
//    fstream1.close();
//    fstream2.close();
//    fstream3.close();

//    std::cout << std::endl;

//    std::cout << "fstream1 is: "
//              << (fstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream2 is: "
//              << (fstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::string strFileName1 = "test1.txt";
//    std::fstream fstream1;
//    //1-2) 等效地调用 rdbuf()->open(filename, mode ).
//    fstream1.open(strFileName1.c_str(), std::ios::in | std::ios::out);

//    std::fstream fstream2;
//    std::string strFileName2 = "test2.txt";
//    //3-4) 等效地调用 (1-2) ，如同以 open(filename.c_str(), mode) 。
//    fstream2.open(strFileName2, std::ios::in | std::ios::out);

//    std::fstream fstream3;
//    std::string strFileName3 = "test3.txt";
//    fstream2.open(strFileName3, std::ios::in | std::ios::out);

//    std::cout << "fstream1 is: "
//              << (fstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream2 is: "
//              << (fstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::fstream fstream1("test1.txt", std::ios::in | std::ios::out);
//    std::fstream fstream2("test2.txt", std::ios::in | std::ios::out);
//    std::fstream fstream3("test3.txt", std::ios::in | std::ios::out);

//    std::cout << "fstream1 is: "
//              << (fstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream2 is: "
//              << (fstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::fstream fstream1("test1.txt", std::ios::out);
//    std::cout << "fstream1 is: "
//              << (fstream1 ? "true" : "false") << std::endl;
//    std::fstream fstream2("test2.txt", std::ios::out);
//    std::cout << "fstream2 is: "
//              << (fstream2 ? "true" : "false") << std::endl;
//    std::cout << std::endl;

//    fstream1 << "hello 1" << " ";
//    fstream2 << "hello 2" << " ";
//    //交换流与 other 的状态。
//    //通过调用 basic_ostream<CharT, Traits>::swap(other)
//    //和 rdbuf()->swap(other.rdbuf()) 进行。
//    std::cout << "fstream1.swap(fstream2) " << std::endl;
//    fstream1.swap(fstream2);
//    fstream1 << "hello 1" << " ";
//    fstream2 << "hello 2" << " ";
//    fstream1.close();
//    fstream2.close();
//    std::cout << std::endl;

//    std::fstream fstream3("test1.txt", std::ios::in);
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;
//    if (fstream3.is_open())
//    {
//        std::cout << fstream3.rdbuf() << std::endl;
//    }
//    std::cout << std::endl;

//    std::fstream fstream4("test2.txt", std::ios::in);
//    std::cout << "fstream2 is: "
//              << (fstream4.is_open() ? "true" : "false") << std::endl;
//    if (fstream4.is_open())
//    {
//        std::cout << fstream4.rdbuf() << std::endl;
//    }
//    std::cout << std::endl;

//    return 0;
//}

//int main()
//{
//    std::fstream fstream1("test1.txt", std::ios::out);
//    std::cout << "fstream1 is: "
//              << (fstream1 ? "true" : "false") << std::endl;
//    fstream1 << "hello" << " ";
//    std::cout << std::endl;

//    //移动赋值文件流 other 给 *this ，
//    //等效地移动赋值 std::basic_ostream 基类和关联的 std::basic_filebuf 。
//    std::fstream fstream2 = std::move(fstream1);
//    std::cout << "fstream1 is: "
//              << (fstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "fstream2 is: "
//              << (fstream2.is_open() ? "true" : "false") << std::endl;
//    fstream2 << "word" << "!";
//    std::cout << std::endl;
//    fstream1.close();
//    fstream2.close();

//    std::fstream fstream3("test1.txt", std::ios::in);
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;
//    if (fstream3.is_open())
//    {
//        std::cout << fstream3.rdbuf() << std::endl;
//    }
//    std::cout << std::endl;

//    return 0;
//}

//int main()
//{
//    //1) 默认构造函数：构造不关联到文件的流
//    std::fstream fstream1;
//    std::cout << "fstream1 is: "
//              << (fstream1.is_open() ? "true" : "false") << std::endl;

//    //2-3) 首先，进行同默认构造函数的步骤，
//    std::string strFileName2 = "test2.txt";
//    std::fstream fstream2(strFileName2.c_str(), std::ios::out);
//    std::cout << "fstream2 is: "
//              << (fstream2.is_open() ? "true" : "false") << std::endl;

//    //4-5) 同 basic_fstream(filename.c_str(), mode) 。
//    std::string strFileName3 = "test3.txt";
//    std::fstream fstream3(strFileName3, std::ios::out);
//    std::cout << "fstream3 is: "
//              << (fstream3.is_open() ? "true" : "false") << std::endl;

//    //6) 移动构造函数：首先，从 other 移动构造基类（这不影响 rdbuf() 指针）
//    std::fstream fstream4(std::move(strFileName3));
//    std::cout << "fstream4 is: "
//              << (fstream4.is_open() ? "true" : "false") << std::endl;

//    //7) 复制构造函数被删除：此类不可复制。
//    return 0;
//}
