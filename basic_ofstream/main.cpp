#include <fstream>
#include <utility>
#include <string>
#include <iostream>

int main()
{
    std::ofstream ofstream1("test1.txt", std::ios::out);
    std::cout << "ofstream1 is: "
              << (ofstream1 ? "true" : "false") << std::endl;
    std::ofstream ofstream2("test2.txt", std::ios::out);
    std::cout << "ofstream2 is: "
              << (ofstream2 ? "true" : "false") << std::endl;
    std::cout << std::endl;

    ofstream1 << "hello 1" << " ";
    ofstream2 << "hello 2" << " ";
    //为 std::basic_ofstream 特化 std::swap 算法。
    //交换 lhs 与 rhs 的状态。等效地调用 lhs.swap(rhs) 。
    std::cout << "std::swap(ofstream1, ofstream2) " << std::endl;
    std::swap(ofstream1, ofstream2);
    ofstream1 << "hello 1" << " ";
    ofstream2 << "hello 2" << " ";
    ofstream1.close();
    ofstream2.close();
    std::cout << std::endl;

    std::ifstream ifstream1("test1.txt", std::ios::in);
    std::cout << "ifstream1 is: "
              << (ifstream1.is_open() ? "true" : "false") << std::endl;
    if (ifstream1.is_open())
    {
        std::cout << ifstream1.rdbuf() << std::endl;
    }
    std::cout << std::endl;

    std::ifstream ifstream2("test2.txt", std::ios::in);
    std::cout << "ifstream2 is: "
              << (ifstream2.is_open() ? "true" : "false") << std::endl;
    if (ifstream2.is_open())
    {
        std::cout << ifstream2.rdbuf() << std::endl;
    }
    std::cout << std::endl;

    return 0;
}

//int main()
//{
//    std::ofstream ofstream1("test1.txt", std::ios::in);
//    std::ofstream ofstream2("test2.txt", std::ios::in);
//    std::ofstream ofstream3("test3.txt", std::ios::in);
//    std::cout << "ofstream1 is: "
//              << (ofstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream2 is: "
//              << (ofstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream3 is: "
//              << (ofstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    std::cout << "std::ofstream close" << std::endl;
//    ofstream1.close();
//    ofstream2.close();
//    ofstream3.close();

//    std::cout << std::endl;

//    std::cout << "ofstream1 is: "
//              << (ofstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream2 is: "
//              << (ofstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream3 is: "
//              << (ofstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::string strFileName1 = "test1.txt";
//    std::ofstream ofstream1;
//    //1-2) 等效地调用 rdbuf()->open(filename, mode | ios_base::out).
//    ofstream1.open(strFileName1.c_str(), std::ios::out);

//    std::ofstream ofstream2;
//    std::string strFileName2 = "test2.txt";
//    //3-4) 等效地调用 (1-2) ，如同以 open(filename.c_str(), mode) 。
//    ofstream2.open(strFileName2, std::ios::out);

//    std::ofstream ofstream3;
//    std::string strFileName3 = "test3.txt";
//    ofstream2.open(strFileName3, std::ios::out);

//    std::cout << "ofstream1 is: "
//              << (ofstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream2 is: "
//              << (ofstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream3 is: "
//              << (ofstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::ofstream ofstream1("test1.txt", std::ios::in);
//    std::ofstream ofstream2("test2.txt", std::ios::in);
//    std::ofstream ofstream3("test3.txt", std::ios::in);
//    std::cout << "ofstream1 is: "
//              << (ofstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream2 is: "
//              << (ofstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream3 is: "
//              << (ofstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::ofstream ofstream1("test1.txt", std::ios::out);
//    std::cout << "ofstream1 is: " << (ofstream1 ? "true" : "false") << std::endl;
//    std::ofstream ofstream2("test2.txt", std::ios::out);
//    std::cout << "ofstream2 is: " << (ofstream2 ? "true" : "false") << std::endl;
//    std::cout << std::endl;

//    ofstream1 << "hello 1" << " ";
//    ofstream2 << "hello 2" << " ";
//    //交换流与 other 的状态。
//    //通过调用 basic_ostream<CharT, Traits>::swap(other) 和 rdbuf()->swap(other.rdbuf()) 进行。
//    std::cout << "ofstream1.swap(ofstream2) " << std::endl;
//    ofstream1.swap(ofstream2);
//    ofstream1 << "hello 1" << " ";
//    ofstream2 << "hello 2" << " ";
//    ofstream1.close();
//    ofstream2.close();
//    std::cout << std::endl;

//    std::ifstream ifstream1("test1.txt", std::ios::in);
//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    if (ifstream1.is_open())
//    {
//        std::cout << ifstream1.rdbuf() << std::endl;
//    }
//    std::cout << std::endl;

//    std::ifstream ifstream2("test2.txt", std::ios::in);
//    std::cout << "ifstream2 is: " << (ifstream2.is_open() ? "true" : "false") << std::endl;
//    if (ifstream2.is_open())
//    {
//        std::cout << ifstream2.rdbuf() << std::endl;
//    }
//    std::cout << std::endl;

//    return 0;
//}

//int main()
//{
//    std::ofstream ofstream1("test1.txt", std::ios::out);
//    std::cout << "ofstream1 is: " << (ofstream1 ? "true" : "false") << std::endl;
//    ofstream1 << "hello" << " ";
//    std::cout << std::endl;

//    //移动赋值文件流 other 给 *this ，等效地移动赋值 std::basic_ostream 基类和关联的 std::basic_filebuf 。
//    std::ofstream ofstream2 = std::move(ofstream1);
//    std::cout << "ofstream1 is: " << (ofstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ofstream2 is: " << (ofstream2.is_open() ? "true" : "false") << std::endl;
//    ofstream2 << "word" << "!";
//    std::cout << std::endl;
//    ofstream1.close();
//    ofstream2.close();

//    std::ifstream ifstream1("test1.txt", std::ios::in);
//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    if (ifstream1.is_open())
//    {
//        std::cout << ifstream1.rdbuf() << std::endl;
//    }
//    std::cout << std::endl;

//    return 0;
//}


//int main()
//{
//    //1) 默认构造函数：构造不关联到文件的流
//    std::ofstream ofstream1;
//    std::cout << "ofstream1 is: " << (ofstream1.is_open() ? "true" : "false") << std::endl;

//    //2-3) 首先，进行同默认构造函数的步骤，
//    std::string strFileName2 = "test2.txt";
//    std::ofstream ofstream2(strFileName2.c_str(), std::ios::out);
//    std::cout << "ofstream2 is: " << (ofstream2.is_open() ? "true" : "false") << std::endl;

//    //4-5) 同 basic_ofstream(filename.c_str(), mode) 。
//    std::string strFileName3 = "test3.txt";
//    std::ofstream ofstream3(strFileName3, std::ios::out);
//    std::cout << "ofstream3 is: " << (ofstream3.is_open() ? "true" : "false") << std::endl;

//    //6) 移动构造函数：首先，从 other 移动构造基类（这不影响 rdbuf() 指针）
//    std::ofstream ofstream4(std::move(strFileName3));
//    std::cout << "ofstream4 is: " << (ofstream4.is_open() ? "true" : "false") << std::endl;

//    //7) 复制构造函数被删除：此类不可复制。
//    return 0;
//}
