#include "customcombox.h"

CustomComBox::CustomComBox(QWidget *parent)
    : QComboBox(parent)
{
    m_pListWidget = new QListWidget();

     this->setModel(m_pListWidget->model());
     this->setView(m_pListWidget);
     this->setEditable(true);
}

CustomComBox::~CustomComBox()
{

}

void CustomComBox::addComBoxItem(CustomComBoxItem *item)
{
    QListWidgetItem *list_item = new QListWidgetItem(m_pListWidget);
    m_pListWidget->setItemWidget(list_item, item);
    connect(item, SIGNAL(sgShow(QString)), this, SLOT(slSetEditText(QString)));
    connect(item, SIGNAL(sgRemove(QString)), this, SLOT(slRemove(QString)));
}

void CustomComBox::slSetEditText(QString text)
{
    this->setEditText(text);
    this->hidePopup();
}

void CustomComBox::slRemove(QString text)
{
    this->hidePopup();
    int count = m_pListWidget->count();
    for(int i = 0; i < count; i++)
    {
        QListWidgetItem *item = m_pListWidget->item(i);
        CustomComBoxItem *ComBoxItem = (CustomComBoxItem *)(m_pListWidget->itemWidget(item));
        QString rtext = ComBoxItem->text();
        if(rtext == text)
        {
            m_pListWidget->takeItem(i);
            delete item;
            break;
        }
    }
}

