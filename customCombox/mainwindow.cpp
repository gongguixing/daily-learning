#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->widget->setText("GGX");
//    QPixmap pixmap = QPixmap("./Head/GGX.jpg");
//    ui->widget->setPixmap(pixmap);

    CustomComBoxItem *item = new CustomComBoxItem("GGX1");
    item->setGeometry(QRect(160, 20, 120, 80));
    ui->comboBox->addComBoxItem(item);
    ui->comboBox->addComBoxItem(ui->widget);
}

MainWindow::~MainWindow()
{
    delete ui;
}
