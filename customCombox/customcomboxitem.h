#ifndef CUSTOMCOMBOXITEM_H
#define CUSTOMCOMBOXITEM_H


#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QEvent>
#include <QMouseEvent>

class CustomComBoxItem : public QWidget
{
    Q_OBJECT

public:
    explicit CustomComBoxItem(QWidget *parent = 0);
    explicit CustomComBoxItem(const QString text, QWidget *parent = 0);
    explicit CustomComBoxItem(const QString text, QPixmap Pixmap, QWidget *parent = 0);

    ~CustomComBoxItem();

    void setText(const QString text);
    QString text() const;
    void setPixmap(const QPixmap Pixmap);

private:
    void init();

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:
    bool mousePress;
    QLabel *m_pLabel;
    QPushButton *m_pPushbutton;

signals:
    void sgShow(QString text);
    void sgRemove(QString text);

private slots:
    void sl_pushbutton_clicked();
};

#endif // CUSTOMCOMBOXITEM_H
