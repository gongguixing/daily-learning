#include "customcomboxitem.h"

CustomComBoxItem::CustomComBoxItem(QWidget *parent)
    : QWidget(parent)
{
    init();
}

CustomComBoxItem::CustomComBoxItem(const QString text, QWidget *parent)
    : QWidget(parent)
{
    init();
    m_pLabel->setText(text);
}

CustomComBoxItem::CustomComBoxItem(const QString text, QPixmap Pixmap, QWidget *parent)
    : QWidget(parent)
{
    init();
    m_pLabel->setText(text);
    m_pLabel->setPixmap(Pixmap);
}

CustomComBoxItem::~CustomComBoxItem()
{
    delete m_pLabel;
    delete m_pPushbutton;
}

void CustomComBoxItem::setText(const QString text)
{
    m_pLabel->setText(text);
}

QString CustomComBoxItem::text() const
{
    return m_pLabel->text();
}

void CustomComBoxItem::setPixmap(const QPixmap Pixmap)
{
    m_pLabel->setPixmap(Pixmap);
}

void CustomComBoxItem::init()
{
    mousePress = false;
    m_pLabel = new QLabel();
    m_pPushbutton = new QPushButton();
    QPixmap pixmap("./Head/Delete.png");
    m_pPushbutton->setIcon(pixmap);
    m_pPushbutton->setIconSize(pixmap.size());
    m_pPushbutton->setStyleSheet("background:transparent;");
    connect(m_pPushbutton, SIGNAL(clicked()), this, SLOT(sl_pushbutton_clicked()));

    QHBoxLayout *main_layout = new QHBoxLayout();
    main_layout->addWidget(m_pLabel);
    main_layout->addStretch();
    main_layout->addWidget(m_pPushbutton);
    main_layout->setContentsMargins(5, 5, 5, 5);
    main_layout->setSpacing(5);
    this->setLayout(main_layout);
}

void CustomComBoxItem::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        mousePress = true;
    }
}

void CustomComBoxItem::mouseReleaseEvent(QMouseEvent *event)
{
    if(mousePress)
    {
      QString str = m_pLabel->text();
        emit sgShow(str);
        mousePress = false;
    }
}

void CustomComBoxItem::sl_pushbutton_clicked()
{
    emit sgRemove(m_pLabel->text());
}
