#ifndef CUSTOMCOMBOX_H
#define CUSTOMCOMBOX_H

#include <QWidget>
#include <QComboBox>
#include <QListWidget>
#include <QListWidgetItem>

#include "customcomboxitem.h"


class CustomComBox : public QComboBox
{
    Q_OBJECT

public:
    explicit CustomComBox(QWidget *parent = 0);
    ~CustomComBox();

    void addComBoxItem(CustomComBoxItem *item);

private:
    QListWidget *m_pListWidget;
    QListWidgetItem *m_pListWidgetItem;

private slots:
    void slSetEditText(QString text);
    void slRemove(QString text);
};

#endif // CUSTOMCOMBOX_H
