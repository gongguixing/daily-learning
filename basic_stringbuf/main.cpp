#include <sstream>
#include <string>
#include <iostream>

int main()
{
    std::basic_stringbuf<char> one("one", std::ios_base::in
                                   | std::ios_base::out
                                   | std::ios_base::ate);

    std::basic_stringbuf<char> two("two", std::ios_base::in
                                   | std::ios_base::out
                                   | std::ios_base::ate);

    std::cout << "Before move, one = \"" << one.str() << '"'
              << " two = \"" << two.str() << "\"" << std::endl;

    //为 std::basic_stringbuf 特化 std::swap 算法。
    //交换 lhs 与 rhs 的内部状态。等效地调用 lhs.swap(rhs) 。
    std::swap(one, two);

    std::cout << "After swap, one = \"" << one.str() << '"'
              << " two = \"" << two.str() << "\"" << std::endl;

    return 0;
}

//  typedef basic_stringbuf<char> 	stringbuf;
//struct mybuf : std::stringbuf
//{
//    mybuf(const std::string& new_str,
//          std::ios_base::openmode which =
//              std::ios_base::in | std::ios_base::out)
//        : std::stringbuf(new_str, which) {}

//    pos_type tellp()
//    {
//        char ch = *pptr();
//        return ch;
//    }

//    pos_type tellg()
//    {
//        char ch = *gptr();
//        return ch;
//    }

//    pos_type seekpos(pos_type sp, std::ios_base::openmode which)
//    {
//        std::cout << "Before seekpos(" << sp
//                  << "), size of the get area is "
//                  << egptr() - eback() << " with "
//                  << egptr() - gptr()
//                  << " read positions available" << std::endl;

//        pos_type rc = std::stringbuf::seekpos(sp, which);

//        std::cout << "seekpos() returns " << rc
//                  << ".\nAfter the call, "
//                  << "size of the get area is "
//                  << egptr() - eback() << " with "
//                  << egptr() - gptr()
//                  << " read positions available" << std::endl;

//        return rc;
//    }

//};

//int main()
//{
//    mybuf buf("12345");
//    std::iostream stream(&buf);
//    stream.seekg(2);

//    return 0;
//}

//int main()
//{
//    mybuf sbuf("123"); // 入/出
//    std::cout << "put pos = " << sbuf.tellp()
//              << " get pos = " << sbuf.tellg() << std::endl;

//    // 两个指针绝对寻位
//    sbuf.pubseekoff(1, std::ios_base::beg); // 都前移 1
//    std::cout << "put pos = " << sbuf.tellp()
//              << " get pos = " << sbuf.tellg() << std::endl;

//    // 试图从当前位置前移两个指针 1
//    if (-1 == sbuf.pubseekoff(1, std::ios_base::cur))
//    {
//        std::cout << "moving both pointers from current position failed\n";
//    }
//    std::cout << "put pos = " << sbuf.tellp()
//              << " get pos = " << sbuf.tellg() << std::endl;

//    // 前移写指针 1 ，但不前移读指
//    // can also be called as ss.seekp(1, std::ios_base::cur);
//    sbuf.pubseekoff(1, std::ios_base::cur, std::ios_base::out);
//    std::cout << "put pos = " << sbuf.tellp()
//              << " get pos = " << sbuf.tellg() << std::endl;

//    sbuf.sputc('a'); // 写入输出位置
//    std::cout << "Wrote 'a' at put position, the buffer is now "
//              << sbuf.str() << std::endl;

//    char ch = sbuf.sgetc();
//    std::cout << "reading at get position gives '"
//              << ch << "'" << std::endl;
//    return 0;
//}

//int main()
//{
//    std::stringbuf sbuf;
//    char c[1024] = {};
//    sbuf.pubsetbuf(c, 1024);
//    std::iostream stream(&sbuf);
//    stream << 3.14 << std::endl;
//    std::cout << c << std::endl;
//    return 0;
//}

//  typedef basic_stringbuf<char> 	stringbuf;
//struct mybuf : std::stringbuf
//{
//    mybuf(const std::string& new_str,
//          std::ios_base::openmode which =
//              std::ios_base::in | std::ios_base::out)
//        : std::stringbuf(new_str, which) {}

//    int_type overflow(int_type c = EOF) override
//    {
//        std::cout << "stringbuf::overflow('"
//                  << char(c) << "') called" << std::endl
//                  << "Before: size of get area: "
//                  << egptr() - eback() << std::endl
//                  << "        size of put area: "
//                  << epptr() - pbase() << std::endl;

//        int_type ret = std::stringbuf::overflow(c);

//        std::cout << "After : size of get area: "
//                  << egptr() - eback() << std::endl
//                  << "        size of put area: "
//                  << epptr() - pbase() << std::endl;

//        return ret;
//    }
//};

//int main()
//{
//    std::cout << "read-write stream:" << std::endl;
//    mybuf sbuf("   "); // 只读流
//    std::iostream stream(&sbuf);
//    stream << 1234;
//    std::cout << sbuf.str() << std::endl;

//    std::cout << std::endl << "read-only stream:" << std::endl;
//    mybuf ro_buf("   ", std::ios_base::in); // 只读流
//    std::iostream ro_stream(&ro_buf);
//    ro_stream << 1234;

//    std::cout << std::endl << "write-only stream:" << std::endl;
//    mybuf wr_buf("   ", std::ios_base::out); // 只写流
//    std::iostream wr_stream(&wr_buf);
//    wr_stream << 1234;

//    return 0;
//}

//struct mybuf : std::stringbuf
//{
//    mybuf(const std::string& new_str,
//          std::ios_base::openmode which =
//              std::ios_base::in | std::ios_base::out)
//        : std::stringbuf(new_str, which) {}

//    int_type pbackfail(int_type c) override
//    {
//        std::cout << "Before pbackfail(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        int_type ch = std::stringbuf::pbackfail(c);

//        std::cout << "After pbackfail(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        if (ch == EOF)
//        {
//            std::cout << "pbackfail() returns EOF\n";
//        }
//        else
//        {
//            std::cout << "pbackfail() returns '"
//                      << char(ch) << "'" << std::endl;
//        }

//        return ch;
//    }
//};

//int main()
//{
//    mybuf sbuf("12345"); // 读写流

//    sbuf.sungetc();
//    std::cout << std::endl;
//    sbuf.sputbackc('5');
//    return 0;
//}

//struct mybuf : std::stringbuf
//{
//    mybuf(const std::string& new_str,
//          std::ios_base::openmode which =
//              std::ios_base::in | std::ios_base::out)
//        : std::stringbuf(new_str, which) {}

//    int_type overflow(int_type c)
//    {
//        std::cout << "Before overflow(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " the put area size is "
//                  << epptr() - pbase() << std::endl;

//        int_type rc = std::stringbuf::overflow(c);

//        std::cout << "After overflow(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        return rc;
//    }

//    int_type underflow()
//    {
//        std::cout << "Before underflow(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        int_type ch = std::stringbuf::underflow();

//        std::cout << "After underflow(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        if (ch == EOF)
//        {
//            std::cout << "underflow() returns EOF\n";
//        }
//        else
//        {
//            std::cout << "underflow() returns '"
//                      << char(ch) << "'" << std::endl;
//        }

//        return ch;
//    }

//    int_type pbackfail(int_type c) override
//    {
//        std::cout << "Before pbackfail(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        int_type ch = std::stringbuf::pbackfail(c);

//        std::cout << "After pbackfail(): get area size is "
//                  << egptr() - eback() << ' '
//                  << " put area size is "
//                  << epptr() - pbase() << std::endl;

//        if (ch == EOF)
//        {
//            std::cout << "pbackfail() returns EOF\n";
//        }
//        else
//        {
//            std::cout << "pbackfail() returns '"
//                      << char(ch) << "'" << std::endl;
//        }

//        return ch;
//    }
//};

//int main()
//{
//    char n;

//    std::basic_stringbuf<char> in;  // 亦能用 in("1 2")
//    in.str("1 2"); // 设置获取区
//    n = in.sgetc();
//    std::cout << "after reading the first char from \"1 2\", the char is "
//              << n << ", str() = \"" << in.str() << "\"" << std::endl; // 或 in.str()

//    std::basic_stringbuf<char> out("1 2");
//    out.sputc('3');
//    std::cout << "after writing the char '3' to output stream \"1 2\""
//              << ", str() = \"" << out.str() << "\"" << std::endl;

//    return 0;
//}

//int main()
//{
//    std::basic_stringbuf<char> one("one", std::ios_base::in
//                                   | std::ios_base::out
//                                   | std::ios_base::ate);

//    std::basic_stringbuf<char> two("two", std::ios_base::in
//                                   | std::ios_base::out
//                                   | std::ios_base::ate);

//    std::cout << "Before move, one = \"" << one.str() << '"'
//              << " two = \"" << two.str() << "\"" << std::endl;

//    //交换 *this 和 rhs 的状态与内容。
//    one.swap(two);

//    std::cout << "After swap, one = \"" << one.str() << '"'
//              << " two = \"" << two.str() << "\"" << std::endl;

//    return 0;
//}

//int main()
//{
//    std::basic_stringbuf<char> one("one", std::ios_base::in
//                                   | std::ios_base::out
//                                   | std::ios_base::ate);

//    std::basic_stringbuf<char> two("two", std::ios_base::in
//                                   | std::ios_base::out
//                                   | std::ios_base::ate);

//    std::cout << "Before move, one = \"" << one.str() << '"'
//              << " two = \"" << two.str() << "\"" << std::endl;

//    //1) 移动赋值运算符：移动 rhs 的内容到 *this 中。
//    //移动后 *this 拥有 rhs 之前保有的关联 string 、打开模式、本地环境和所有其他状态。
//    //保证 *this 中 std::basic_streambuf 的六个指针有别于被移动的 rhs 的对应指针，除非它们为空。
//    one = std::move(two);

//    std::cout << "After move, one = \"" << one.str() << '"'
//              << " two = \"" << two.str() << "\"" << std::endl;

//    return 0;
//}


//int main()
//{
//    // 默认构造函数（ mode = in|out ）
//    std::basic_stringbuf<char> basic_stringbuf1;
//    basic_stringbuf1.sputc('1');
//    std::cout << "basic_stringbuf1: " << &basic_stringbuf1 << std::endl;

//    // 在尾端模式中的 string 构造函数 (C++11)
//    std::basic_stringbuf<char> basic_stringbuf2("test", std::ios_base::in
//            | std::ios_base::out
//            | std::ios_base::ate);
//    basic_stringbuf2.sputc('1');
//    std::cout << "basic_stringbuf2: " << &basic_stringbuf2 << std::endl;

//    // 后附模式测试（结果在编译器间有别）
//    std::basic_stringbuf<char> basic_stringbuf3("test", std::ios_base::in
//            | std::ios_base::out
//            | std::ios_base::app);
//    basic_stringbuf3.sputc('1');
//    basic_stringbuf3.pubseekpos(1);
//    basic_stringbuf3.sputc('2');
//    std::cout << "basic_stringbuf3: " << &basic_stringbuf3 << std::endl;

//    //5) 通过从另一 std::basic_basic_stringbuf 对象 rhs 移动所有状态，
//    //包含关联 string 、打开模式、 locale 和所有其他状态，
//    //移动构造 std::basic_basic_stringbuf 对象。
//    std::basic_stringbuf<char> basic_stringbuf4(std::move(basic_stringbuf2));
//    std::cout << "basic_stringbuf4: " << &basic_stringbuf4 << std::endl;

//    return 0;
//}
