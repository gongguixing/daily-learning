#include <iostream>
#include <algorithm>
#include <functional>
#include <vector>
#include <iterator>
#include <time.h>

using namespace std;

struct Cell
{
    int x;
    int y;

    Cell &operator +=(const Cell &cell)
    {
        x += cell.x;
        y += cell.y;
        return *this;
    }

    bool operator <(const Cell &cell) const
    {
        if (x == cell.x)
        {
            return y < cell.y;
        }
        else
        {
            return x < cell.x;
        }
    }
};

std::ostream &operator<<(std::ostream &os, const Cell &cell)
{
    os << "{" << cell.x << "," << cell.y << "}";
    return os;
}

int main()
{
    srand((unsigned)time(NULL));;

    std::cout.setf(std::ios_base::boolalpha);

    auto func1 = []()
    {
        int n = std::rand() % 10 + 100;
        Cell cell{n, n};
        return cell;
    };

    vector<Cell> cells(8);
    std::generate(cells.begin(), cells.end(), func1);

    vector<Cell> fcells(3);
    std::generate(fcells.begin(), fcells.end(), func1);

    std::sort(cells.begin(), cells.end());
    std::cout << "cells :       ";
    std::copy(cells.begin(), cells.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl << std::endl;

    for (const Cell &cell : fcells)
    {
        bool b = std::binary_search(cells.begin(), cells.end(), cell);
        std::cout << "cell: " << cell << (b ? " found " : " not found ") << std::endl;
    }

    std::cout << std::endl << std::endl;

    auto func2 = [](const Cell & a, const Cell & b)
    {
        if (a.x == b.x)
        {
            return a.y < b.y;
        }
        return a.x < b.x;
    };

    std::generate(cells.begin(), cells.end(), func1);
    std::generate(fcells.begin(), fcells.end(), func1);

    std::sort(cells.begin(), cells.end());
    std::cout << "cells :       ";
    std::copy(cells.begin(), cells.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl << std::endl;

    for (const Cell &cell : fcells)
    {
        bool b = std::binary_search(cells.begin(), cells.end(), cell, func2);
        std::cout << "cell: " << cell << (b ? " found " : " not found ") << std::endl;
    }

    return 0;
}
