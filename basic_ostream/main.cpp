#include <iostream>
#include <fstream>

int main()
{
    std::cout << "Hello, world" // const char* 重载
              << std::endl;          // char 重载
    std::ofstream("test.txt") << 1.2; // 右值重载
}

//#include <iostream>
//#include <sstream>

//struct Foo
//{
//    char n[6];
//};

//std::ostream& operator<<(std::ostream& os, Foo& f)
//{
//    std::ostream::sentry s(os);
//    if (s)
//    {
//        os.write(f.n, 5);
//    }
//    return os;
//}

//int main()
//{
//    Foo f = { "abcde" };
//    std::cout << f << '\n';
//    return 0;
//}

//#include <sstream>
//#include <iostream>
//#include <utility>

//int main()
//{
//    std::ostringstream s1("hello");
//    std::ostringstream s2("bye");

//    s1.swap(s2); // OK ： ostringstream 拥有公开 swap()
//    std::swap(s1, s2); // OK ：调用 s1.swap(s2)

////  std::cout.swap(s2); // 错误： swap 是受保护成员

//    std::cout << s1.str() << '\n';
//}

//#include <thread>
//#include <iostream>
//#include <chrono>

//void f()
//{
//    std::cout << "Output from thread...";
//    std::this_thread::sleep_for(std::chrono::seconds(2));
//    std::cout << "...thread calls flush()" << std::endl;
//    std::cout.flush();
//}

//int main()
//{
//    std::thread t1(f);
//    std::this_thread::sleep_for(std::chrono::seconds(1));
//    std::clog << "Output from main" << std::endl;
//    t1.join();
//}

//#include <sstream>
//#include <iostream>

//int main()
//{
//    std::ostringstream os("hello, world");
//    os.seekp(7);
//    os << 'W';
//    os.seekp(0, std::ios_base::end);
//    os << '!';
//    os.seekp(0);
//    os << 'H';
//    std::cout << os.str() << '\n';
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream s;
//    std::cout << s.tellp() << std::endl;
//    s << 'h';
//    std::cout << s.tellp() << std::endl;
//    s << "ello, world ";
//    std::cout << s.tellp() << std::endl;
//    s << 3.14 << std::endl;
//    std::cout << s.tellp() << std::endl << s.str();

//    return 0;
//}

//#include <iostream>

//int main()
//{
//    int n = 0x41424344;
//    std::cout.write(reinterpret_cast<char*>(&n), sizeof n) << std::endl;

//    char c[] = "This is sample text.";
//    std::cout.write(c, 4) << std::endl;
//}

//#include <fstream>
//#include <iostream>

//int main()
//{
//    std::cout.put('a'); // 正常用法
//    std::cout.put('\n');

//    std::ofstream s("/does/not/exist/");
//    s.clear(); // 假装流是好的
//    std::cout << "Unformatted output: ";
//    s.put('c'); // 这将设置 badbit ，但非 failbit
//    std::cout << " fail=" << bool(s.rdstate() & s.failbit);
//    std::cout << " bad=" << s.bad() << '\n';
//    s.clear();
//    std::cout << "Formatted output:   ";
//    s << 'c'; // 这将设置 badbit 和 failbit
//    std::cout << " fail=" << bool(s.rdstate() & s.failbit);
//    std::cout << " bad=" << s.bad() << '\n';
//}

//#include <iostream>
//#include <iomanip>
//#include <sstream>

//int main()
//{
//    std::istringstream input(" \"Some text.\" ");
//    volatile int n = 42;
//    double f = 3.14;
//    bool b = true;
//    std::cout << n   // int 重载
//              << ' ' // 非成员重载
//              << std::boolalpha << b // bool 重载
//              << " " // non-member overload
//              << std::fixed << f // double 重载
//              << input.rdbuf() // streambuf 重载
//              << &n // bool 重载： volatile int* 不转换成 const void*
//              << std::endl; // 函数重载
//}

//#include <sstream>
//#include <utility>
//#include <iostream>

//int main()
//{
//    std::ostringstream s;
////  std::cout = s;                             // 错误：复制赋值运算符被删除
////  std::cout = std::move(s);                  // 错误：移动赋值运算符为受保护
//    s = std::move(std::ostringstream() << 42); // OK ：通过导出类移动
//    std::cout << s.str() << '\n';
//}

//#include <sstream>
//#include <iostream>

//void add_words(std::streambuf* p)
//{
//    std::ostream buf(p); // buf 与 s 共享缓冲
//    buf << " is the answer";
//} // 调用 buf 的析构函数。 p 保持不受影响。

//int main()
//{
//    std::ostringstream s;
//    s << 42;
//    add_words(s.rdbuf());
//    s << ".";
//    std::cout << s.str() << '\n';
//}

//#include <sstream>
//#include <utility>
//#include <iostream>

//int main()
//{
//    // 错误：复制构造函数被删除
////  std::ostream myout(std::cout);

//    // OK ：与 cout 共享缓冲
//    std::ostream myout(std::cout.rdbuf());

//    // 错误：移动构造函数受保护
////  std::ostream s2(std::move(std::ostringstream() << 7.1));

//    // OK ：通过导出类调用移动构造函数
//    std::ostringstream s2(std::move(std::ostringstream() << 7.1));
//    myout << s2.str() << '\n';
//}

//#include <iostream>

//struct Foo
//{
//    int n;
//    Foo()
//    {
//        std::clog << "static constructor\n";
//    }
//    ~Foo()
//    {
//        std::clog << "static destructor\n";
//    }
//};

//Foo f; // 静态对象

//int main()
//{
//    std::clog << "main function\n";
//}

//#include <thread>
//#include <iostream>
//#include <chrono>

//void f()
//{
//    std::cout << "Output from thread...";
//    std::this_thread::sleep_for(std::chrono::seconds(2));
//    std::cout << "...thread calls flush()" << std::endl;
//}

//int main()
//{
//    std::thread t1(f);
//    std::this_thread::sleep_for(std::chrono::seconds(1));
//    std::clog << "This output from main is not tie()'d to cout\n";
//    std::cerr << "This output is tie()'d to cout\n";
//    t1.join();
//}

//#include <iostream>
//struct Foo
//{
//    int n;
//    Foo()
//    {
//        std::cout << "static constructor\n";
//    }
//    ~Foo()
//    {
//        std::cout << "static destructor\n";
//    }
//};

//Foo f; // 静态对象

//int main()
//{
//    std::cout << "main function\n";
//}
