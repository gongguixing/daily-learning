#include "decodexml.h"
#include "tinyxml2.h"
#include <string>
#include <iostream>

using namespace std;
using namespace tinyxml2;

DecodeXML::DecodeXML()
{
//    char   buffer[MAX_PATH];
//    getcwd(buffer, MAX_PATH);
    //    printf("getcwd:%s", buffer);
}

bool DecodeXML::Decode(const std::string &name)
{
    //声明
    XMLDocument xml;

    if (xml.LoadFile(name.c_str()) != XML_SUCCESS)
    {
        return false;
    }

    //判断头文件是否为空
    XMLElement* rootNode = xml.RootElement();
    if (rootNode == NULL)
    {
        return false;
    }

    //读取第一层信息
    XMLElement* root_1_name = rootNode->FirstChildElement("name");
    //读取第二层信息
    XMLElement* root_2_gender = root_1_name->FirstChildElement("gender");
    //信息输出
    string text_gender = root_2_gender->GetText();
    cout << "gender: " << text_gender << endl;

    //读取第一层信息
    XMLElement* root_1_age = rootNode->FirstChildElement("age");
    const XMLAttribute* att_1_age = root_1_age->FirstAttribute();
    cout << att_1_age->Name() << ":" << att_1_age->Value() << endl;

    string text_age = root_1_age->GetText();
    cout << "age: " << text_age << endl;

    return true;
}
