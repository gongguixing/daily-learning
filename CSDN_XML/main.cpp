#include <iostream>
//#include "decodexml.h"
#include "tinystr.h"
#include "tinyxml.h"
#include <direct.h>
#include <stdio.h>
#include <string>

using namespace std;

#define MAX_PATH 256

void displayxml(const char* fname, const char* dbname)
{
    TiXmlDocument xmlDocument;
    if (!xmlDocument.LoadFile(fname, TIXML_ENCODING_UTF8)) // 需要解析的xml文件
    {
        return;
    }

    TiXmlElement* xmlRoot = xmlDocument.RootElement(); // 找根节点

    // 通过根节点开始遍历下面的所有 名为dbname的子节点，xmlRoot->FirstChildElement();不加参数表示遍历根节点下所有子节点
    // 加参数表示遍历名为dbname的子节点 elem = elem->NextSiblingElement()每次向后找下一个
    for (TiXmlElement* elem = xmlRoot->FirstChildElement(dbname); elem != NULL; elem = elem->NextSiblingElement())
    {
        const char* arr = elem->Attribute("name"); // 取出子节点的属性（rtdb的name属性）
        //const char* node1Name = elem->GetText();  // 取出标签中间的值，我的xml文件用不到
        cout << "rtdb-name属性:" << arr << endl;

        // 通过rtdb继续找他下面的子节点
        // 查找方式和上面的一样 （要是下面还有属性可以继续通过这种方式继续找）
        for (TiXmlElement* elem1 = elem->FirstChildElement(); elem1 != NULL; elem1 = elem1->NextSiblingElement())
        {
            const char* name = elem1->Attribute("name"); // data节点下的属性
            const char* type = elem1->Attribute("type"); // data节点下的属性
            const char* init = elem1->Attribute("init"); // data节点下的属性
            const char* length = elem1->Attribute("length"); // data节点下的属性
            cout << name << " " << type << " " << init << " " << length << endl; // 加上空格
        }
    }
}

int main()
{

    //导入xml文件
    char   buffer[MAX_PATH] = {0};
    getcwd(buffer, MAX_PATH);
    std::string fname = string(buffer) + "\\" + "CSDN.xml";

    displayxml(fname.c_str(), "fname");

//    DecodeXML decodeXML;
//    decodeXML.Decode(fname);
    return 0;
}
