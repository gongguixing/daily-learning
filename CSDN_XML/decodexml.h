#ifndef DECODEXML_H
#define DECODEXML_H

#include "string"

class DecodeXML
{
public:
    DecodeXML();

    bool Decode(const std::string &name);
};

#endif // DECODEXML_H
