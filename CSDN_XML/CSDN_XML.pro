TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
#    tinyxml2.cpp \
#    decodexml.cpp \
#    ctest.cpp \
    tinystr.cpp \
    tinyxml.cpp \
    tinyxml2.cpp \
    tinyxmlerror.cpp \
    tinyxmlparser.cpp

HEADERS += \
#    tinyxml2.h \
#    decodexml.h \
#    ctest.h \
    tinystr.h \
    tinyxml.h \
    tinyxml2.h
