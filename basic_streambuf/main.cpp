#include <iostream>
#include <sstream>

int main()
{
    std::stringstream s("abcdef"); // gptr() 指向 'a'
    char c1 = s.get(); // c = 'a', gptr() 现在指向 'b'
    char c2 = s.rdbuf()->sungetc(); // 同 s.unget() ： gptr() 又指向 'a'
    char c3 = s.get(); // c3 = 'a' ， gptr() 现在指向 'b'
    char c4 = s.get(); // c4 = 'b' ， gptr() 现在指向 'c'
    std::cout << c1 << c2 << c3 << c4 << '\n';

    s.rdbuf()->sungetc();  // 回到 'b'
    s.rdbuf()->sungetc();  // 回到 'a'
    int eof = s.rdbuf()->sungetc();  // 无内容可反获取： pbackfail() 失败
    if (eof == EOF)
    {
        std::cout << "Nothing to unget after 'a'\n";
    }
}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::stringstream s("abcdef"); // gptr() 指向 "abcdef" 中的 'a'
//    std::cout << "Before putback, string holds " << s.str() << '\n';
//    char c1 = s.get(); // c1 = 'a' ， gptr() 现在指向 "abcdef" 中的 'b'
//    char c2 = s.rdbuf()->sputbackc('z'); // 同 s.putback('z')
//    // gptr() 现在指向 "zbcdef" 中的 'z'
//    std::cout << "After putback, string holds " << s.str() << '\n';
//    char c3 = s.get(); // c3 = 'z' ， gptr() 现在指向 "zbcdef" 中的 'b'
//    char c4 = s.get(); // c4 = 'b' ， gptr() 现在指向 "zbcdef" 中的 'c'
//    std::cout << c1 << c2 << c3 << c4 << '\n';

//    s.rdbuf()->sputbackc('b');  // gptr() 现在指向 "zbcdef" 中的 'b'
//    s.rdbuf()->sputbackc('z');  // gptr() 现在指向 "zbcdef" 中的 'z'
//    int eof = s.rdbuf()->sputbackc('x');  // 无内容能反获取： pbackfail() 失败
//    if (eof == EOF)
//    {
//        std::cout << "No room to putback after 'z'\n";
//    }
//}

//#include <iostream>
//#include <array>

//// 以 std::array 实现的 std::ostream 缓冲区
//template<std::size_t SIZE, class CharT = char>
//class ArrayedStreamBuffer : public std::basic_streambuf<CharT>
//{
//public:

//    using Base = std::basic_streambuf<CharT>;
//    using char_type = typename Base::char_type;
//    using int_type = typename Base::int_type;

//    ArrayedStreamBuffer() : buffer_{} // 值初始化 buffer_ 为全零
//    {
//        Base::setp(buffer_.begin(), buffer_.end()); // 设置 std::basic_streambuf
//        // 放置区指针以 'buffer_' 工作
//    }

//    int_type overflow(int_type ch)
//    {
//        std::cout << "overflow\n";
//        return Base::overflow(ch);
//    }

//    void print_buffer()
//    {
//        for (const auto& i : buffer_)
//        {
//            if (i == 0)
//            {
//                std::cout << "NULL";
//            }
//            else
//            {
//                std::cout << i;
//            }
//            std::cout << " ";
//        }
//        std::cout << "\n";
//    }

//private:
//    std::array<char_type, SIZE> buffer_;
//};

//int main()
//{
//    ArrayedStreamBuffer<10> streambuf;
//    std::ostream stream(&streambuf);

//    stream << "hello";
//    streambuf.print_buffer();
//    if (stream.good())
//    {
//        std::cout << "stream is good\n";
//    }

//    stream << "world";
//    streambuf.print_buffer();
//    if (stream.good())
//    {
//        std::cout << "stream is good\n";
//    }

//    stream << "!";
//    streambuf.print_buffer();
//    if (!stream.good())
//    {
//        std::cout << "stream is not good\n";
//    }
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream s1;
//    std::streamsize sz = s1.rdbuf()->sputn("This is a test", 14);
//    s1 << '\n';
//    std::cout << "The call to sputn() returned " << sz << '\n'
//              << "The output sequence contains " << s1.str();

//    std::istringstream s2;
//    sz = s2.rdbuf()->sputn("This is a test", 14);
//    std::cout << "The call to sputn() on an input stream returned " << sz << '\n';
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream s1;
//    std::streamsize sz = s1.rdbuf()->sputn("This is a test", 14);
//    s1 << '\n';
//    std::cout << "The call to sputn() returned " << sz << '\n'
//              << "The output sequence contains " << s1.str();

//    std::istringstream s2;
//    sz = s2.rdbuf()->sputn("This is a test", 14);
//    std::cout << "The call to sputn() on an input stream returned " << sz << '\n';
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::ostringstream s;
//    s.rdbuf()->sputc('a');
//    std::cout << s.str() << '\n';
//}

//#include <iostream>
//#include <sstream>

//class null_filter_buf : public std::streambuf
//{
//    std::streambuf* src;
//    char ch; // 单字节缓冲区
//protected:
//    int underflow()
//    {
//        while ((ch = src->sbumpc()) == '\0') ; // 跳过零
//        setg(&ch, &ch, &ch + 1); // 使得一个读取位置可用
//        return ch; // 可返回 EOF
//    }
//public:
//    null_filter_buf(std::streambuf* buf) : src(buf)
//    {
//        setg(&ch, &ch + 1, &ch + 1); // 缓冲区被初始填充
//    }
//};

//void filtered_read(std::istream& in)
//{
//    std::streambuf* orig = in.rdbuf();
//    null_filter_buf buf(orig);
//    in.rdbuf(&buf);
//    for (char c; in.get(c);)
//    {
//        std::cout << c;
//    }
//    in.rdbuf(orig);
//}

//int main()
//{
//    char a[] = "This i\0s \0an e\0\0\0xample";
//    std::istringstream in(std::string(std::begin(a), std::end(a)));
//    filtered_read(in);
//}

//#include <iostream>
//#include <sstream>

//int main()
//{
//    std::stringstream stream("Hello, world");
//    std::cout << "sgetc() returned '" << (char)stream.rdbuf()->sgetc() << "'\n";
//    std::cout << "peek() returned '" << (char)stream.peek() << "'\n";
//    std::cout << "get() returned '" << (char)stream.get() << "'\n";
//}

//#include <fstream>
//#include <iostream>
//#include <string>

//int main()
//{
//    int cnt = 0;
//    std::ifstream file;
//    char buf[10241];

//    file.rdbuf()->pubsetbuf(buf, sizeof buf);

//    file.open("/usr/share/dict/words");

//    for (std::string line; getline(file, line);)
//    {
//        cnt++;
//    }

//    std::cout << cnt << '\n';
//}

//#include <iostream>
//#include <fstream>

//int main()
//{
//    std::filebuf* fbp = new std::filebuf;
//    fbp->open("test.txt", std::ios_base::out);
//    fbp->sputn("Hello\n", 6);
//    std::streambuf* sbp = fbp;
//    delete sbp; // 关闭文件，冲入并写入输出
//    std::ifstream f("test.txt");
//    std::cout << f.rdbuf(); // 证明
//}
