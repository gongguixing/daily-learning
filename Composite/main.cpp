#include <iostream>
#include "leaf.h"
#include "composite.h"

#include "sleaf.h"
#include "scomposite.h"

int main()
{
    // 透明方式调用
    // 创建树枝构件
    IComponent *c0 = new Composite("c0");
    IComponent *c1 = new Composite("c1");
    // 创建叶子构件
    IComponent *leaf1 = new Leaf("leaf1");
    IComponent *leaf2 = new Leaf("leaf2");
    IComponent *leaf3 = new Leaf("leaf3");
    // c0添加叶子leaf1
    c0->add(leaf1);
    // c0添加树枝c1
    c0->add(c1);
    // c1添加叶子leaf2
    c1->add(leaf2);
    // c1添加叶子leaf3
    c1->add(leaf3);
    // c0方法调用
    c0->operation();
    // 释放所有子节点
    c0->deleteChild();

    std::cout << std::endl << std::endl;

    // 安全方式调用
    // 创建树枝构件
    SComposite *sc0 = new SComposite("sc0");
    SComposite *sc1  = new SComposite("sc1");
    // 创建叶子构件
    ISComponent *sleaf1 = new SLeaf("sleaf1");
    ISComponent *sleaf2 = new SLeaf("sleaf2");
    ISComponent *sleaf3 = new SLeaf("sleaf3");
    // sc0添加叶子sleaf1
    sc0->add(sleaf1);
    // sc0添加树枝sc1
    sc0->add(sc1);
    // sc1添加叶子sleaf2
    sc1->add(sleaf2);
    // sc1添加叶子sleaf3
    sc1->add(sleaf3);
    // sc0方法调用
    sc0->operation();
    // 释放所有子节点
    sc0->deleteChild();

    return 0;
}
