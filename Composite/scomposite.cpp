#include "scomposite.h"
#include <iostream>

SComposite::SComposite(const string &key)
    : mKey(key)
{
    mType = "SComposite";
}

void SComposite::add(ISComponent *p)
{
    mChildren.push_back(p);
    std::cout << "SComposite: " << mKey << " add ";
    std::cout << p->getType() << ": " << p->getKey() << std::endl;
}

void SComposite::remove(ISComponent *c)
{
    uint8_t i = 0;
    for (ISComponent *p : mChildren)
    {
        if (p == c)
        {
            mChildren.erase(mChildren.begin() + i);
            std::cout << "Composite: " << mKey << " remove ";
            std::cout << c->getType() << ": " << c->getKey() << std::endl;
            break;
        }
        i++;
    }
}

ISComponent *SComposite::getChild(uint8_t i)
{
    if (i <= mChildren.size())
    {
        return mChildren.at(i);
    }

    return NULL;
}

void SComposite::deleteChild()
{
    for (ISComponent *p : mChildren)
    {
        std::cout << "SComposite: " << mKey << " delete ";
        std::cout << p->getType() << ": " << p->getKey() << std::endl;

        if (p->getType().compare("SComposite") == 0)
        {
            SComposite * s = (SComposite*)p;
            if (s)
            {
                s->deleteChild();
            }
            delete p;
        }
        else if (p->getType().compare("SLeef") == 0)
        {
            delete p;
        }
    }

    mChildren.clear();
}

void SComposite::operation()
{
    for (ISComponent *p : mChildren)
    {
        if (p)
        {
            p->operation();
        }
    }
}

string &SComposite::getKey()
{
    return mKey;
}

string &SComposite::getType()
{
    return mType;
}
