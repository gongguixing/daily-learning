#ifndef COMPOSITE_H
#define COMPOSITE_H

#include "icomponent.h"
#include <vector>

// 透明方式
// 树枝构件角色 / 中间构件
// 树枝构件（Composite）角色 / 中间构件：是组合中的分支节点对象，它有子节点，用于继承和实现抽象构件。
// 它的主要作用是存储和管理子部件，通常包含 Add()、Remove()、GetChild() 等方法。
class Composite : public IComponent
{
public:
    Composite(const string &key);

    void add(IComponent *c)     override;

    void remove(IComponent *c)  override;

    IComponent* getChild(uint8_t i)override;

    void deleteChild()          override;

    void operation()            override;

    string &getKey()            override;

    string &getType()           override;

private:
    // 子对象容器
    vector<IComponent*> mChildren;
    // 唯一标识属性
    string mKey;
    // 节点类型
    string mType;
};

#endif // COMPOSITE_H
