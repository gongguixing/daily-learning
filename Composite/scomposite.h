#ifndef SCOMPOSITE_H
#define SCOMPOSITE_H

#include "iscomponent.h"
#include <vector>

// 安全方式
// 树枝构件角色 / 中间构件
// 树枝构件（Composite）角色 / 中间构件：是组合中的分支节点对象，它有子节点，用于继承和实现抽象构件。
// 它的主要作用是存储和管理子部件，通常包含 Add()、Remove()、GetChild() 等方法。
class SComposite : public ISComponent
{
public:
    SComposite(const string &key);

    void add(ISComponent * p);

    void remove(ISComponent *c) ;

    ISComponent* getChild(uint8_t i);

    void deleteChild();

    void operation()            override;

    string &getKey()            override;

    string &getType()           override;

private:
    vector<ISComponent*> mChildren;
    string mKey;
    string mType;
};

#endif // SCOMPOSITE_H
