#include "leaf.h"
#include <iostream>

Leaf::Leaf(const string &key)
    : mKey(key)
{
    mType = "Leaf";
}

void Leaf::add(IComponent *c)
{
}

void Leaf::remove(IComponent *c)
{

}

IComponent *Leaf::getChild(uint8_t i)
{
    return NULL;
}

void Leaf::deleteChild()
{

}

void Leaf::operation()
{
    std::cout << "Leaf: " << mKey << " operation." << std::endl;
}

string &Leaf::getKey()
{
    return mKey;
}

string &Leaf::getType()
{
    return mType;
}
