#ifndef ISCOMPONENT_H
#define ISCOMPONENT_H

#include <string>
using namespace std;

// 透明方式
// 抽象构件角色
// 抽象构件（Component）角色：
// 它的主要作用是为树叶构件和树枝构件声明公共接口，并实现它们的默认行为。
// 在安全式的组合模式中不声明访问和管理子类的接口，管理工作由树枝构件完成
class ISComponent
{
public:
    virtual ~ISComponent() {}

    // 方法调用
    virtual void operation()            = 0;

    // 获取唯一标识属性
    virtual string &getKey()            = 0;

    // 获取节点类型
    virtual string &getType()           = 0;
};

#endif // ISCOMPONENT_H
