#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <float.h>

int main()
{
    //std::ptrdiff_t 类型对象的最小值
    printf("PTRDIFF_MIN:    %d\n",      PTRDIFF_MIN);
    //std::ptrdiff_t 类型对象的最大值
    printf("PTRDIFF_MAX:    %d\n",      PTRDIFF_MAX);
    //std::size_t 类型对象的最大值
    printf("SIZE_MAX:       %d\n",      SIZE_MAX);
    //std::sig_atomic_t 类型对象的最小值
    printf("SIG_ATOMIC_MIN: %d\n",      SIG_ATOMIC_MIN);
    //std::sig_atomic_t 类型对象的最大值
    printf("SIG_ATOMIC_MAX: %d\n",      SIG_ATOMIC_MAX);
    //wchar_t 类型对象的最小值
    printf("WCHAR_MIN:      %d\n",      WCHAR_MIN);
    //wchar_t 类型对象的最大值
    printf("WCHAR_MAX:      %d\n",      WCHAR_MAX);
    //std::wint_t 类型对象的最小值
    printf("WINT_MIN:       %d\n",      WINT_MIN);
    //std::wint_t 类型对象的最大值
    printf("WINT_MAX:       %d\n",      WINT_MAX);
    printf("\n");

    //整数类型极限
    //字节的位数
    printf("CHAR_BIT:       %d\n",      CHAR_BIT);
    //多字节字符的最大字节数
    printf("MB_LEN_MAX:     %d\n",      MB_LEN_MAX);
    //char 的最小值
    printf("CHAR_MIN:       %d\n",      CHAR_MIN);
    //char 的最大值
    printf("CHAR_MAX:       %d\n",      CHAR_MAX);
    printf("\n");

    //分别为 signed char、short、int、long 及 long long 的最小值
    printf("SCHAR_MIN:      %d\n",      SCHAR_MIN);
    printf("SHRT_MIN:       %d\n",      SHRT_MIN);
    printf("INT_MIN:        %d\n",      INT_MIN);
    printf("LONG_MIN:       %I32d\n",   LONG_MIN);
    printf("LLONG_MIN:      %I64d\n",   LLONG_MIN);
    printf("\n");

    //分别为 signed char、short、int、long 及 long long 的最大值
    printf("SCHAR_MAX:      %d\n",      SCHAR_MAX);
    printf("SHRT_MAX:       %d\n",      SHRT_MAX);
    printf("INT_MAX:        %d\n",      INT_MAX);
    printf("LONG_MAX:       %I32d\n",   LONG_MAX);
    printf("LLONG_MAX:      %I64d\n",   LLONG_MAX);
    printf("\n");

    //分别为 unsigned char、unsigned short、unsigned int、
    //unsigned long 及 unsigned long long 的最大值
    printf("UCHAR_MAX:      %d\n",      UCHAR_MAX);
    printf("USHRT_MAX:      %d\n",      USHRT_MAX);
    printf("UINT_MAX:       %d\n",      UINT_MAX);
    printf("ULONG_MAX:      %I32u\n",   ULONG_MAX);
    printf("ULLONG_MAX:     %I64u\n",   ULLONG_MAX);
    printf("\n");

    //浮点类型极限
    //所有三种浮点类型的表示所用的基数（整数底）
    printf("FLT_RADIX:      %d\n",      FLT_RADIX);
    //从 long double 转换到至少有 DECIMAL_DIG 位数字的十进制表示，
    //再转换回 long double 为恒等转换：这是序列化/反序列化 long double 所要求的十进制精度
    //（参阅 std::numeric_limits::max_digits10）
    printf("DECIMAL_DIG:    %d\n",      DECIMAL_DIG);
    printf("FLT_DECIMAL_DIG:%d\n",      FLT_DECIMAL_DIG);
    printf("DBL_DECIMAL_DIG:%d\n",      DBL_DECIMAL_DIG);
    printf("LDBL_DECIMAL_DIG:%d\n",     LDBL_DECIMAL_DIG);
    printf("\n");

    //分别为 float、double 与 long double 的最小规格化正数值
    printf("FLT_MIN:      %f\n",        FLT_MIN);
    printf("DBL_MIN:      %f\n",        DBL_MIN);
    printf("LDBL_MIN:     %lf\n",       LDBL_MIN);
    printf("\n");

    //分别为 float、double 与 long double 的最小正数值
    printf("FLT_TRUE_MIN:      %f\n",   FLT_TRUE_MIN);
    printf("DBL_TRUE_MIN:      %f\n",   DBL_TRUE_MIN);
    printf("LDBL_TRUE_MIN:     %lf\n",  LDBL_TRUE_MIN);
    printf("\n");

    //分别为 float、double 与 long double 的最大值
    printf("FLT_MAX:      %f\n",        FLT_MAX);
    printf("DBL_MAX:      %f\n",        DBL_MAX);
    printf("LDBL_MAX:     %lf\n",       LDBL_MAX);
    printf("\n");

    //分别为 1.0 和 float、double 及 long double 的下一个可表示值之差
    printf("FLT_EPSILON:  %f\n",        FLT_EPSILON);
    printf("DBL_EPSILON:  %f\n",        DBL_EPSILON);
    printf("LDBL_EPSILON: %lf\n",       LDBL_EPSILON);
    printf("\n");

    //保证能在文本→ float/double/long double →文本的往返转换中保留而不会
    //因舍入或溢出发生改变的的十进制位数（解释参阅 std::numeric_limits::digits10）
    printf("FLT_DIG:      %d\n",        FLT_DIG);
    printf("DBL_DIG:      %d\n",        DBL_DIG);
    printf("LDBL_DIG:     %d\n",        LDBL_DIG);
    printf("\n");

    //分别为能无精度损失地表示成 float、double 及 long double 的基数 FLT_RADIX 的数字位数
    printf("FLT_MANT_DIG: %d\n",        FLT_MANT_DIG);
    printf("DBL_MANT_DIG: %d\n",        DBL_MANT_DIG);
    printf("LDBL_MANT_DIG:%d\n",        LDBL_MANT_DIG);
    printf("\n");

    //分别为能够使FLT_RADIX 的该整数减一次幂为规格化的 float、double 与 long double 的最小负整数
    printf("FLT_MIN_EXP:  %d\n",        FLT_MIN_EXP);
    printf("DBL_MIN_EXP:  %d\n",        DBL_MIN_EXP);
    printf("LDBL_MIN_EXP: %d\n",        LDBL_MIN_EXP);
    printf("\n");

    //分别为能够使 10 的该整数减一次幂为规格化的 float、double 与 long double 的最小负整数
    printf("FLT_MIN_10_EXP:%d\n",       FLT_MIN_10_EXP);
    printf("DBL_MIN_10_EXP:%d\n",       DBL_MIN_10_EXP);
    printf("LDBL_MIN_10_EXP:%d\n",      LDBL_MIN_10_EXP);
    printf("\n");

    //分别为能够使 FLT_RADIX 的该整数减一次幂为可表示的有限的 float、double 与 long double 的最大正整数
    printf("FLT_MAX_EXP:    %d\n",      FLT_MAX_EXP);
    printf("DBL_MAX_EXP:    %d\n",      DBL_MAX_EXP);
    printf("LDBL_MAX_EXP:   %d\n",      LDBL_MAX_EXP);
    printf("\n");

    //分别为能够使 10 的该整数减一次幂为可表示的有限的 float、double 与 long double 的最大正整数
    printf("FLT_MAX_10_EXP: %d\n",      FLT_MAX_10_EXP);
    printf("DBL_MAX_10_EXP: %d\n",      DBL_MAX_10_EXP);
    printf("LDBL_MAX_10_EXP:%d\n",      LDBL_MAX_10_EXP);
    printf("\n");

    //浮点算术的默认舍入模式
    printf("FLT_ROUNDS:     %d\n",      FLT_ROUNDS);
    //指定进行所有算术运算所用的精度
    printf("FLT_EVAL_METHOD:%d\n",      FLT_EVAL_METHOD);
    //指明类型是否支持非正规数值：-1 为不确定，0 为不支持，1 为支持。
    printf("FLT_HAS_SUBNORM:%d\n",      FLT_HAS_SUBNORM);
    printf("DBL_HAS_SUBNORM:%d\n",      DBL_HAS_SUBNORM);
    printf("LDBL_HAS_SUBNORM:%d\n",     LDBL_HAS_SUBNORM);
    printf("\n");
    return 0;
}
