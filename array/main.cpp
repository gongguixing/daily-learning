#include <iostream>
#include <string>
#include <iterator>
#include <algorithm>
#include <functional>
#include <time.h>
#include <array>

using namespace std;

struct Cell
{
    int x;
    int y;

    Cell() = default;
    Cell(int a, int b): x(a), y(b) {}

    Cell &operator +=(const Cell &cell)
    {
        x += cell.x;
        y += cell.y;
        return *this;
    }

    Cell &operator +(const Cell &cell)
    {
        x += cell.x;
        y += cell.y;
        return *this;
    }

    Cell &operator *(const Cell &cell)
    {
        x *= cell.x;
        y *= cell.y;
        return *this;
    }

    Cell &operator ++()
    {
        x += 1;
        y += 1;
        return *this;
    }


    bool operator <(const Cell &cell) const
    {
        if (x == cell.x)
        {
            return y < cell.y;
        }
        else
        {
            return x < cell.x;
        }
    }

    bool operator >(const Cell &cell) const
    {
        if (x == cell.x)
        {
            return y > cell.y;
        }
        else
        {
            return x > cell.x;
        }
    }

    bool operator ==(const Cell &cell) const
    {
        return x == cell.x && y == cell.y;
    }
};

std::ostream &operator<<(std::ostream &os, const Cell &cell)
{
    os << "{" << cell.x << "," << cell.y << "}";
    return os;
}

template<class T>
void my_tuple_size(const string &name, T t)
{
    //提供作为编译时常量表达式访问 std::array 中元素数量的方法。
    int a[std::tuple_size<T>::value]; // 能用于编译时
    std::cout << name << " ";
    std::cout << "std::tuple_size<T>::value:    " << std::tuple_size<T>::value << std::endl;
}

int main()
{
    std::cout << std::boolalpha;

    std::mt19937 g{std::random_device{}()};
    srand((unsigned)time(NULL));

    auto generate = []()
    {
        int n = std::rand() % 10 + 110;
        Cell cell{n, n};
        return cell;
    };

    //遵循聚合初始化的规则初始化 array （注意默认初始化可以导致非类的 T 的不确定值）
    std::array<Cell, 6> array1;
    std::generate(array1.begin(), array1.end(), generate);
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::array<Cell, 6> array2;
    std::generate(array2.begin(), array2.end(), generate);
    std::cout << "array2:   ";
    std::copy(array2.begin(), array2.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::array<Cell, 6> array3 = array1;
    std::cout << "array3:   ";
    std::copy(array3.begin(), array3.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    //比较二个 array 的内容。
    //1-2) 检查 lhs 与 rhs 的内容是否相等，即 lhs 中的每个元素是否与 rhs 中同一位置的元素比较相等。
    //1) 若容器内容相等则为 true ，否则为 false 。
    std::cout << "array1 == array2 :    " << (array1 == array2) << std::endl;
    std::cout << "array1 == array3 :    " << (array1 == array3) << std::endl;
    //2) 若容器的内容不相等则为 true ，否则为 false 。
    std::cout << "array1 != array2 :    " << (array1 != array2) << std::endl;
    std::cout << "array1 != array3 :    " << (array1 != array3) << std::endl;
    //3) 若 lhs 的内容按字典序小于 rhs 的内容则为 true ，否则为 false 。
    std::cout << "array1 <  array2 :    " << (array1 <  array2) << std::endl;
    std::cout << "array1 <  array3 :    " << (array1 <  array3) << std::endl;
    //4) 若 lhs 的内容按字典序小于或等于 rhs 的内容则为 true ，否则为 false 。
    std::cout << "array1 <= array2 :    " << (array1 <= array2) << std::endl;
    std::cout << "array1 <= array3 :    " << (array1 <= array3) << std::endl;
    //5) 若 lhs 的内容按字典序大于 rhs 的内容则为 true ，否则为 false 。
    std::cout << "array1 >  array2 :    " << (array1 >  array2) << std::endl;
    std::cout << "array1 >  array3 :    " << (array1 >  array3) << std::endl;
    //6) 若 lhs 的内容按字典序大于或等于 rhs 的内容则为 true ，否则为 false 。
    std::cout << "array1 >= array2 :    " << (array1 >= array2) << std::endl;
    std::cout << "array1 >= array3 :    " << (array1 >= array3) << std::endl;
    std::cout << std::endl;


    //从 array 提取第 Ith 个元素。
    //I 必须是范围 [0, N) 中的整数值。与 at() 或 operator[] 相反，这在编译时强制。
    std::cout << "std::get<0>(array1) :     " << std::get<0>(array1) << std::endl;
    std::cout << "std::get<1>(array1) :     " << std::get<1>(array1) << std::endl;
    std::cout << "std::get<2>(array1) :     " << std::get<2>(array1) << std::endl;
    std::cout << "std::get<3>(array1) :     " << std::get<3>(array1) << std::endl;
    std::cout << "std::get<4>(array1) :     " << std::get<4>(array1) << std::endl;
    std::cout << "std::get<5>(array1) :     " << std::get<5>(array1) << std::endl;
    std::cout << std::endl;

    std::cout << "swap before:" << std::endl;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << "array3:   ";
    std::copy(array3.begin(), array3.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    //为 std::array 特化 std::swap 算法。交换 lhs 与 rhs 的内容。调用 lhs.swap(rhs) 。
    std::swap(array1, array3);
    std::cout << "swap after:" << std::endl;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << "array3:   ";
    std::copy(array3.begin(), array3.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    my_tuple_size("array1", array1);
    my_tuple_size("array1", array3);
    return 0;
}

void empty_size_max_size_swap()
{
    auto generate = []()
    {
        int n = std::rand() % 10 + 110;
        Cell cell{n, n};
        return cell;
    };

    //遵循聚合初始化的规则初始化 array （注意默认初始化可以导致非类的 T 的不确定值）
    std::array<Cell, 6> array1;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;

    std::generate(array1.begin(), array1.end(), generate);
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    //检查容器是否无元素，即是否 begin() == end() 。
    std::cout << "array1 empty: " << array1.empty() << std::endl;
    std::array<Cell, 0> array2;
    std::cout << "array2 empty: " << array2.empty() << std::endl;
    std::cout << std::endl;

    //返回容器中的元素数，即 std::distance(begin(), end()) 。
    std::cout << "array1 contains " << array1.size() << " elements." << std::endl;
    std::cout << "array2 contains " << array2.size() << " elements." << std::endl;
    std::cout << std::endl;


    //返回根据系统或库实现限制的容器可保有的元素最大数量，即对于最大容器的 std::distance(begin(), end()) 。
    std::cout << std::endl;
    std::cout << "array1 max_size " << array1.max_size() << std::endl;
    std::cout << "array2 max_size " << array2.max_size() << std::endl;
    std::cout << std::endl;


    //赋给定值 value 给容器中的所有元素。
    Cell cell = generate();
    array1.fill(cell);
    std::cout << "array1.fill( " << cell << " )" << std::endl;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    std::array<Cell, 6> array3;
    std::generate(array3.begin(), array3.end(), generate);

    std::cout << "swap before:" << std::endl;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << "array3:   ";
    std::copy(array3.begin(), array3.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    //将容器内容与 other 的内容交换。不导致迭代器和引用关联到别的容器。
    array1.swap(array3);
    std::cout << "swap after:" << std::endl;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << "array3:   ";
    std::copy(array3.begin(), array3.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;
}

void iterator()
{
    auto generate = []()
    {
        int n = std::rand() % 10 + 110;
        Cell cell{n, n};
        return cell;
    };

    //遵循聚合初始化的规则初始化 array （注意默认初始化可以导致非类的 T 的不确定值）
    std::array<Cell, 6> array1;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;

    std::generate(array1.begin(), array1.end(), generate);
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    //返回指向容器首元素的迭代器。若容器为空，则返回的迭代器将等于 end() 。
    //返回指向容器末元素后一元素的迭代器。此元素表现为占位符；试图访问它导致未定义行为。
    for (std::array<Cell, 6>::iterator it = array1.begin(); it != array1.end(); it++)
    {
        *it = generate();
    }

    std::cout << "array1 const_iterator:   " << std::endl;
    for (std::array<Cell, 6>::const_iterator cit = array1.cbegin(); cit != array1.cend(); cit++)
    {
        std::cout << cit << " " << *cit << std::endl;
    }
    std::cout << std::endl;

    //返回指向逆向容器首元素的逆向迭代器。它对应非逆向容器的末元素。
    //返回指向逆向容器末元素后一元素的逆向迭代器。
    //它对应非逆向容器首元素的前一元素。此元素表现为占位符，试图访问它导致未定义行为。
    for (std::array<Cell, 6>::reverse_iterator  rit = array1.rbegin(); rit != array1.rend(); rit++)
    {
        *rit = generate();
    }

    std::cout << "array1 const_reverse_iterator:   " << std::endl;
    for (std::array<Cell, 6>::const_reverse_iterator crit = array1.crbegin(); crit != array1.crend(); crit++)
    {
        std::cout << "crit: " << " " << *crit << std::endl;
    }
    std::cout << std::endl;
}

void at_back_data()
{
    std::mt19937 g{std::random_device{}()};
    srand((unsigned)time(NULL));

    auto generate = []()
    {
        int n = std::rand() % 10 + 110;
        Cell cell{n, n};
        return cell;
    };

    //遵循聚合初始化的规则初始化 array （注意默认初始化可以导致非类的 T 的不确定值）
    std::array<Cell, 6> array1;
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;

    std::generate(array1.begin(), array1.end(), generate);
    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    for (size_t index = 0; index < array1.size(); index++)
    {
        //返回位于指定位置 pos 的元素的引用，有边界检查。
        //若 pos 不在容器范围内，则抛出 std::out_of_range 类型的异常。
        std::cout << "array1.at(" << index << "):   " << array1.at(index) << " ";
        std::cout << std::endl;
        array1.at(index) = generate();
    }
    std::cout << std::endl;

    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    for (size_t index = 0; index < array1.size(); index++)
    {
        //返回位于指定位置 pos 的元素的引用。不进行边界检查。
        std::cout << "array1[" << index << "]:      " << array1.at(index) << " ";
        std::cout << std::endl;
        array1[index] = generate();
    }
    std::cout << std::endl;

    std::cout << "array1:   ";
    std::copy(array1.begin(), array1.end(), std::ostream_iterator<Cell>(std::cout, " "));
    std::cout << std::endl;
    std::cout << std::endl;

    //返回到容器首元素的引用。 在空容器上对 front 的调用是未定义的。
    std::cout << "array1.front():   " << array1.front() << std::endl;
    array1.front() = generate();
    std::cout << "array1.front():   " << array1.front() << std::endl;
    std::cout << std::endl;

    std::cout << "array1.back():    " << array1.back() << std::endl;
    array1.back() = generate();
    std::cout << "array1.back():    " << array1.back() << std::endl;
    std::cout << std::endl;

    //返回指向作为元素存储工作的底层数组的指针。
    //指针满足范围 [data(); data() + size()) 始终是合法范围，
    //即使容器为空（该情况下 data() 不可解引用）。
    for (size_t index = 0; index < array1.size(); index++)
    {
        std::cout << "array1.data() + " << index << ":  " << array1.data() + index << " --- ";
        std::cout << *(array1.data() + index) << std::endl;
        array1[index] = generate();
    }
    std::cout << std::endl;

}
