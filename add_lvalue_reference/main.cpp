#include <iostream>
#include <type_traits>

int main()
{
    using nonref = int;
    using lref = typename std::add_lvalue_reference<nonref>::type;
    using rref = typename std::add_rvalue_reference<nonref>::type;

    std::cout << std::boolalpha;
    std::cout << "std::is_lvalue_reference<int>::value:                                     "
              << std::is_lvalue_reference<int>::value << std::endl;
    std::cout << "std::is_lvalue_reference<std::add_lvalue_reference<nonref>::type>::value: "
              << std::is_lvalue_reference<lref>::value << std::endl;
    std::cout << "std::is_rvalue_reference<std::add_rvalue_reference<nonref>::type>::value: "
              << std::is_rvalue_reference<rref>::value << std::endl;

    return 0;
}
