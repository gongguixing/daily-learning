#include <iostream>
#include <type_traits>
#include <string>

template<class T, std::size_t N>
class static_vector
{
    // N 个 T 的正确对齐的未初始化存储
    typename std::aligned_union<sizeof(T), T>::type data[N];
    std::size_t m_size = 0;

public:
    // 于对齐存储创建对象
    template<typename ...Args> void emplace_back(Args&&... args)
    {
        if (m_size >= N)  // 可行的错误处理
            throw std::bad_alloc{};
        new (data + m_size) T(std::forward<Args>(args)...);
        ++m_size;
    }

    // 访问对齐存储中的对象
    const T& operator[](std::size_t pos) const
    {
        // 注意： C++17 起需要 std::launder
        return *reinterpret_cast<const T*>(data + pos);
    }

    // 从对齐存储删除对象
    ~static_vector()
    {
        for (std::size_t pos = 0; pos < m_size; ++pos)
        {
            // 注意： C++17 起需要 std::launder
            reinterpret_cast<T*>(data + pos)->~T();
        }
    }
};

int main()
{
    static_vector<std::string, 10> v1;
    v1.emplace_back(5, '*');
    v1.emplace_back(10, '*');
    std::cout << v1[0] << std::endl << v1[1] << std::endl;
    return 0;
}
