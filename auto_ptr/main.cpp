#include <iostream>
#include <memory>

int main()
{
    //初始化方式一
    std::auto_ptr<int>a_ptr(new int(8));
    std::cout << *a_ptr.get() << std::cout;
    return 0;
}
