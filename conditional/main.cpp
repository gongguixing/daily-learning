#include <iostream>
#include <type_traits>
#include <typeinfo>

struct A
{
};

struct B
{
};

int main()
{
    typedef std::conditional<true, int, double>::type Type1;
    typedef std::conditional<false, int, double>::type Type2;
    typedef std::conditional<sizeof(int) >= sizeof(double), int, double >::type Type3;
    typedef std::conditional<false, A, B>::type Type4;
    typedef std::conditional<true, std::string, char*>::type Type5;

    std::cout << "std::conditional<true, int, double>::type:    "
              << typeid(Type1).name() << std::endl;
    std::cout << "std::conditional<false, int, double>::type:   "
              << typeid(Type2).name() << std::endl;
    std::cout << "std::conditional<sizeof(int)>=sizeof(double),int,double>::type:"
              << typeid(Type3).name() << std::endl;
    std::cout << "std::conditional<false, A, B>::type:          "
              << typeid(Type4).name() << std::endl;
    std::cout << "std::conditional<true, std::string, char*>::type: "
              << typeid(Type5).name() << std::endl;
    return 0;
}
