#include <iostream>
#include <type_traits>

struct foo
{
    void m()
    {
        std::cout << "Non-cv" << std::endl;
    }
    void m() const
    {
        std::cout << "Const" << std::endl;
    }
    void m() volatile
    {
        std::cout << "Volatile" << std::endl;
    }
    void m() const volatile
    {
        std::cout << "Const-volatile" << std::endl;
    }
};

int main()
{
    foo{}.m();
    std::cout << "std::add_const<foo>::type:        ";
    std::add_const<foo>::type{}.m();
    std::cout << "std::add_volatile<foo>::type:     ";
    std::add_volatile<foo>::type{}.m();
    std::cout << "std::add_cv<foo>::type:           ";
    std::add_cv<foo>::type{}.m();

    return 0;
}
