#include <QCoreApplication>
#include <iostream>
#include <string>
#include <stdint.h>
using namespace std;

class A
{
public:

    A(){
        cout << "A instance" << endl;
    }
};

class B
{
public:
    static void instance()
    {
        static A a;
    }
};

void  test()
{
    static A a;
}

int main(int argc, char *argv[])
{
    cout << "main" << endl;
    B::instance();
    return 0;
}
