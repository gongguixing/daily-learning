#include <fstream>
#include <iostream>

struct mybuf : std::filebuf
{
    pos_type seekpos(pos_type sp, std::ios_base::openmode which)
    {
        std::cout << "Before seekpos(" << sp << "), size of the get area is "
                  << egptr() - eback() << " with "
                  << egptr() - gptr() << " read positions available\n";
        pos_type rc = std::filebuf::seekpos(sp, which);
        std::cout << "seekpos() returns " << rc << ".\nAfter the call, "
                  << "size of the get area is "
                  << egptr() - eback() << " with "
                  << egptr() - gptr() << " read positions available\n";
// 若 seekpos() 清空获取区则反注释
//         std::filebuf::underflow();
//         std::cout << "after forced underflow(), size of the get area is "
//                   << egptr()-eback() << " with "
//                   << egptr()-gptr() << " read positions available\n";
        return rc;
    }
};

int main()
{
    mybuf buf;
    buf.open("test.txt", std::ios_base::in);
    std::istream stream(&buf);
    stream.get(); // 读一个字符以强制 underflow()
    stream.seekg(2);
}

//#include <iostream>
//#include <fstream>
//#include <locale>
//int main()
//{
//    // 准备 10 字节文件，保有 4 个 UTF8 中的字符
//    std::ofstream("text.txt") << u8"z\u00df\u6c34\U0001d10b";
//    // 用非转换编码打开
//    std::ifstream f1("text.txt");
//    std::cout << "f1's locale's encoding() returns "
//              << std::use_facet<std::codecvt<char, char, std::mbstate_t>>(f1.getloc()).encoding()
//              << std::endl
//              << "pubseekoff(3, beg) returns "
//              << f1.rdbuf()->pubseekoff(3, std::ios_base::beg) << std::endl
//              << "pubseekoff(0, end) returns "
//              << f1.rdbuf()->pubseekoff(0, std::ios_base::end) << std::endl;

//    // 用 UTF-8 打开
//    std::wifstream f2("text.txt");
//    f2.imbue(std::locale("C"));
//    std::cout << "f2's locale's encoding() returns "
//              << std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(f2.getloc()).encoding()
//              << std::endl
//              << "pubseekoff(3, beg) returns "
//              << f2.rdbuf()->pubseekoff(3, std::ios_base::beg) << std::endl
//              << "pubseekoff(0, end) returns "
//              << f2.rdbuf()->pubseekoff(0, std::ios_base::end) << std::endl;
//}

//#include <fstream>
//#include <iostream>
//#include <string>

//int main()
//{
//    int cnt = 0;
//    std::ifstream file;
//    char buf[10241];

//    file.rdbuf()->pubsetbuf(buf, sizeof buf);
//    file.open("test.txt");

//    if (!file.is_open())
//    {
//        std::cout << "open error" << std::endl;
//    }

//    for (std::string line; getline(file, line);)
//    {
//        ++cnt;
//    }
//    std::cout << cnt << std::endl;
//    return 0;
//}

//#include <fstream>
//#include <iostream>

//struct mybuf : std::filebuf
//{
//    int underflow()
//    {
//        std::cout << "Before underflow(): size of the get area is "
//                  << egptr() - eback() << " with "
//                  << egptr() - gptr() << " read positions available\n";
//        int rc = std::filebuf::underflow();
//        std::cout << "underflow() returns " << rc << ".\nAfter the call, "
//                  << "size of the get area is "
//                  << egptr() - eback() << " with "
//                  << egptr() - gptr() << " read positions available\n";
//        return rc;
//    }
//};

//int main()
//{
//    mybuf buf;
//    buf.open("test.txt", std::ios_base::in);
//    std::istream stream(&buf);
//    while (stream.get()) ;
//    return 0;
//}

//#include <fstream>
//#include <iostream>

//struct mybuf : std::filebuf
//{
//    using std::filebuf::showmanyc;
//};

//int main()
//{
//    mybuf fin;
//    fin.open("test.txt", std::ios_base::in);
//    if (!fin.is_open())
//    {
//        std::cout << "open error" << std::endl;
//    }
//    std::cout << "showmanyc() returns " << fin.showmanyc() << std::endl;
//    return 0;
//}

//#include <fstream>
//#include <iostream>

//int main()
//{
//    std::ifstream fs("test.txt");
//    std::filebuf fb;
//    fb.open("test.txt", std::ios_base::in);
//    std::cout << std::boolalpha
//              << "direct call: " << fb.is_open() << '\n'
//              << "through streambuf: " << fs.rdbuf()->is_open() << '\n'
//              << "through fstream: " << fs.is_open() << '\n';
//}

//#include <fstream>
//#include <string>
//#include <iostream>

//int main()
//{
//    std::ifstream fin("test.in"); // 只读
//    std::ofstream fout("test.out"); // 只写

//    std::string s;
//    getline(fin, s);
//    std::cout << s << '\n'; // 输出 test.in 的第一行

//    fin.rdbuf()->swap(*fout.rdbuf()); // 交换底层缓冲

//    getline(fin, s); // 失败：不能从只写 filebuf 读取
//    std::cout << s << '\n'; // 打印空行
//}

//#include <fstream>
//#include <string>
//#include <iostream>

//int main()
//{

//    std::ifstream fin("test.in"); // 只读
//    std::ofstream fout("test.out"); // 只写

//    std::string s;
//    getline(fin, s);
//    std::cout << s << '\n'; // 输出

//    *fin.rdbuf() = std::move(*fout.rdbuf());

//    getline(fin, s);
//    std::cout << s << '\n'; // 空行

//    std::cout << std::boolalpha << fout.is_open() << '\n'; // 打印 "false"
//    return 0;

//}
