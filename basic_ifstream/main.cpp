#include <fstream>
#include <utility>
#include <string>
#include <iostream>

int main()
{
    std::ifstream ifstream1("test1.txt", std::ios::in);
    std::cout << "ifstream1: " << std::endl;
    if (ifstream1.is_open())
    {
        std::string strLine;
        uint8_t index = 0;
        while (std::getline(ifstream1, strLine))
        {
            std::cout << strLine << std::endl;
            if (index > 1)
            {
                break;
            }
            index++;
        }
    }
    std::cout << std::endl;

    std::ifstream ifstream2("test2.txt", std::ios::in);
    std::cout << "ifstream2: " << std::endl;
    if (ifstream2.is_open())
    {
        std::string strLine;
        uint8_t index = 0;
        while (std::getline(ifstream2, strLine))
        {
            std::cout << strLine << std::endl;
            if (index > 1)
            {
                break;
            }
            index++;
        }
    }
    std::cout << std::endl;

    //为 std::basic_ifstream 特化 std::swap 算法。
    std::swap(ifstream1, ifstream2);

    std::cout << "ifstream1: " << std::endl;
    if (ifstream1.is_open())
    {
        std::string strLine;
        while (std::getline(ifstream1, strLine))
        {
            std::cout << strLine << std::endl;
        }
    }

    std::cout << std::endl;

    std::cout << "ifstream2: " << std::endl;
    if (ifstream2.is_open())
    {
        std::string strLine;
        while (std::getline(ifstream2, strLine))
        {
            std::cout << strLine << std::endl;
        }
    }

    return 0;
}

//int main()
//{
//    std::ifstream ifstream1("test1.txt", std::ios::in);
//    std::ifstream ifstream2("test2.txt", std::ios::in);
//    std::ifstream ifstream3("test3.txt", std::ios::in);
//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream2 is: " << (ifstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream3 is: " << (ifstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    std::cout << "std::ifstream close" << std::endl;
//    ifstream1.close();
//    ifstream2.close();
//    ifstream3.close();

//    std::cout << std::endl;

//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream2 is: " << (ifstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream3 is: " << (ifstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::string strFileName1 = "test1.txt";
//    std::ifstream ifstream1;
//    //1-2) 等效地调用 rdbuf()->open(filename, mode | ios_base::in).
//    ifstream1.open(strFileName1.c_str(), std::ios::in);

//    std::ifstream ifstream2;
//    std::string strFileName2 = "test2.txt";
//    //3-4) 等效地调用 (1-2) ，如同以 open(filename.c_str(), mode) 。
//    ifstream2.open(strFileName2, std::ios::in);

//    std::ifstream ifstream3;
//    std::string strFileName3 = "test3.txt";
//    ifstream2.open(strFileName3, std::ios::in);

//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream2 is: " << (ifstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream3 is: " << (ifstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::ifstream ifstream1("test1.txt", std::ios::in);
//    std::ifstream ifstream2("test2.txt", std::ios::in);
//    std::ifstream ifstream3("test3.txt", std::ios::in);
//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream2 is: " << (ifstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream3 is: " << (ifstream3.is_open() ? "true" : "false") << std::endl;

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::ifstream ifstream1("test1.txt", std::ios::in);
//    std::cout << "ifstream1: " << std::endl;
//    if (ifstream1.is_open())
//    {
//        while (ifstream1)
//        {
//            //返回指向底层未处理文件设备对象的指针。
//            std::cout << ifstream1.rdbuf() << std::endl;
//        }
//    }

//    std::cout << std::endl;
//    return 0;
//}

//int main()
//{
//    std::ifstream ifstream1("test1.txt", std::ios::in);
//    std::cout << "ifstream1: " << std::endl;
//    if (ifstream1.is_open())
//    {
//        std::string strLine;
//        uint8_t index = 0;
//        while (std::getline(ifstream1, strLine))
//        {
//            std::cout << strLine << std::endl;
//            if (index > 1)
//            {
//                break;
//            }
//            index++;
//        }
//    }
//    std::cout << std::endl;

//    std::ifstream ifstream2("test2.txt", std::ios::in);
//    std::cout << "ifstream2: " << std::endl;
//    if (ifstream2.is_open())
//    {
//        std::string strLine;
//        uint8_t index = 0;
//        while (std::getline(ifstream2, strLine))
//        {
//            std::cout << strLine << std::endl;
//            if (index > 1)
//            {
//                break;
//            }
//            index++;
//        }
//    }
//    std::cout << std::endl;

//    ifstream2.swap(ifstream1);

//    std::cout << "ifstream1: " << std::endl;
//    if (ifstream1.is_open())
//    {
//        std::string strLine;
//        while (std::getline(ifstream1, strLine))
//        {
//            std::cout << strLine << std::endl;
//        }
//    }

//    std::cout << std::endl;

//    std::cout << "ifstream2: " << std::endl;
//    if (ifstream2.is_open())
//    {
//        std::string strLine;
//        while (std::getline(ifstream2, strLine))
//        {
//            std::cout << strLine << std::endl;
//        }
//    }

//    return 0;
//}

//int main()
//{
//    std::cout << "ifstream1: " << std::endl;
//    std::ifstream ifstream1("test.txt", std::ios::in);
//    std::cout << "ifstream1 is: " << (ifstream1 ? "true" : "false") << std::endl;
//    if (ifstream1.is_open())
//    {
//        std::string strLine;
//        while (std::getline(ifstream1, strLine))
//        {
//            std::cout << strLine << std::endl;
//            break;
//        }
//    }
//    std::cout << std::endl;


//    std::ifstream ifstream2 = std::move(ifstream1);
//    std::cout << "ifstream1 is: " << (ifstream1.is_open() ? "true" : "false") << std::endl;
//    std::cout << "ifstream2 is: " << (ifstream2.is_open() ? "true" : "false") << std::endl;
//    std::cout << std::endl;

//    std::cout << "ifstream2: " << std::endl;
//    if (ifstream2.is_open())
//    {
//        std::string strLine;
//        while (std::getline(ifstream2, strLine))
//        {
//            std::cout << strLine << std::endl;
//        }
//    }

//    return 0;
//}

//int main()
//{
//    std::ifstream f0;
//    std::ifstream f1("test.bin", std::ios::binary);
//    std::string name = "example.txt";
//    std::ifstream f2(name);
//    std::ifstream f3(std::move(f1));
//    return 0;
//}
