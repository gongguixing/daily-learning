//#include <iostream>
//#include <utility>
//#include <atomic>
//#include <array>

//struct B
//{
//    int x, y;
//};

//struct A
//{
//};
//int main()
//{
//    std::atomic<A> a;
//    std::atomic<B> b;
//    std::cout << std::boolalpha
//              << "std::atomic<A> is lock free? "
//              << a.is_lock_free() << '\n';
//    std::cout << "std::atomic<B> is lock free? "
//              << b.is_lock_free() << '\n';
//}


//#include <atomic>
//template<typename T>
//struct node
//{
//    T data;
//    node* next;
//    node(const T& data) : data(data), next(nullptr) {}
//};

//template<typename T>
//class stack
//{
//    std::atomic<node<T>*> head;
//public:
//    void push(const T& data)
//    {
//        node<T>* new_node = new node<T>(data);

//        // 放 head 的当前值到 new_node->next 中
//        new_node->next = head.load(std::memory_order_relaxed);

//        // 现在领 new_node 为新的 head ，但若 head 不再是
//        // 存储于 new_node->next 的值（某些其他线程必须在刚才插入结点）
//        // 则放新的 head 到 new_node->next 中并再尝试
//        while (!head.compare_exchange_weak(new_node->next, new_node,
//                                           std::memory_order_release,
//                                           std::memory_order_relaxed))
//            ; // 循环体为空

//// 注意：上述使用至少在这些版本不是线程安全的
//// 先于 4.8.3 的 GCC （漏洞 60272 ），先于 2014-05-05 的 clang （漏洞 18899 ）
//// 先于 2014-03-17 的 MSVC （漏洞 819819 ）。下面是变通方法：
////      node<T>* old_head = head.load(std::memory_order_relaxed);
////      do {
////          new_node->next = old_head;
////       } while(!head.compare_exchange_weak(old_head, new_node,
////                                           std::memory_order_release,
////                                           std::memory_order_relaxed));
//    }
//};
//int main()
//{
//    stack<int> s;
//    s.push(1);
//    s.push(2);
//    s.push(3);
//}

//#include <atomic>
//#include <iostream>

//std::atomic<int>  ai;

//int  tst_val = 4;
//int  new_val = 5;
//bool exchanged = false;

//void valsout()
//{
//    std::cout << "ai= " << ai
//              << "  tst_val= " << tst_val
//              << "  new_val= " << new_val
//              << "  exchanged= " << std::boolalpha << exchanged
//              << "\n";
//}

//int main()
//{
//    ai = 3;
//    valsout();

//    // tst_val != ai   ==>  tst_val 被修改
//    exchanged = ai.compare_exchange_strong(tst_val, new_val);
//    valsout();

//    // tst_val == ai   ==>  ai 被修改
//    exchanged = ai.compare_exchange_strong(tst_val, new_val);
//    valsout();
//}

//#include <iostream>
//#include <thread>
//#include <atomic>

//std::atomic<long long> data;
//void do_work()
//{
//    data.fetch_add(1, std::memory_order_relaxed);
//}

//int main()
//{
//    std::thread th1(do_work);
//    std::thread th2(do_work);
//    std::thread th3(do_work);
//    std::thread th4(do_work);
//    std::thread th5(do_work);

//    th1.join();
//    th2.join();
//    th3.join();
//    th4.join();
//    th5.join();

//    std::cout << "Result:" << data << '\n';
//}

