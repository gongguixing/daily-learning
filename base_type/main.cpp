#include <cstdio>
#include <cinttypes>

int main()
{
    std::printf("INT8_MIN:          %+" PRId8 "\n", INT8_MIN);
    std::printf("INT16_MIN:         %+" PRId16 "\n", INT16_MIN);
    std::printf("INT32_MIN:         %+" PRId32 "\n", INT32_MIN);
    std::printf("INT64_MIN:         %+" PRId64 "\n", INT64_MIN);
    std::printf("\n");

    std::printf("INT_LEAST8_MIN:    %+" PRIdLEAST8 "\n", INT_LEAST8_MIN);
    std::printf("INT_LEAST16_MIN:   %+" PRIdLEAST16 "\n", INT_LEAST16_MIN);
    std::printf("INT_LEAST32_MIN:   %+" PRIdLEAST32 "\n", INT_LEAST32_MIN);
    std::printf("INT_LEAST64_MIN:   %+" PRIdLEAST64 "\n", INT_LEAST64_MIN);
    std::printf("\n");

    std::printf("INT_FAST8_MIN:     %+" PRIdFAST8 "\n", INT_FAST8_MIN);
    std::printf("INT_FAST16_MIN:    %+" PRIdFAST16 "\n", INT_FAST16_MIN);
    std::printf("INT_FAST32_MIN:    %+" PRIdFAST32 "\n", INT_FAST32_MIN);
    std::printf("INT_FAST64_MIN:    %+" PRIdFAST64 "\n", INT_FAST64_MIN);
    std::printf("\n");

    std::printf("INT8_MIN:          %+" PRIi8 "\n", INT8_MIN);
    std::printf("INT16_MIN:         %+" PRIi16 "\n", INT16_MIN);
    std::printf("INT32_MIN:         %+" PRIi32 "\n", INT32_MIN);
    std::printf("INT64_MIN:         %+" PRIi64 "\n", INT64_MIN);
    std::printf("\n");

    std::printf("INT_LEAST8_MIN:    %+" PRIiLEAST8 "\n", INT_LEAST8_MIN);
    std::printf("INT_LEAST16_MIN:   %+" PRIiLEAST16 "\n", INT_LEAST16_MIN);
    std::printf("INT_LEAST32_MIN:   %+" PRIiLEAST32 "\n", INT_LEAST32_MIN);
    std::printf("INT_LEAST64_MIN:   %+" PRIiLEAST64 "\n", INT_LEAST64_MIN);
    std::printf("\n");

    std::printf("INT_FAST8_MIN:     %+" PRIiFAST8 "\n", INT_FAST8_MIN);
    std::printf("INT_FAST16_MIN:    %+" PRIiFAST16 "\n", INT_FAST16_MIN);
    std::printf("INT_FAST32_MIN:    %+" PRIiFAST32 "\n", INT_FAST32_MIN);
    std::printf("INT_FAST64_MIN:    %+" PRIiFAST64 "\n", INT_FAST64_MIN);
    std::printf("\n");

    std::printf("INT8_MIN:          %" PRIo8 "\n", INT8_MIN);
    std::printf("INT16_MIN:         %" PRIo16 "\n", INT16_MIN);
    std::printf("INT32_MIN:         %" PRIo32 "\n", INT32_MIN);
    std::printf("INT64_MIN:         %" PRIo64 "\n", INT64_MIN);
    std::printf("\n");

    std::printf("INT_LEAST8_MIN:    %" PRIoLEAST8 "\n", INT_LEAST8_MIN);
    std::printf("INT_LEAST16_MIN:   %" PRIoLEAST16 "\n", INT_LEAST16_MIN);
    std::printf("INT_LEAST32_MIN:   %" PRIoLEAST32 "\n", INT_LEAST32_MIN);
    std::printf("INT_LEAST64_MIN:   %" PRIoLEAST64 "\n", INT_LEAST64_MIN);
    std::printf("\n");

    std::printf("INT_FAST8_MIN:     %" PRIoFAST8 "\n", INT_FAST8_MIN);
    std::printf("INT_FAST16_MIN:    %" PRIoFAST16 "\n", INT_FAST16_MIN);
    std::printf("INT_FAST32_MIN:    %" PRIoFAST32 "\n", INT_FAST32_MIN);
    std::printf("INT_FAST64_MIN:    %" PRIoFAST64 "\n", INT_FAST64_MIN);
    std::printf("\n");

    std::printf("UINT8_MAX:         %" PRIu8 "\n", UINT8_MAX);
    std::printf("UINT16_MAX:        %" PRIu16 "\n", UINT16_MAX);
    std::printf("UINT32_MAX:        %" PRIu32 "\n", UINT32_MAX);
    std::printf("UINT64_MAX:        %" PRIu64 "\n", UINT64_MAX);
    std::printf("\n");

    std::printf("UINT_LEAST8_MAX:   %" PRIuLEAST8 "\n", UINT_LEAST8_MAX);
    std::printf("UINT_LEAST16_MAX:  %" PRIuLEAST16 "\n", UINT_LEAST16_MAX);
    std::printf("UINT_LEAST32_MAX:  %" PRIuLEAST32 "\n", UINT_LEAST32_MAX);
    std::printf("UINT_LEAST64_MAX:  %" PRIuLEAST64 "\n", UINT_LEAST64_MAX);
    std::printf("\n");

    std::printf("UINT_FAST8_MAX:    %" PRIuFAST8 "\n", UINT_FAST8_MAX);
    std::printf("UINT_FAST16_MAX:   %" PRIuFAST16 "\n", UINT_FAST16_MAX);
    std::printf("UINT_FAST32_MAX:   %" PRIuFAST32 "\n", UINT_FAST32_MAX);
    std::printf("UINT_FAST64_MAX:   %" PRIuFAST64 "\n", UINT_FAST64_MAX);
    std::printf("\n");

    std::printf("UINT8_MAX:         %" PRIx8 "\n", UINT8_MAX);
    std::printf("UINT16_MAX:        %" PRIx16 "\n", UINT16_MAX);
    std::printf("UINT32_MAX:        %" PRIx32 "\n", UINT32_MAX);
    std::printf("UINT64_MAX:        %" PRIx64 "\n", UINT64_MAX);
    std::printf("\n");

    std::printf("UINT_LEAST8_MAX:   %" PRIxLEAST8 "\n", UINT_LEAST8_MAX);
    std::printf("UINT_LEAST16_MAX:  %" PRIxLEAST16 "\n", UINT_LEAST16_MAX);
    std::printf("UINT_LEAST32_MAX:  %" PRIxLEAST32 "\n", UINT_LEAST32_MAX);
    std::printf("UINT_LEAST64_MAX:  %" PRIxLEAST64 "\n", UINT_LEAST64_MAX);
    std::printf("\n");

    std::printf("UINT_FAST8_MAX:    %" PRIxFAST8 "\n", UINT_FAST8_MAX);
    std::printf("UINT_FAST16_MAX:   %" PRIxFAST16 "\n", UINT_FAST16_MAX);
    std::printf("UINT_FAST32_MAX:   %" PRIxFAST32 "\n", UINT_FAST32_MAX);
    std::printf("UINT_FAST64_MAX:   %" PRIxFAST64 "\n", UINT_FAST64_MAX);
    std::printf("\n");
    return 0;
}

//#include <iostream>
//#include <cstdint>

//int main()
//{
//    //展开成拥有其实参所指定的值且类型分别为 int_least8_t、int_least16_t、
//    //int_least32_t、int_least64_t 的整数常量表达式
//    std::cout << "INT8_C(8):            " << INT8_C(8) << std::endl;
//    std::cout << "INT16_C(16):          " << INT16_C(16) << std::endl;
//    std::cout << "INT32_C(32):          " << INT32_C(32) << std::endl;
//    std::cout << "INT64_C(64):          " << INT64_C(64) << std::endl;
//    std::cout << std::endl;

//    //展开成拥有其实参所指定的值且类型分别为 uint_least8_t、uint_least16_t、
//    //uint_least32_t、uint_least64_t 的整数常量表达式
//    std::cout << "UINT8_C(8):           " << UINT8_C(8) << std::endl;
//    std::cout << "UINT16_C(16):         " << UINT16_C(16) << std::endl;
//    std::cout << "UINT32_C(32):         " << UINT32_C(32) << std::endl;
//    std::cout << "UINT64_C(64):         " << UINT64_C(64) << std::endl;
//    std::cout << std::endl;

//    //展开成拥有其实参所指定的值且类型为 intmax_t 的整数常量表达式
//    std::cout << "INTMAX_C(32):         " << INTMAX_C(32) << std::endl;
//    //展开成拥有其实参所指定的值且类型为 uintmax_t 的整数常量表达式
//    std::cout << "UINTMAX_C(64):        " << UINTMAX_C(64) << std::endl;
//    std::cout << std::endl;
//    return 0;
//}

//#include <iostream>
//#include <cstdint>

//int main()
//{
//    //int8_t、int16_t、int32_t、int64_t 类型对象的最小值
//    std::cout << "INT8_MIN:         " << INT8_MIN << std::endl;
//    std::cout << "INT16_MIN:        " << INT16_MIN << std::endl;
//    std::cout << "INT32_MIN:        " << INT32_MIN << std::endl;
//    std::cout << "INT64_MIN:        " << INT64_MIN << std::endl;
//    std::cout << std::endl;

//    //int_fast8_t、int_fast16_t、int_fast32_t、int_fast64_t 类型对象的最小
//    std::cout << "INT_FAST8_MIN:    " << INT_FAST8_MIN << std::endl;
//    std::cout << "INT_FAST16_MIN:   " << INT_FAST16_MIN << std::endl;
//    std::cout << "INT_FAST32_MIN:   " << INT_FAST32_MIN << std::endl;
//    std::cout << "INT_FAST64_MIN:   " << INT_FAST64_MIN << std::endl;
//    std::cout << std::endl;

//    //int_least8_t、int_least16_t、int_least32_t、int_least64_t 类型对象的最小值
//    std::cout << "INT_LEAST8_MIN:   " << INT_LEAST8_MIN << std::endl;
//    std::cout << "INT_LEAST16_MIN:  " << INT_LEAST16_MIN << std::endl;
//    std::cout << "INT_LEAST32_MIN:  " << INT_LEAST32_MIN << std::endl;
//    std::cout << "INT_LEAST64_MIN:  " << INT_LEAST64_MIN << std::endl;
//    std::cout << std::endl;

//    //int8_t、int16_t、int32_t、int64_t 类型对象的最大值
//    std::cout << "INT8_MAX:         " << INT8_MAX << std::endl;
//    std::cout << "INT16_MAX:        " << INT16_MAX << std::endl;
//    std::cout << "INT32_MAX:        " << INT32_MAX << std::endl;
//    std::cout << "INT64_MAX:        " << INT64_MAX << std::endl;
//    std::cout << std::endl;

//    //int_fast8_t、int_fast16_t、int_fast32_t、int_fast64_t 类型对象的最大值
//    std::cout << "INT_FAST8_MAX:    " << INT_FAST8_MAX << std::endl;
//    std::cout << "INT_FAST16_MAX:   " << INT_FAST16_MAX << std::endl;
//    std::cout << "INT_FAST32_MAX:   " << INT_FAST32_MAX << std::endl;
//    std::cout << "INT_FAST64_MAX:   " << INT_FAST64_MAX << std::endl;
//    std::cout << std::endl;

//    //int_least8_t、int_least16_t、int_least32_t、int_least64_t 类型对象的最大值
//    std::cout << "INT_LEAST8_MAX:   " << INT_LEAST8_MAX << std::endl;
//    std::cout << "INT_LEAST16_MAX:  " << INT_LEAST16_MAX << std::endl;
//    std::cout << "INT_LEAST32_MAX:  " << INT_LEAST32_MAX << std::endl;
//    std::cout << "INT_LEAST64_MAX:  " << INT_LEAST64_MAX << std::endl;
//    std::cout << std::endl;

//    //intptr_t 类型对象的最小值
//    std::cout << "INTPTR_MIN:       " << INTPTR_MIN << std::endl;
//    //intmax_t 类型对象的最小值
//    std::cout << "INTMAX_MIN:       " << INTMAX_MIN << std::endl;
//    //intptr_t 类型对象的最大值
//    std::cout << "INTPTR_MAX:       " << INTPTR_MAX << std::endl;
//    //intmax_t 类型对象的最大值
//    std::cout << "INTMAX_MAX:       " << INTMAX_MAX << std::endl;
//    std::cout << std::endl;

//    //uint8_t、uint16_t、uint32_t、uint64_t 类型对象的最大值
//    std::cout << "UINT8_MAX:         " << UINT8_MAX << std::endl;
//    std::cout << "UINT16_MAX:        " << UINT16_MAX << std::endl;
//    std::cout << "UINT32_MAX:        " << UINT32_MAX << std::endl;
//    std::cout << "UINT64_MAX:        " << UINT64_MAX << std::endl;
//    std::cout << std::endl;

//    //uint_fast8_t、uint_fast16_t、uint_fast32_t、uint_fast64_t 类型对象的最大值
//    std::cout << "UINT_FAST8_MAX:    " << UINT_FAST8_MAX << std::endl;
//    std::cout << "UINT_FAST16_MAX:   " << UINT_FAST16_MAX << std::endl;
//    std::cout << "UINT_FAST32_MAX:   " << UINT_FAST32_MAX << std::endl;
//    std::cout << "UINT_FAST64_MAX:   " << UINT_FAST64_MAX << std::endl;
//    std::cout << std::endl;

//    //uint_least8_t、uint_least16_t、uint_least32_t、uint_least64_t 类型对象的最大值
//    std::cout << "UINT_LEAST8_MAX:   " << UINT_LEAST8_MAX << std::endl;
//    std::cout << "UINT_LEAST16_MAX:  " << UINT_LEAST16_MAX << std::endl;
//    std::cout << "UINT_LEAST32_MAX:  " << UINT_LEAST32_MAX << std::endl;
//    std::cout << "UINT_LEAST64_MAX:  " << UINT_LEAST64_MAX << std::endl;
//    std::cout << std::endl;

//    //uintptr_t 类型对象的最大值
//    std::cout << "UINTPTR_MAX:       " << UINTPTR_MAX << std::endl;
//    //uintmax_t 类型对象的最大值
//    std::cout << "UINTMAX_MAX:       " << UINTMAX_MAX << std::endl;
//    std::cout << std::endl;
//    return 0;
//}

//#include <iostream>
//#include <cstdint>

//int main()
//{
//    //分别为宽度恰为 8、16、32 和 64 位的有符号整数类型,无填充位并对负值使用补码
//    std::cout << "sizeof(int8_t):           " << sizeof(int8_t) << std::endl;
//    std::cout << "sizeof(int16_t):          " << sizeof(int16_t) << std::endl;
//    std::cout << "sizeof(int32_t):          " << sizeof(int32_t) << std::endl;
//    std::cout << "sizeof(int64_t):          " << sizeof(int64_t) << std::endl;
//    std::cout << std::endl;

//    //分别为宽度至少有 8、16、32 和 64 位的最快的有符号整数类型
//    std::cout << "sizeof(int_fast8_t):      " << sizeof(int_fast8_t) << std::endl;
//    std::cout << "sizeof(int_fast16_t):     " << sizeof(int_fast16_t) << std::endl;
//    std::cout << "sizeof(int_fast32_t):     " << sizeof(int_fast32_t) << std::endl;
//    std::cout << "sizeof(int_fast64_t):     " << sizeof(int_fast64_t) << std::endl;
//    std::cout << std::endl;

//    //分别为宽度至少有 8、16、32 和 64 位的最小的有符号整数类型
//    std::cout << "sizeof(int_least8_t):     " << sizeof(int_least8_t) << std::endl;
//    std::cout << "sizeof(int_least16_t):    " << sizeof(int_least16_t) << std::endl;
//    std::cout << "sizeof(int_least32_t):    " << sizeof(int_least32_t) << std::endl;
//    std::cout << "sizeof(int_least32_t):    " << sizeof(int_least64_t) << std::endl;
//    std::cout << std::endl;

//    //宽度恰为 8、16、32 和 64 位的无符号整数类型
//    std::cout << "sizeof(uint8_t):          " << sizeof(uint8_t) << std::endl;
//    std::cout << "sizeof(uint16_t):         " << sizeof(uint16_t) << std::endl;
//    std::cout << "sizeof(uint32_t):         " << sizeof(uint32_t) << std::endl;
//    std::cout << "sizeof(uint64_t):         " << sizeof(uint64_t) << std::endl;
//    std::cout << std::endl;

//    //分别为宽度至少有 8、16、32 和 64 位的最快无符号整数类型
//    std::cout << "sizeof(uint_fast8_t):     " << sizeof(uint_fast8_t) << std::endl;
//    std::cout << "sizeof(uint_fast16_t):    " << sizeof(uint_fast16_t) << std::endl;
//    std::cout << "sizeof(uint_fast32_t):    " << sizeof(uint_fast32_t) << std::endl;
//    std::cout << "sizeof(uint_fast64_t):    " << sizeof(uint_fast64_t) << std::endl;
//    std::cout << std::endl;

//    //分别为宽度至少有 8、16、32 和 64 位的最小无符号整数类型
//    std::cout << "sizeof(uint_least8_t):    " << sizeof(uint_least8_t) << std::endl;
//    std::cout << "sizeof(uint_least16_t):   " << sizeof(uint_least16_t) << std::endl;
//    std::cout << "sizeof(uint_least32_t):   " << sizeof(uint_least32_t) << std::endl;
//    std::cout << "sizeof(uint_least32_t):   " << sizeof(uint_least64_t) << std::endl;
//    std::cout << std::endl;

//    //最大宽度的有符号整数类型
//    std::cout << "sizeof(intmax_t):         " << sizeof(intmax_t) << std::endl;
//    //足以保有指针的有符号整数类型
//    std::cout << "sizeof(intptr_t):         " << sizeof(intptr_t) << std::endl;
//    std::cout << "sizeof(uintmax_t):        " << sizeof(uintmax_t) << std::endl;
//    std::cout << "sizeof(uintptr_t):        " << sizeof(uintptr_t) << std::endl;
//    std::cout << std::endl;
//    return 0;
//}

//#include <cstdio>
//#include <cinttypes>

//int main()
//{
//    std::printf("%zu\n", sizeof(std::int64_t));
//    std::printf("%s\n", PRId64);
//    std::printf("%+" PRId64 "\n", INT64_MIN);
//    std::printf("%+" PRId64 "\n", INT64_MAX);

//    std::int64_t n = 7;
//    std::printf("%+" PRId64 "\n", n);
//    return 0;
//}

//#include <iostream>
//#include <cstddef>

//struct S
//{
//    char c;
//    double d;
//};

//int main()
//{
//    std::cout << "the first element is at offset "
//              << offsetof(S, c) << std::endl
//              << "the double is at offset "
//              << offsetof(S, d) << std::endl;
//    return 0;
//}

//#include <iostream>
//#include <cstddef>

//int main()
//{
//    std::cout << "alignof(std::max_align_t): "
//              << alignof(std::max_align_t) << std::endl;
//    return 0;
//}

//#include <cstddef>
//#include <type_traits>
//#include <iostream>

//class S;

//int main()
//{
//    int* p = NULL;
//    int* p2 = static_cast<std::nullptr_t>(NULL);
//    void(*f)(int) = NULL;
//    int S::*mp = NULL;
//    void(S::*mfp)(int) = NULL;

//    if (std::is_same<decltype(NULL), std::nullptr_t>::value)
//    {
//        std::cout << "NULL implemented with type std::nullptr_t" << std::endl;
//    }
//    else
//    {
//        std::cout << "NULL implemented using an integral type" << std::endl;
//    }
//    return 0;
//}

//#include <cstddef>
//#include <iostream>

//void f(int* pi)
//{
//    std::cout << "Pointer to integer overload" << std::endl;
//}

//void f(double* pd)
//{
//    std::cout << "Pointer to double overload" << std::endl;
//}

//void f(std::nullptr_t nullp)
//{
//    std::cout << "null pointer overload" << std::endl;
//}

//int main()
//{
//    int* pi;
//    double* pd;

//    f(pi);
//    f(pd);
//    f(nullptr);  // 无 void f(nullptr_t) 可能有歧义
//    // f(0);  // 歧义调用：三个函数全部为候选
//    // f(NULL); // 若 NULL 是整数空指针常量则为歧义
//    // （如在大部分实现中的情况）
//    return 0;
//}

//#include <cstddef>
//#include <iostream>
//#include <iomanip>

//int main()
//{
//    const std::size_t N = 100;
//    int* a = new int[N];
//    int* end = a + N;
//    for (std::ptrdiff_t i = N; i > 0; --i)
//    {
//        if (i % 10 == 0)
//        {
//            std::cout << std::endl;
//        }
//        std::cout << std::setw(3) << (*(end - i) = i) << ' ';
//    }
//    std::cout << std::endl;

//    delete[] a;
//    return 0;
//}

//#include <cstddef>
//#include <iostream>

//int main()
//{
//    const std::size_t N = 10;
//    int* a = new int[N];

//    for (std::size_t n = 0; n < N; ++n)
//    {
//        a[n] = n;
//    }

//    for (std::size_t n = N; n-- > 0;) // 对于无符号类型的逆向循环技巧。
//    {
//        std::cout << a[n] << " ";
//    }

//    delete[] a;
//    return 0;
//}

//#include <iostream>

//using namespace std;

//int main()
//{
//    cout << "Hello World!" << endl;
//    return 0;
//}
