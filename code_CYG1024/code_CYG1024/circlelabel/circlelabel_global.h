#ifndef CIRCLELABEL_GLOBAL_H
#define CIRCLELABEL_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CIRCLELABEL_LIBRARY)
#  define CIRCLELABELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CIRCLELABELSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CIRCLELABEL_GLOBAL_H
