#include "circlelabel.h"
#include <QPainter>
#include <QTimer>
#include <QDebug>

CircleLabel::CircleLabel(QWidget *parent):
    QLabel(parent)
{
    m_bg = QImage("./123456.png");

    m_pCircleTimer = new QTimer(this);
    m_pCircleTimer->setInterval(50);
    connect(m_pCircleTimer, &QTimer::timeout, this, &CircleLabel::onTimeout);
}

void CircleLabel::startCircle()
{
    m_pCircleTimer->start();
}
void CircleLabel::stopCircle()
{
    m_pCircleTimer->stop();
    m_nAngle = 0;
    update();
}

void CircleLabel::setImage(const QImage &iamge)
{
    m_bg = iamge;
}

QPoint CircleLabel::getCentre()
{
    QPoint gPos = mapToGlobal(pos());
    QPoint centre = QPoint(gPos.x() + width() / 2, gPos.y() + height() / 2);
    qDebug() << centre;
    return centre;
}

QPoint CircleLabel::getGlobal()
{
    return mapToGlobal(pos());
}
void CircleLabel::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::NoBrush);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    painter.translate(this->width() / 2, this->height() / 2);          //锟斤拷锟斤拷锟斤拷转锟斤拷锟斤拷
    painter.rotate(m_nAngle);          //锟斤拷转
    painter.translate(-(this->width() / 2), -(this->height() / 2));        //锟斤拷原锟姐复位
    painter.drawImage(this->rect(), m_bg);
    return QWidget::paintEvent(e);
}

void CircleLabel::onTimeout()
{
//    if (m_nAngle == 360)
//    {
//        m_nAngle = 0;
//    }
//    m_nAngle += 15;

    if (m_nAngle > 30)
    {
        index = - 15;
    }
    if (m_nAngle < -30)
    {
        index = 15;
    }
    m_nAngle += index;

    update();
}
