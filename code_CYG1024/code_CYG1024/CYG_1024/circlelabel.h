#ifndef CIRCLELABEL_H
#define CIRCLELABEL_H

#include <QLabel>
#include <QStyleOption>

class QTimer;

class CircleLabel : public QLabel
{
    Q_OBJECT
public:
    explicit CircleLabel(QWidget *parent = 0);

    void startCircle();
    void stopCircle();

    void setImage(const QImage &iamge);
    QPoint getCentre();
    QPoint getGlobal();

protected:
    void paintEvent(QPaintEvent *e);

private slots:
    void onTimeout();

private:
    QImage m_bg;
    QTimer *m_pCircleTimer = Q_NULLPTR;
    int m_nAngle = 0;
    int index = 15;

};

#endif // CIRCLELABEL_H
