#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qtimer.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void movePos(int *x,int *y,int *x_inc,int *y_inc);

    void TQmove();
    void YMove();

private slots:
    void slTimer();

    void on_pushButton_start_clicked();

    void on_pushButton_stop_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *mPtimer;
    QPoint mCentre;
    int step = 10;
    int stepx = 10;
    int stepy = -10;
    int stepx1 = 10;
    int stepy1 = -10;
    int model = 0;
    int count = 0;
};

#endif // MAINWINDOW_H
