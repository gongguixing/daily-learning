#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->showMaximized();
    model = 0;
    mPtimer = new QTimer(this);
    connect(mPtimer, SIGNAL(timeout()), this, SLOT(slTimer()));
    ui->label_1024->setImage(QImage(":/image/iamge/1024.png"));
    ui->label_CYG->setImage(QImage(":/image/iamge/CYG.png"));
    qDebug() << "MainWindow size: " << this->size() ;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::movePos(int *x, int *y, int *x_inc, int *y_inc)
{
    if (*x <= 0 || *x >= width() - 30)//等于0或者 ROW-1，即是边界，便忘反方向走
    {
        *x_inc = -*x_inc;
    }
    *x += *x_inc;//单步移动
    if (*y <= 0 || *y >= height() - 30)//同上，
    {
        *y_inc = -*y_inc;
    }
    *y += *y_inc;
}

void MainWindow::TQmove()
{
    int x = ui->label_1024->pos().x();
    int y = ui->label_1024->pos().y();
    movePos(&x, &y, &stepx, &stepy);
    if(count % 2 == 0)
    {
        movePos(&x, &y, &stepx, &stepy);
    }
    ui->label_1024->move(x,y);

    x = ui->label_CYG->pos().x();
    y = ui->label_CYG->pos().y();
    movePos(&x, &y, &stepx1, &stepy1);
    if(count % 2 == 0)
    {
        movePos(&x, &y, &stepx1, &stepy1);
    }
    ui->label_CYG->move(x,y);
}

void MainWindow::YMove()
{
    qDebug() << "MainWindow size: " << this->size() ;
    qDebug() << "label_CYG size: " << ui->label_CYG->size() ;
    qDebug() << "label_CYG pos: " << ui->label_CYG->pos() ;
    qDebug() << "label_1024 size: " << ui->label_1024->size() ;
    qDebug() << "label_1024 pos: " << ui->label_1024->pos() ;
    if(ui->label_CYG->pos().x() > width() / 2 - ui->label_CYG->width())
    {
        ui->label_CYG->move(ui->label_CYG->pos().x() - step, ui->label_CYG->pos().y());
    }

    if(ui->label_CYG->pos().y() < height() / 2 - ui->label_1024->height())
    {
        ui->label_CYG->move(ui->label_CYG->pos().x(), ui->label_CYG->pos().y() + step);
    }

    if(ui->label_1024->pos().x() < width() / 2)
    {
        ui->label_1024->move(ui->label_1024->pos().x() + step, ui->label_1024->pos().y());
    }
    if(ui->label_1024->pos().y() > height() / 2 - ui->label_1024->height())
    {
        ui->label_1024->move(ui->label_1024->pos().x(), ui->label_1024->pos().y() - step);
    }
}

void MainWindow::slTimer()
{
    count++;
    if(count > 1024)
    {
        count = 0;
    }

    if(model == 0)
    {
        YMove();
    }
    else {
        TQmove();
    }
    mCentre = QPoint(pos().x() + width() / 2, pos().y() + height() / 2);
}

void MainWindow::on_pushButton_start_clicked()
{
    mPtimer->stop();
    model = 0;
    int temp = 50;
    ui->label_CYG->startCircle();
    ui->label_1024->startCircle();
    ui->label_CYG->move(this->width() - ui->label_CYG->width() - temp, temp);
    ui->label_1024->move(temp, this->height() - temp);
    mPtimer->start(50);
}

void MainWindow::on_pushButton_stop_clicked()
{
    ui->label_CYG->stopCircle();
    ui->label_1024->stopCircle();
}

void MainWindow::on_pushButton_clicked()
{
    mPtimer->stop();
    model = 1;
    int temp = 50;
    ui->label_CYG->startCircle();
    ui->label_1024->startCircle();
    ui->label_CYG->move(this->width() - ui->label_CYG->width() - temp, temp);
    ui->label_1024->move(temp, this->height() - temp);
    mPtimer->start(5);
}
