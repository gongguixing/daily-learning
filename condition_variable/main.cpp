//#include <iostream>
//#include <condition_variable>
//#include <thread>
//#include <chrono>

//std::condition_variable cv;
//std::mutex cv_m;
//int i = 0;
//bool done = false;

//void waits()
//{
//    std::unique_lock<std::mutex> lk(cv_m);
//    std::cout << "Waiting... \n";
//    cv.wait(lk, [] {return i == 1;});
//    std::cout << "...finished waiting. i == 1\n";
//    done = true;
//}

//void signals()
//{
//    std::this_thread::sleep_for(std::chrono::seconds(1));
//    std::cout << "Notifying falsely...\n";
//    cv.notify_one(); // 等待线程被通知 i == 0.
//    // cv.wait 唤醒，检查 i ，再回到等待

//    std::unique_lock<std::mutex> lk(cv_m);
//    i = 1;
//    while (!done)
//    {
//        std::cout << "Notifying true change...\n";
//        lk.unlock();
//        cv.notify_one(); // 等待线程被通知 i == 1 ， cv.wait 返回
//        std::this_thread::sleep_for(std::chrono::seconds(1));
//        lk.lock();
//    }
//}

//int main()
//{
//    std::thread t1(waits), t2(signals);
//    t1.join();
//    t2.join();
//}

// 通知所有等待的线程

//#include <iostream>
//#include <condition_variable>
//#include <thread>
//#include <chrono>

//std::condition_variable cv;
//std::mutex cv_m; // 此互斥用于三个目的：
//// 1) 同步到 i 的访问
//// 2) 同步到 std::cerr 的访问
//// 3) 为条件变量 cv
//int i = 0;

//void waits()
//{
//    std::unique_lock<std::mutex> lk(cv_m);
//    std::cerr << "Waiting... \n";
//    cv.wait(lk, [] {return i == 1;});
//    std::cerr << "...finished waiting. i == 1\n";
//}

//void signals()
//{
//    std::this_thread::sleep_for(std::chrono::seconds(1));
//    {
//        std::lock_guard<std::mutex> lk(cv_m);
//        std::cerr << "Notifying...\n";
//    }
//    cv.notify_all();

//    std::this_thread::sleep_for(std::chrono::seconds(1));

//    {
//        std::lock_guard<std::mutex> lk(cv_m);
//        i = 1;
//        std::cerr << "Notifying again...\n";
//    }
//    cv.notify_all();
//}

//int main()
//{
//    std::thread t1(waits), t2(waits), t3(waits), t4(signals);
//    t1.join();
//    t2.join();
//    t3.join();
//    t4.join();
//}

//阻塞当前线程，直到条件变量被唤醒
//阻塞当前线程，直到条件变量被唤醒，或到指定时限时长后

//#include <iostream>
//#include <atomic>
//#include <condition_variable>
//#include <thread>
//#include <chrono>
//using namespace std::chrono;

//std::condition_variable cv;
//std::mutex cv_m;
//int i;

//void waits(int idx)
//{
//    std::unique_lock<std::mutex> lk(cv_m);
//    if (cv.wait_for(lk, milliseconds(idx * 100), [] {return i == 1;}))
//        std::cerr << "Thread " << idx << " finished waiting. i == " << i << '\n';
//    else
//    {
//        std::cerr << "Thread " << idx << " timed out. i == " << i << '\n';
//    }
//}

//void signals()
//{
//    std::this_thread::sleep_for(milliseconds(120));
//    std::cerr << "Notifying...\n";
//    cv.notify_all();
//    std::this_thread::sleep_for(milliseconds(100));
//    {
//        std::lock_guard<std::mutex> lk(cv_m);
//        i = 1;
//    }
//    std::cerr << "Notifying again...\n";
//    cv.notify_all();
//}

//int main()
//{
//    std::thread t1(waits, 1), t2(waits, 2), t3(waits, 3), t4(signals);
//    t1.join();
//    t2.join();
//    t3.join();
//    t4.join();
//}

//阻塞当前线程，直到条件变量被唤醒，或直到抵达指定时间点

#include <iostream>
#include <atomic>
#include <condition_variable>
#include <thread>
#include <chrono>
using namespace std::chrono;

std::condition_variable cv;
std::mutex cv_m;
std::atomic<int> i{0};

void waits(int idx)
{
    std::unique_lock<std::mutex> lk(cv_m);
    auto now = std::chrono::system_clock::now();
    auto func = []()
    {
        return i == 1;
    };

    if (cv.wait_until(lk, now + milliseconds(idx * 100), func))
    {
        std::cerr << "Thread " << idx << " finished waiting. i == " << i << '\n';
    }
    else
    {
        std::cerr << "Thread " << idx << " timed out. i == " << i << '\n';
    }
}

void signals()
{
    std::this_thread::sleep_for(milliseconds(120));
    std::cerr << "Notifying...\n";
    cv.notify_all();
    std::this_thread::sleep_for(milliseconds(100));
    i = 1;
    std::cerr << "Notifying again...\n";
    cv.notify_all();
}

int main()
{
    std::thread t1(waits, 1), t2(waits, 2), t3(waits, 3), t4(signals);
    t1.join();
    t2.join();
    t3.join();
    t4.join();
}
